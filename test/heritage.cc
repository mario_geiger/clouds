#include <iostream>

using namespace std;

class A {
public:
	virtual void affiche() { cout << "Je suis un A" << endl; }
};

class B : public A {
public:
	void affiche() { cout << "Je suis un B" << endl; }
};

class C : public B {
public:
	void affiche() { cout << "Je suis un C" << endl; }
};

int main()
{
	B* p = new C();
	p->affiche();
	
	return 0;
}
