#include <iostream>
using namespace std;

template<typename K>
class A {
public:
	template<typename U>
	void bar(K a, U b)
	{
		cout << a << " " << b;
	}
};

int main() 
{
	
	A<int> a;
	a.bar(10.0, 3.4);
}
