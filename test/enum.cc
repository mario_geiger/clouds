#include <iostream>
using namespace std;
enum Options { option1 = 0x1, option2 = 0x2, option3 = 0x4, def = option1 | option2 };
//constexpr inline Options operator|(Options f, Options g)
//{ return static_cast<Options>(static_cast<int>(f) | static_cast<int>(g)); }

int main()
{
	int opt = def | option2;
	if (opt & option2) {
		cout << "Hello" << endl;
	} else {
		cout << "Pas hello" << endl;
	}
}
