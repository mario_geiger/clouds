#include <iostream>
#include <array>
#include <initializer_list>
using namespace std;

void foo(std::initializer_list<int> l)
{
	for (auto x:l)
		cout << x << " ";
}

class A {
	public:
	A() { 
		cout << "A" << endl;
	}
	~A() {
		cout << "~A" << endl;
	}
};

int main()
{
	A* a = new A();
	free(a);
}
