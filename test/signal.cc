#include <iostream>
#include <boost/bind.hpp>
#include <boost/signals2/signal.hpp>

class A
{
public:
  typedef boost::signals2::signal<void (int i, double e)>  signal_t;

  boost::signals2::connection connect(const signal_t::slot_type &subscriber)
  {
    return m_sig.connect(subscriber);
  }
  
  void solve()
  {
    int iteration = 0;
    double error = 1000.0;
    /* ... */
    m_sig(iteration, error);
  }
private:
  signal_t m_sig;
};

class B
{
public:
  void foo(int i, double e)
  {
    std::cout << "La barre progresse ! " << i << " erreur=" << e << std::endl;
  }
};

int main()
{
  A a;
  B b;
  
  a.connect(boost::bind(&B::foo, &b, _1, _2));
  
  a.solve();
  return 0;
}
