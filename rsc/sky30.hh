#ifndef __SKY30_HH__
#define __SKY30_HH__

extern const unsigned int posx_jpg_size;
extern const unsigned char posx_jpg[];

extern const unsigned int negx_jpg_size;
extern const unsigned char negx_jpg[];

extern const unsigned int posy_jpg_size;
extern const unsigned char posy_jpg[];

extern const unsigned int negy_jpg_size;
extern const unsigned char negy_jpg[];

extern const unsigned int posz_jpg_size;
extern const unsigned char posz_jpg[];

extern const unsigned int negz_jpg_size;
extern const unsigned char negz_jpg[];

#endif
