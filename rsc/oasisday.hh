#ifndef __OASISDAY_HH__
#define __OASISDAY_HH__

extern const unsigned int oasisday_back_jpg_size;
extern const unsigned char oasisday_back_jpg[];

extern const unsigned int oasisday_front_jpg_size;
extern const unsigned char oasisday_front_jpg[];

extern const unsigned int oasisday_left_jpg_size;
extern const unsigned char oasisday_left_jpg[];

extern const unsigned int oasisday_right_jpg_size;
extern const unsigned char oasisday_right_jpg[];

extern const unsigned int oasisday_top_jpg_size;
extern const unsigned char oasisday_top_jpg[];

#endif
