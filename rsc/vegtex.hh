#ifndef __VEGTEX_HH__
#define __VEGTEX_HH__

extern const unsigned int beech_png_size;
extern const unsigned char beech_png[];

extern const unsigned int beech_top_png_size;
extern const unsigned char beech_top_png[];

extern const unsigned int palm_png_size;
extern const unsigned char palm_png[];

extern const unsigned int palm_top_png_size;
extern const unsigned char palm_top_png[];

extern const unsigned int bush_png_size;
extern const unsigned char bush_png[];

extern const unsigned int bush2_png_size;
extern const unsigned char bush2_png[];

#endif
