#ifndef __TEX_GROUND_HH__
#define __TEX_GROUND_HH__

extern const unsigned int grass_jpg_size;
extern const unsigned char grass_jpg[];

extern const unsigned int rock_jpg_size;
extern const unsigned char rock_jpg[];

extern const unsigned int smoketex_jpg_size;
extern const unsigned char smoketex_jpg[];

extern const unsigned int arid_jpg_size;
extern const unsigned char arid_jpg[];

#endif
