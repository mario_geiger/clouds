#ifndef __PROGRESSBAR_HH__
#define __PROGRESSBAR_HH__

class ProgressBar
{
public:
	ProgressBar();
	void setMinimum(double minimum);
	void setMaximum(double maximum);
	void display(double value);
	void clearline();
private:
	double m_minimum;
	double m_maximum;
	int m_count;
};

#endif 
