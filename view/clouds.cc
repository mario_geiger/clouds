#include <iostream>
#include "system.hh"
#include "parser.hh"
using namespace std;
   
int main(int argc, char* argv[]) { // utilisation : ./test_system2   nom_du_fichier_xml   nombre_de_pas_de_temps   duree_du_pas_de_temps
	System monde;
	Parser lecteur(&monde);
	
	if(argc > 1 && lecteur.parse(argv[1])) {
		monde.init();
		if(argc <= 3) {
			cerr << "Error : undefined number of time intervals and lentgth of interval." << endl;
		} else {
			int fin = atoi(argv[2]);
			for(int i(0); i<fin; i++) {
				monde.evolve(atof(argv[3]));
			}
			cout << monde << endl;
		}
	} else {
		cerr << "Error : undefined XML file name." << endl;
	}
	
	return 0;
}
