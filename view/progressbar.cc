#include <sys/ioctl.h>
#include "progressbar.hh"
#include <iostream>
#include <iomanip>
using namespace std;

ProgressBar::ProgressBar() :
	m_minimum(0.0),
	m_maximum(0.0),
	m_count(0)
{
}

void ProgressBar::setMinimum(double minimum)
{
	m_minimum = minimum;
}

void ProgressBar::setMaximum(double maximum)
{
	m_maximum = maximum;
}

void ProgressBar::display(double value)
{
	++m_count;

	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);
	
	// [o   o   o   o   o]*  0%
	// [----c   o   o   o]*
	// [-------Co   o   o]*
	// [-----------------]*100%
	
	// [o  ]*  0%
	// [c  ]* 25%
	// [-C ]* 50%
	// [--c]* 75%
	// [---]*100%
	
	double v = (value - m_minimum) / (m_maximum - m_minimum);
	v = std::max(0.0, std::min(v, 1.0));
	int len = w.ws_col - 7 - 3;
	int p = v * (len + 1.0);
	int pc = 100.0 * v;
	
	clearline();
	cout << '[';
	for (int i = 0; i < len; ++i) {
		if (i < p) {
			if (i == p-1) {
				cout << "\033[1;33m";
				if ((i+1) % 4 == 0 || (i+1) % 4 == 1)
					cout << 'C';
				else
					cout << 'c';
				cout << "\033[0m";
			} else {
				cout << '-';
			}
		} else {
			if (i % 4 == 0)
				cout << 'o';
			else 
				cout << ' ';
		}
	}
	cout << ']';
	
	int roll = (m_count / 10) % 4;
	if (roll == 1) cout << '-';
	else if (roll == 2) cout << '\\';
	else if (roll == 3) cout << '|';
	else if (roll == 0) cout << '/';

	cout << setfill(' ');
	cout.unsetf(ios::left);
	cout << setw(3) << pc << '%' << flush;
}

void ProgressBar::clearline()
{
	cout << "\033[999D\033[K" << flush;
}
