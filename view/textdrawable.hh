#ifndef __DRAWABLE_HH__
#define __DRAWABLE_HH__

#include <iostream>

class TextDrawable
{
public:
	virtual void draw(std::ostream& out) const = 0;
	
	friend std::ostream& operator<< (std::ostream& out, const TextDrawable& t)
	{
		t.draw(out);
		return out;
	}
};

#endif
