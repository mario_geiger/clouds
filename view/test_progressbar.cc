#include "progressbar.hh"
#include <iostream>
#include <unistd.h>
using namespace std;

int main()
{
	ProgressBar p;
	p.setMaximum(100.0);
	double v = 0.0;
	for (; v <= 100.0; v += 0.1) {
		usleep(10000);
		p.display(v);
	}
	p.clearline();
}
