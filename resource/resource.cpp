#include <QCoreApplication>
#include <QStringList>
#include <QByteArray>
#include <QImage>
#include <QBuffer>
#include <QFile>
#include <QFileInfo>
#include <QTextStream>

/* Ce petit programme écrit les images dans un fichier c
 *
 * - il lit l'image
 * - la redimentionne, par exemple 512x512
 * - écrit dans stdout avec le format d'un tableau à la c
 */


QTextStream cout(stdout);
QTextStream cerr(stderr);

void showHelp();
QByteArray convertImageToRaw(const QImage &img);
void writeData(const QByteArray &data, const QString &fileName);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    QStringList args = a.arguments();

    if (args.size() < 2) {
        showHelp();
        return 0;
    }

    int newWidth = -1;
    int newHeight = -1;

    args.takeFirst();
    if (args.isEmpty()) return 0;
    QString nextArgument = args.takeFirst();
    while (nextArgument.startsWith('-')) {
        if (nextArgument == "-S" && args.size() >= 2) {
            newWidth = args.takeFirst().toInt();
            newHeight = args.takeFirst().toInt();
            cerr << "new size set to " << newWidth << ", " << newHeight << endl;
        } else {
            showHelp();
            return 0;
        }
		if (args.isEmpty()) return 0;
        nextArgument = args.takeFirst();
    }
    QFileInfo fileInfo(nextArgument);

    QByteArray data;
    if (newWidth == -1 || newHeight == -1) {
        QFile f(fileInfo.filePath());
        if (!f.open(QIODevice::ReadOnly)) {
            cerr << "error while opening the file" << endl;
            return 1;
        }
        data = f.readAll();
    } else {
        QImage image(fileInfo.filePath());
        image = image.scaled(newWidth, newHeight, Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
        QBuffer buffer(&data);
        buffer.open(QIODevice::WriteOnly);
        image.save(&buffer, fileInfo.suffix().toLatin1());
    }

    writeData(data, fileInfo.fileName());
    return 0;
}

void showHelp()
{
    cerr << "write the file given in argument into the standard output on format C" << endl;
    cerr << "example : resource -S 300 300 image.jpeg > myfile.c" << endl;
    cerr << "will resize the image and put it in myfile.c in jpeg format" << endl;
}

QByteArray convertImageToRaw(const QImage &img)
{
    QImage converted = img.convertToFormat(QImage::Format_RGB888);
    QByteArray data((const char *)converted.bits(), converted.byteCount());
    return data;
}

void writeData(const QByteArray &data, const QString &fileName)
{
    QString variableName = fileName.toLower();
    variableName.replace(QRegExp("\\W"), "_");

    cerr << "Past this into the header : " << endl;
    cerr << "extern const unsigned int " << variableName << "_size;" << endl;
    cerr << "extern const unsigned char " << variableName << "[];" << endl;

    cout << "const unsigned int " << variableName << "_size = " << data.size() << ";" << endl;
    cout << "const unsigned char " << variableName << "[] = {" << endl;

    QString nStr;
    for (int i = 0; i < data.size(); ++i) {
        nStr.setNum((quint8)data[i], 16);
        cout << "0x" << nStr.rightJustified(2, '0') << ",";
        if (i % 14 == 13)
            cout << endl;
    }
    cout << "};" << endl;
}
