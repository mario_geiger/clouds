#include <iostream>
#include "textdrawable.hh"
#include "mount.hh"
#include "potential.hh"
#include "sky.hh"
using namespace std;

			/* Ancienne conception du système, en suivant initialement la donnée de l'exercice P10.
			 * La conception actuelle a été revue à la lumière de la conception générale de notre projet.
			 * Voir les fichiers system.cc, REPONSES.txt & CONCEPTION.txt pour des détails sur la conception finale.
			 * 
			 * Dans cet état antérieur, la classe System ne contentait pour attributs qu'un champ de potentiels, un ciel et une montagne (alors encore une simple gaussienne).
			 * 
			 * Ce code ne compile plus car certaines des méthodes qu'il appelle ont depuis changé de prototype, et certaines classes de conception.
			 */

class System : public TextDrawable { //Classe textview pour l'affichage en mode texte...
	public:
		System(size_t nx, size_t ny, size_t nz, double lambda, double xCenter, double yCenter, double amplitude, double xSpread, double ySpread);
		
		System(const System&) = delete;
		System& operator=(const System&) = delete;
		
		//Getters utiles pour la vue (nuages, terrain)
		const PotentialField& potential_field() const { return _potentialField; }
		const Sky& sky() const { return _sky; }
		const Mount& mount() const { return _mount; }
		
		void init(double plainTemperature, double plainPressure, double humidity, double v, double epsilon, double precision, int max_iter, bool verbal = false, ostream& out = cout);
		void evolve() {}
		
		void draw(std::ostream& out) const override;
		
	private:
		PotentialField _potentialField;
		
		Sky _sky;
		
		Mount _mount;
};

System::System(size_t nx, size_t ny, size_t nz, double lambda, double xCenter, double yCenter, double amplitude, double xSpread, double ySpread)
	: _potentialField(nx, ny, nz, lambda), _sky(nx, ny, nz, lambda), _mount(xCenter, yCenter, amplitude, xSpread, ySpread) {}

void System::init(double plainTemperature, double plainPressure, double humidity, double v, double epsilon, double precision, int max_iter, bool verbal, ostream& out) {
	_potentialField.init(v, _mount);
	_potentialField.solve(epsilon, precision, max_iter, _mount, verbal);
	_sky.initialize(plainTemperature, plainPressure, plainWindSpeed, humidity);
	_sky.setWindSpeeds(_potentialField);
	//if(verbal) draw(out);
	//evolve();
}

void System::draw(ostream& out) const override {
	out << "\tThe system is composed of :" << endl
		
		<< _mount << endl
		
		<< "A potential field :" << endl
		<< _potentialField << endl
		
		<< "A sky :" << endl
		<< _sky << endl;
}
