#include "array3d.hh"

using namespace std;

int main()
{
	// cree un tableau tridimentionel de double et de taille 8x8x1
	Array3D<double> m(8, 8, 1);
	
	// initialise toutes les valeurs a l'aide d'un indice unidimensionnel i
	for (size_t i = 0; i < m.size(); ++i)
		m[i] = 1.0;
	
	// effectue des operations a l'aide des indices tridimensionnels x,y,z
	for (size_t x = 0; x < m.sizex(); ++x)
		m(x,0,0) = x;
	for (size_t y = 0; y < m.sizey(); ++y)
		m(0,y,0) = y;

	for (size_t x = 1; x < m.sizex(); ++x)
	for (size_t y = 1; y < m.sizey(); ++y)		
		m(x,y,0) = m(x,0,0) * m(0,y,0);
	
	// affiche la table de multiplication
	cout << m << endl;
	
	// autre fonctionalitee utile, parcourir le tableau de maniere unidimensionnelle
	// tout en obtenant les indices de position tridimensionnels
	size_t x, y, z;
	for (size_t i = 0; i < m.size(); ++i) {
		double e = m.at(i, x, y, z); // x,y,z sont passe par reference puis modifies
		cout << "(" << x << "," << y << "," << z << ") = " << e << endl;
	}

	return 0;
}
