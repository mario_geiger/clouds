#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <cmath>

#include "random.hh"

using namespace std;

int main()
{
	std::map<int, int> hist;
	for(int n=0; n<100000; ++n) {
		int sum = 0;
		for (int k=0; k<4; ++k)
			sum += rdm::uniformi(1, 6);
		++hist[sum];
	}
	for(auto p : hist) {
		std::cout << std::fixed << std::setprecision(1) << std::setw(2)
				  << p.first << ' ' << std::string(p.second/500, '*') << '\n';
	}
	return 0;
}
