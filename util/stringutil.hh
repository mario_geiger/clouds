#ifndef __STRING_UTIL_HH__
#define __STRING_UTIL_HH__

#include <cstdlib>
#include <string>
#include <vector>

inline std::vector<std::string> split(const std::string& s, char sep)
{
	std::vector<std::string> v;
	size_t i = 0;
	size_t j = s.find(sep);
	while (j != std::string::npos) {
		v.push_back(s.substr(i, j-i));
		i = j + 1;
		j = s.find(sep, i);
	}
	v.push_back(s.substr(i));
	return v;
}

inline std::vector<std::string> splitws(const std::string& s)
{
	std::vector<std::string> v;
	size_t a = 0;
	size_t b = 0;
	for (size_t i = 0; i < s.size(); ++i) {
		while (i < s.size() && isspace(s[i])) ++i;
		a = i;
		while (i < s.size() && !isspace(s[i])) ++i;
		b = i;
		if (a != b)
			v.push_back(s.substr(a, b-a));
	}
	return v;
}

inline double stringtonumber(const std::string& s, bool* ok = nullptr)
{
	char *ptr;
	double value = std::strtod(s.c_str(), &ptr);
	if (ok != nullptr)
		*ok = (*ptr == '\0' && ptr != s.c_str());
	return value;
}

inline int stringtointeger(const std::string& s, bool* ok = nullptr)
{
	char *ptr;
	int value = (int)std::strtol(s.c_str(), &ptr, 10);
	if (ok != nullptr)
		*ok = (*ptr == '\0' && ptr != s.c_str());
	return value;
}

#endif
