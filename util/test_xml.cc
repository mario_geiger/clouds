#include "xml.hh"
#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;

class Test : public SAXXmlReader {
public:
	virtual bool startTagParsed(const std::string& tagname, const std::map<std::string, std::string>& attribs) {
		cout << "start tag : \"" << tagname << "\"" << endl;
		for (auto att : attribs) {
			cout << "attrib    : \"" << att.first << "\" -> \"" << att.second << "\"" << endl; 
		}
		return true;
	}
	virtual bool emptyTagParsed(const std::string& tagname, const std::map<std::string, std::string>& attribs) {
		cout << "empty tag : \"" << tagname << "\"" << endl;
		for (auto att : attribs) {
			cout << "attrib    : \"" << att.first << "\" -> \"" << att.second << "\"" << endl; 
		}
		return true;
	}
	virtual bool endTagParsed(const std::string& tagname) {
		cout << "end tag   : \"" << tagname << "\"" << endl;
		return true;
	}
	virtual bool contentParsed(const std::string& content) {
		cout << "content   : \"" << content << "\"" << endl;
		return true;
	}
	virtual bool commentParsed(const std::string& comment) {
		cout << "comment   : \"" << comment << "\"" << endl;
		return true;
	}
	virtual bool processingInstructionParsed(const std::string& target, const std::string& data) {
		cout << "PI        : \"" << target << "\"" << " with \"" << data << "\"" << endl;
		return true;
	}
	virtual bool cdataSectionParsed(const std::string& data) {
		cout << "CDATA     : \"" << data << "\"" << endl;
		return true;
	}
};

int main()
{
	ifstream file("example.xml");
	if (!file.is_open()) {
		cout << "ouvrir pas pouvoir" << endl;
		return 1;
	}
		
	Test t;
	if (!t.parse(&file)) {
		cout << "return false" << endl;
		cout << t.line() << ":" << t.column() << " " << t.errorString() << endl;
	}
}
