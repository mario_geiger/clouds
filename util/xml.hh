#ifndef __XML2_HH__
#define __XML2_HH__

#include <string>
#include <map>
#include <iostream>
#include <stack>
#include <vector>

/***********************************************************************
 * La classe XmlReader ne fournit qu'un parsage très limité du XML.
 * - supporte que le ASCII
 * - ne supporte pas les doctypedecl
 * - ne supporte pas les CharRef
 * - ne supporte pas les PEReference
 * - ne supporte pas les elementdecl
 * - ne supporte pas les AttlistDecl
 * - ne supporte pas les EntityDecl
 * - parse XMLDecl comme un PI
 * - et sans doute d'autres choses
 * 
 * Liste des choses supportées :
 * + EmptyElemTag
 * + Attribute
 * + Comment
 * + General EntityRef &amp; &lt; &gl; &apos; &quot;
 * + PI
 * + CDSect
 * 
 * Exemple d'un contenu XML supporté :
 * <html>
 * L'Extensible Markup Language (XML, &quot;langage de balisage extensible&quot; en français)<br />
 * Voir sur <a href="google.ch">google</a> pour plus d&apos;infos.
 * </html>
 * 
 * ref:  http://www.w3.org/TR/2008/REC-xml-20081126/
 **********************************************************************/

class SAXXmlReader
{
public:
	bool parse(std::istream* input);
	
	int line() const;
	int column() const;
	
	const std::string& errorString() const;

protected:
	virtual bool startTagParsed(const std::string& tagname, const std::map<std::string, std::string>& attribs) = 0;
	virtual bool emptyTagParsed(const std::string& tagname, const std::map<std::string, std::string>& attribs) = 0;
	virtual bool endTagParsed(const std::string& tagname) = 0;
	
	// supprime les espaces superflux dus par exemple a l'incrémentation
	// appelle contentParsed que si le contenu est non vide
	// exemples:
	//   <tag>   </tag>  n'envera aucun contenu
	//   <tag> lien : <a>...</a> .</tag>  envera "lien : ", "..." puis " ."
	// seuls les éspace du début et de la fin sont supprimés
	virtual bool contentParsed(const std::string& content) = 0;
	virtual bool commentParsed(const std::string& comment) = 0;
	virtual bool processingInstructionParsed(const std::string& target, const std::string& data) = 0;
	virtual bool cdataSectionParsed(const std::string& data) = 0;
	
	std::string _errorString; // last error

private:
	bool parseMisc();
	bool parsePInstr();
	bool parseElement();
	bool parseContent();
	bool parseComment(); // a appeler si on tombe sur "<!"
	bool parseCDSect();
	bool parseEntity(); // a appeler si on tombe sur '&'
	bool parseAttribute();
	bool parseName();
	void eatWs();
	void read(char *str, int n); // lit les n prochain caractères
	
	void getNext();
	void getCharType();
	
	std::istream* _input;
	char _c; // current char
	int _ctype; // last type of _c
	int _line, _column; // current line, column
	char _ent; // last entity parsed
	std::string _string; // last name parsed
	std::map<std::string, std::string> _attribs;
};

// DOM
/* La partie DOM est très minimaliste.
 * L'ordre des elements au sein d'une balise n'est pas pris en compte
 * <a><b/>texte<a> est indissociable à <a>texte<b/><a>
 */
class DOMXmlElement
{
public:
	DOMXmlElement();
	DOMXmlElement(const DOMXmlElement& other);
	DOMXmlElement(DOMXmlElement&& other);
	~DOMXmlElement();
	DOMXmlElement& operator =(DOMXmlElement other);

	const std::string& tagname() const;
	void setTagName(const std::string& tagname);
	void setAttribs(const std::map<std::string, std::string>& attribs);
	
	std::string attribute(const std::string& name, bool* ok = nullptr) const;
	bool attributeValue(const std::string& name, double* val1) const;
	
	// lit deux valeurs dans un attribut séparées par un point-virgule
	bool attributeValue(const std::string& name, double* val1, double* val2) const;
	
	// lit trois valeurs séparées par des points-virgules
	bool attributeValue(const std::string& name, double* val1, double* val2, double* val3) const;
	bool attributeValue(const std::string& name, int* val1) const;
	bool attributeValue(const std::string& name, int* val1, int* val2) const;
	bool attributeValue(const std::string& name, int* val1, int* val2, int* val3) const;
	
	const DOMXmlElement& child(size_t i) const;
	size_t childrenCount() const;
	
	const std::string& cdsect(size_t i) const;
	size_t cdsectCount() const;
	
	const std::string& content(size_t i) const;
	size_t contentCount() const;
	
	void addChild(const DOMXmlElement& elem);
	void addChild(DOMXmlElement&& elem);
	void addCDSect(const std::string& data);
	void addContent(const std::string& content);
private:
	std::string m_tagname;
	std::map<std::string, std::string> m_attribs;
	std::vector<DOMXmlElement*> m_children;
	std::vector<std::string> m_CDSect;
	std::vector<std::string> m_content;
};

class DOMXmlReader : public SAXXmlReader
{
public:
	bool parse(std::istream* input);
	
	const DOMXmlElement& rootElement() const;

	bool startTagParsed(const std::string& tagname, const std::map<std::string, std::string>& attribs) override;
	bool emptyTagParsed(const std::string& tagname, const std::map<std::string, std::string>& attribs) override;
	bool endTagParsed(const std::string& tagname) override;
	bool contentParsed(const std::string& content) override;
	bool commentParsed(const std::string& comment) override;
	bool processingInstructionParsed(const std::string& target, const std::string& data) override;
	bool cdataSectionParsed(const std::string& data) override;

private:
	std::stack<DOMXmlElement> m_current;
};

#endif
