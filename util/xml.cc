#include "xml.hh"
#include <cstring>
#include "stringutil.hh"

/* Parsage basé sur le principe de la machine de Turing
 *
 */

using namespace std;

enum CharType {NameBegin = 0x1, NameNotBegin = 0x2, WhiteSpace = 0x4, Unknown = 0x8};

bool SAXXmlReader::parse(std::istream* input)
{
	// init states
	_input = input;
	_column = 0;
	_line = 1;
	_errorString.clear();
	getNext();

	//!TODO XMLDecl
	// des commentaires, des PIs
	if (!parseMisc())
		return false;
	// on est tombé sur un Elem
	if (!parseElement())
		return false;
	// il reste peu etre des commentaires et des PIs
	if (!parseMisc())
		return false;
	if (!_input->eof()) {
		_errorString = "expecting: eof. Maybe there is no root element";
		return false;
	}
	return true;
}

int SAXXmlReader::line() const
{
	return _line;
}

int SAXXmlReader::column() const
{
	return _column;
}

const std::string& SAXXmlReader::errorString() const
{
	return _errorString;
}

// commence n'importe ou et finit a un Elem ou EOF
bool SAXXmlReader::parseMisc()
{
	const int statInit = 0;
	const int statWs   = 1;
	const int statLt   = 2;
	const int statPI   = 3;
	const int statCom  = 4;
	const int statDone = 5;
	
	const int inpWs  = 0;
	const int inpLt  = 1; // <
	const int inpQm  = 2; // ?
	const int inpEm  = 3; // !
	const int inpBeg = 4; // Begi type
	const int inpUnk = 5;
	const int inpEOF = 6;
	
	static const int table[5][7] = {
	//    inpWs   inpLt    inpQm     inpEm     inpBeg    inpUnk   inpEOF
		{ statWs, statLt,  -1,       -1,       -1,       -1,      statDone }, // statInit
		{ statWs, statLt,  -1,       -1,       -1,       -1,      statDone }, // statWs
		{ -1,     -1,      statPI,   statCom,  statDone, -1,      -1       }, // statLt
		{ statWs, statLt,  -1,       -1,       -1,       -1,      statDone }, // statPI (same as Init)
		{ statWs, statLt,  -1,       -1,       -1,       -1,      statDone }, // statCom (same as Init)
	};
	
	int state = statInit;
	int input = 0;
	
	for (;;) {
		getCharType();
		if (_input->eof())
			input = inpEOF;
		else if (_c == '<')
			input = inpLt;
		else if (_c == '?')
			input = inpQm;
		else if (_c == '!')
			input = inpEm;
		else if (_ctype & NameBegin)
			input = inpBeg;
		else if (_ctype & WhiteSpace)
			input = inpWs;
		else 
			input = inpUnk;
		
		state = table[state][input];
		
		switch (state) {
			case -1:
				_errorString = "unexpected character when parsing Misc: ";
				_errorString += _c;
				_errorString += '\'';
				return false;
			case statWs:
			case statLt:
				getNext();
				break;
			case statPI:
				getNext();
				if (!parsePInstr())
					return false;
				break;
			case statCom:
				getNext();
				if (!parseComment())
					return false;
				break;
			case statDone:
				// EOF ou Elem
				return true;
		}
	}
	return false;
}

// commence après un "<?" et finit apres "?>"
bool SAXXmlReader::parsePInstr()
{
	const int statInit = 0;
	const int statTarg = 1;
	const int statWs   = 2;
	const int statData = 3;
	const int statQm   = 4;
	const int statQm2  = 5;
	const int statDone = 6;
	
	const int inpBeg = 0; // Begi type
	const int inpNBe = 1; // NotB type
	const int inpWs  = 2; // 
	const int inpQm  = 3; // ?
	const int inpGt  = 4; // >
	const int inpUnk = 5;
	
	static const int table[6][6] = {
	//    inpBeg     inpNBe     inpWs     inpQm    inpGt     inpUnk
		{ statTarg,  -1,        -1,       -1,      -1,       -1       }, // statInit
		{ -1,        -1,        statWs,   statQm2, -1,       -1       }, // statTarg
		{ statData,  statData,  statWs,   statQm,  statData, statData }, // statWs
		{ statData,  statData,  statData, statQm,  statData, statData }, // statData
		{ statData,  statData,  statData, statQm,  statDone, statData }, // statQm
		{ -1,        -1,        -1,       -1,      statDone, -1       }, // statQm2
	};
	
	int state = statInit;
	int input = 0;
	
	string target;
	string data;
	
	for (;;) {
		if (_input->eof()) {
			_errorString = "unexpected end";
			return false;
		}
		getCharType();
		if (_ctype & NameBegin)
			input = inpBeg;
		else if (_ctype & NameNotBegin)
			input = inpNBe;
		else if (_c == '?')
			input = inpQm;
		else if (_c == '>')
			input = inpGt;
		else if (_ctype & WhiteSpace)
			input = inpWs;
		else 
			input = inpUnk;
		
		state = table[state][input];
		
		switch (state) {
			case -1:
				_errorString = "unexpected character when parsing Processing instruction: '";
				_errorString += _c;
				_errorString += '\'';
				return false;
			case statTarg:
				if (!parseName())
					return false;
				target = _string;
				break;
			case statWs:
			case statQm2:
				getNext();
				break;
			case statQm:
			case statData:
				data.push_back(_c);
				getNext();
				break;
			case statDone:
				if (data.back() == '?')
					data.pop_back(); // remove the '?'
				// _c == '>'
				if (!processingInstructionParsed(target, data))
					return false;
				getNext();
				return true;
		}
	}
	return false;
}

// commence apres un "<" finit apres un ">"
bool SAXXmlReader::parseElement()
{
	const int statInit   = 0; // lit le permier char
	const int statSTag   = 1; // lit le start tag
	const int statWs1    = 2; // espace apres le tag
	const int statAttr   = 3; // ignore les attributs
	const int statEmpty  = 4; // balise vide
	const int statSTagGt = 5; // lance parseContent, quand c fini on a _c == '/' et on fait getnext
	const int statETag   = 6; // lit le end tag
	const int statWs2    = 7; // espaces apres le end tag
	const int statETagGt = 8; // verifie le tag
	const int statDone   = 9;

	const int inpBeg   = 0; 
	const int inpNBe   = 1;
	const int inpSlash = 2;
	const int inpGt    = 3;
	const int inpWs    = 4;
	const int inpUnk   = 5;
	
	static const int table[10][6] = {
	//    inpBeg    inpNBe   inpSlash     inpGt        inpWs    inpUnk
		{ statSTag,  -1,       -1,        -1,          -1,      -1       }, // statInit
		{ -1,        -1,       statEmpty, statSTagGt,  statWs1, -1       }, // statSTag
		{ statAttr,  -1,       statEmpty, statSTagGt,  statWs1, -1       }, // statWs1
		{ statAttr,  -1,       statEmpty, statSTagGt,  statWs1, -1       }, // statAttr
		{ -1,        -1,       -1,        statDone,    -1,      -1       }, // statEmpty
		{ statETag,  -1,       -1,        -1,          -1,      -1       }, // statSTagGt
		{ -1,        -1,       -1,        statETagGt,  statWs2, -1       }, // statETag
		{ -1,        -1,       -1,        statETagGt,  statWs2, -1       }, // statWs2
		{ -1,        -1,       -1,        -1,          -1,      -1       }, // statETagGt
		{ -1,        -1,       -1,        -1,          -1,      -1       }, // statDone
	};
	
	int state = statInit;
	int input = 0;
	
	string sTag, eTag;
	_attribs.clear();
	
	for (;;) {
		if (_input->eof()) {
			_errorString = "unexpected end";
			return false;
		}
		
		getCharType();
		if (_ctype & NameBegin)
			input = inpBeg;
		else if (_ctype & NameNotBegin)
			input = inpNBe;
		else if (_c == '/')
			input = inpSlash;
		else if (_c == '>')
			input = inpGt;
		else if (_ctype & WhiteSpace)
			input = inpWs;
		else 
			input = inpUnk;
			
		state = table[state][input];
		
		switch (state) {
			case -1:
				_errorString = "unexpected character when parsing element: ";
				_errorString += _c;
				_errorString += '\'';
				return false;
			case statSTag:
				if (!parseName())
					return false;
				sTag = _string;
				break;
			case statETag:
				if (!parseName())
					return false;
				eTag = _string;
				break;
			case statWs1:
			case statWs2:
				getNext();
				break;
			case statAttr:
				if (!parseAttribute())
					return false;
				break;
			case statEmpty:
				if (!emptyTagParsed(sTag, _attribs))
					return false;
				getNext();
				break;
			case statSTagGt:
				getNext();
				if (!startTagParsed(sTag, _attribs) || !parseContent())
					return false;
				// ici on est après un "</"
				break;
			case statETagGt:
				if (sTag != eTag) {
					_errorString = "starttag endtag not matching: ";
					_errorString += sTag + " != " + eTag;
					return false;
				}
				if (!endTagParsed(eTag))
					return false;
			case statDone:
				// ici on est sur le ">" du endtag ou du emptytag
				getNext();
				return true;
		}
	}
	return false;
}

// commence apres un ">" fini apres un "</"
bool SAXXmlReader::parseContent()
{
	const int statInit   = 0; // lit le permier char
	const int statWs     = 1; // save un ' ' meme si c'est un '\n'
	const int statWsR    = 2; // ne fait rien
	const int statCont   = 3; // lit le contenu
	const int statLt     = 4; // on a lut un '<'
	const int statCom    = 5; // on est dans un commentaire
	const int statEnt    = 6; // on a lut un '&'
	const int statElem   = 7; // on a un Start Tag
	const int statPI     = 8; // on est a un PI
	const int statCDSect = 9; // on est dans une CDSect
	const int statEm     = 10; // on est à '<!'
	const int statDone   = 11; // on est au End Tag

	const int inpAmp   = 0; // &
	const int inpLt    = 1; // <
	const int inpSlash = 2; // /
	const int inpEm    = 3; // !
	const int inpQm    = 4; // ?
	const int inpOpenB = 5; // [
	const int inpDash  = 6; // -
	const int inpWs    = 7;
	const int inpUnk   = 8;

	static const int table[11][9] = {
	//    inpAmp    inpLt      inpSlash     inpEm        inpQm      inpOpenB    inpDash    inpWs     inpUnk
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWsR,  statCont }, // statInit
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWsR,  statCont }, // statWs
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWsR,  statCont }, // statWsR
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWs,   statCont }, // statCont
		{ -1,       -1,        statDone,    statEm,      statPI,    -1,         -1,        -1,       statElem }, // statLt
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWs,   statCont }, // statCom (same as statCont)
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWs,   statCont }, // statEnt (same as statCont)
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWs,   statCont }, // statElem (same as statCont)
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWs,   statCont }, // statPI (same as statCont)
		{ statEnt,  statLt,    statCont,    statCont,    statCont,  statCont,   statCont,  statWs,   statCont }, // statCDSect (same as statCont)
		{ -1,       -1,        -1,          -1,          -1,        statCDSect, statCom,   -1,      -1        }, // statEm
	};

	int state = statInit;
	int input = 0;

	string con;
	
	for (;;) {
		if (_input->eof()) {
			_errorString = "unexpected end";
			return false;
		}
		
		getCharType();
		if (_c == '&')
			input = inpAmp;
		else if (_c == '<')
			input = inpLt;
		else if (_c == '/')
			input = inpSlash;
		else if (_c == '!')
			input = inpEm;
		else if (_c == '?')
			input = inpQm;
		else if (_c == '[')
			input = inpOpenB;
		else if (_c == '-')
			input = inpDash;
		else if (_ctype & WhiteSpace)
			input = inpWs;
		else 
			input = inpUnk;
			
		state = table[state][input];
		
		switch (state) {
			case -1:
				_errorString = "unexpected character when parsing content: '";
				_errorString += _c;
				_errorString += '\'';
				return false;
			case statWs:
				con.push_back(' ');
			case statWsR:
				getNext();
				break;
			case statCont:
				con.push_back(_c);
				getNext();
				break;
			case statLt:
				getNext();
				break;
			case statCom:
				if (!con.empty() && !contentParsed(con))
					return false;
				con.clear();
				getNext();
				if (!parseComment())
					return false;
				// ici on est apres un "-->"
				break;
			case statEnt:
				getNext();
				if (!parseEntity())
					return false;
				con.push_back(_ent);
				break;
			case statElem:
				if (!con.empty() && !contentParsed(con))
					return false;
				con.clear();
				// ici on est deja apres le '<'
				if (!parseElement())
					return false;
				// ici on est apres '>'
				break;
			case statPI:
				if (!con.empty() && !contentParsed(con))
					return false;
				con.clear();
				getNext();
				if (!parsePInstr())
					return false;
				break;
			case statCDSect:
				if (!con.empty() && !contentParsed(con))
					return false;
				con.clear();
				getNext();
				if (!parseCDSect())
					return false;
				// ici on est apres un "]]>"
				break;
			case statEm:
				getNext();
				break;
			case statDone:
				// _c == '/'
				if (con.back() == ' ')
					con.pop_back();
				if (!con.empty() && !contentParsed(con))
					return false;
				getNext();
				return true;
		}
	}
	return false;
}

// commence apres un "<!-" finit apres un "-->"
bool SAXXmlReader::parseComment()
{
	const int statInit  = 0; 
	const int statDash1 = 1; // lire '-'
	const int statCom   = 2; // lit commentaire
	const int statComD  = 3; // on a lu un '-' dans le commentaire
	const int statComE  = 4; // on a lu deux '-'
	const int statDone  = 5;
	
	const int inpDash = 0; // -
	const int inpGt   = 1; // >
	const int inpUnk  = 2;
	
	static const int table[7][3] = {
	//    inpDash    inpGt      inpUnk    
		{ statDash1,  -1,        -1      }, // statInit
		{ statComD,   statCom,   statCom }, // statDash1
		{ statComD,   statCom,   statCom }, // statCom
		{ statComE,   statCom,   statCom }, // statComD
		{ -1,         statDone,  -1      }, // statComE
		{ -1,         -1,        -1      }, // statDone
	};
	
	int state = statInit;
	int input = 0;
	
	string com;

	for (;;) {
		if (_input->eof()) {
			_errorString = "unexpected end";
			return false;
		}

		if (_c == '-')
			input = inpDash;
		else if (_c == '>')
			input = inpGt;
		else
			input = inpUnk;
			
		state = table[state][input];

		switch (state) {
			case -1:
				_errorString = "unexpected character when parsing comment: '";
				_errorString += _c;
				_errorString += '\'';
				return false;
			case statCom:
			case statComD:
				com.push_back(_c);
			case statDash1:
				getNext();
				break;
			case statComE:
				com.pop_back(); // enleve le '-' qu'a mit statComD
				if (!commentParsed(com))
					return false;
				getNext();
				break;
			case statDone:
				getNext();
				return true;
		}
	}
	return false;
}

// commence apres "<![" finit apres ']]>'
bool SAXXmlReader::parseCDSect()
{
	const int statInit  = 0;
	const int statCDATA = 1;
	const int statData  = 2;
	const int statEnd1  = 3;
	const int statEnd2  = 4;
	const int statDone  = 5;
	
	const int inpCloseB = 0; // ]
	const int inpGt     = 1; // >
	const int inpUnk    = 2;
	
	static const int table[5][3] = {
	//    inpCloseB  inpGt      inpUnk
		{ -1,        -1,        statCDATA }, // statInit
		{ statEnd1,  statData,  statData  }, // statCDATA
		{ statEnd1,  statData,  statData  }, // statData
		{ statEnd2,  statData,  statData  }, // statEnd1
		{ statEnd2,  statDone,  statData  }, // statEnd2
	};
	
	int state = statInit;
	int input = 0;
	
	char cdata[7];
	string data;
	
	for (;;) {
		if (_input->eof()) {
			_errorString = "unexpected end";
			return false;
		}
		if (_c == ']')
			input = inpCloseB;
		else if (_c == '>')
			input = inpGt;
		else
			input = inpUnk;
		
		state = table[state][input];
		
		switch (state) {
			case -1:
				_errorString = "unexpected character when parsing CDSect: '";
				_errorString += _c;
				_errorString += '\'';
				return false;
			case statCDATA:
				// parse 'CDATA['
				cdata[5] = '\0'; // if eof
				read(cdata, 6);
				cdata[6] = '\0';
				if (strcmp(cdata, "CDATA[") != 0) {
					_errorString = "expected 'CDATA[' after the occurrence of '<![': '";
					_errorString += cdata;
					_errorString += "' readed";
					return false;
				}
			case statData:
			case statEnd1:
			case statEnd2:
				data.push_back(_c);
				getNext();
				break;
			case statDone:
				data.pop_back();
				data.pop_back(); // remove ']]'
				if (!cdataSectionParsed(data))
					return false;
				getNext();
				return true;
		}
	}
	return false;
}

// commence apres un "&" finit apres un ";"
bool SAXXmlReader::parseEntity()
{
	/*
	 * &lt;     <   less than
	 * &gt;     >   greater than
	 * &amp;    &   ampersand 
	 * &apos;   '   apostrophe
	 * &quot;   "   quotation mark
	 * */
	
	const int statInit = 0;
	const int statRead = 1;
	const int statDone = 2;
	
	const int inpOk   = 0; // ltgamposqu
	const int inpSemi = 1; // ;
	const int inpUnk  = 2; 

	static const int table[3][3] = {
	//    inpOk    inpSemi      inpUnk    
		{ statRead,  -1,        -1      }, // statInit
		{ statRead,  statDone,  -1      }, // statRead
		{ -1,        -1,        -1      }, // statDone
	};

	int state = statInit;
	int input = 0;
	
	char str[5];
	int pos = 0;
	
	for (;;) {
		if (_input->eof()) {
			_errorString = "unexpected end";
			return false;
		}

		if (_c == ';')
			input = inpSemi;
		else if (strchr("ltgamposqu", _c) != nullptr)
			input = inpOk;
		else
			input = inpUnk;
			
		state = table[state][input];

		switch (state) {
			case -1:
				_errorString = "unexpected character when parsing entity: '";
				_errorString += _c;
				_errorString += '\'';
				return false;
			case statRead:
				if (pos >= 4) {
					_errorString = "bad entity: entity Name too long";
					return false;
				}
				str[pos++] = _c;
				getNext();
				break;
			case statDone:
				str[pos] = '\0';
				if (strcmp(str, "lt") == 0)
					_ent = '<';
				else if (strcmp(str, "gt") == 0)
					_ent = '>';
				else if (strcmp(str, "amp") == 0)
					_ent = '&';
				else if (strcmp(str, "apos") == 0)
					_ent = '\'';
				else if (strcmp(str, "quot") == 0)
					_ent = '\"';
				else {
					_errorString = "bad entity: ";
					_errorString += '\'';
					_errorString += str;
					_errorString += "' dont match with ('lt' | 'gt' | 'amp' | 'apos' | 'quot')";
					return false;
				}
				getNext();
				return true;
		}
	}
}

// commence avec un Begi termine apres un ' ou "
bool SAXXmlReader::parseAttribute()
{
	if (!parseName())
		return false;
	getCharType();
	if (_ctype & WhiteSpace)
		eatWs();
	if (_c != '=') {
		_errorString = "expected '='";
		return false;
	}
	getNext();
	getCharType();
	if (_ctype & WhiteSpace)
		eatWs();
	char sep;
	switch (_c) {
		case '\'':
		case '\"':
			sep = _c;
			break;
		default:
			_errorString = "expected (\' | \")";
			return false;
	}
	getNext();
	string _attValue;
	while (_c != sep) {
		if (_c == '<') {
			_errorString = "unexpected char : '<' not allowed in AttValue";
			return false;
		}
		if (_c == '&') {
			getNext();
			if (!parseEntity())
				return false;
			_attValue.push_back(_ent);
		} else {
			_attValue.push_back(_c);
			getNext();
		}
		if (_input->eof()) {
			_errorString = "unexpected end";
			return false;
		}
	}
	
	_attribs[_string] = _attValue;
	
	getNext();
	return true;
}

// commence sur un Begi fini apres le Name
bool SAXXmlReader::parseName()
{
	_string.clear();
	do {
		_string.push_back(_c);
		getNext();
		if (_input->eof()) {
			_errorString = "unexpected end";
			return false;
		}
		getCharType();
	} while (_ctype & NameNotBegin);
	return true;
}

// commence sur un WSpa fini apres les WSpa peu etre EOF
void SAXXmlReader::eatWs()
{
	do {
		getNext();
		getCharType();
	} while (!_input->eof() && _ctype & WhiteSpace);
}

// lit le caractère courrant et les (n-1) suivants, termine après
void SAXXmlReader::read(char *str, int n)
{
	do {
		*str++ = _c;
		getNext();
	} while (!_input->eof() && --n);
}

void SAXXmlReader::getNext()
{
	_c = _input->get();
	
	if (_c == '\n') {
		_line++;
		_column = -1;
	}
	_column++; // 1st char of a line => column = 1
}

void SAXXmlReader::getCharType()
{
	constexpr int NBNB = NameBegin | NameNotBegin;
	constexpr int NotB = NameNotBegin;
	constexpr int WSpa = WhiteSpace;
	constexpr int Unkn = Unknown;
	constexpr int charTypeTable[128] = {
		// 0-7
		Unkn, Unkn, Unkn, Unkn, Unkn, Unkn, Unkn, Unkn,
		// 8-15   TAB(9), LF(10), CR(13)
		Unkn, WSpa, WSpa, Unkn, Unkn, WSpa, Unkn, Unkn,
		// 16-23
		Unkn, Unkn, Unkn, Unkn, Unkn, Unkn, Unkn, Unkn,
		// 24-31
		Unkn, Unkn, Unkn, Unkn, Unkn, Unkn, Unkn, Unkn,
		// 32-39
		WSpa, Unkn, Unkn, Unkn, Unkn, Unkn, Unkn, Unkn,
		// 40-47   -(45) .(46)
		Unkn, Unkn, Unkn, Unkn, Unkn, NotB, NotB, Unkn,
		// 48-55    [0,9]  :(58)
		NotB, NotB, NotB, NotB, NotB, NotB, NotB, NotB,
		// 56-63
		NotB, NotB, NBNB, Unkn, Unkn, Unkn, Unkn, Unkn,
		// 64-71    [A,G]
		Unkn, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB,
		// 72-79    [H-O]
		NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB,
		// 80-87    [P-W]
		NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB,
		// 88-95    [X-Z]    _(95)
		NBNB, NBNB, NBNB, Unkn, Unkn, Unkn, Unkn, NBNB,
		// 96-103   [a,g]
		Unkn, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB,
		// 104-111  [h,o]
		NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB,
		// 112-119  [p,w]
		NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB, NBNB,
		// 120-127  [x,z]
		NBNB, NBNB, NBNB, Unkn, Unkn, Unkn, Unkn, Unkn,
	};

	if (!(_c & ~0x7f))
		_ctype = charTypeTable[int(_c)];
	else
		_ctype = Unkn;
}


/***********************************************************************
 * 
 **********************************************************************/

DOMXmlElement::DOMXmlElement()
{
}

DOMXmlElement::DOMXmlElement(const DOMXmlElement& other) :
	m_tagname(other.m_tagname),
	m_attribs(other.m_attribs),
	m_CDSect(other.m_CDSect)
{
	for (auto ptr : other.m_children)
		m_children.push_back(new DOMXmlElement(*ptr));
}

DOMXmlElement::DOMXmlElement(DOMXmlElement&& other) :
	m_tagname(other.m_tagname),
	m_attribs(other.m_attribs),
	m_children(other.m_children),
	m_CDSect(other.m_CDSect)
{
	other.m_children.clear();
}

DOMXmlElement::~DOMXmlElement()
{
	for (auto ptr : m_children)
		delete ptr;
}

DOMXmlElement& DOMXmlElement::operator =(DOMXmlElement other)
{
	using std::swap; // enable ADL
	swap(m_tagname, other.m_tagname);
	swap(m_attribs, other.m_attribs);
	swap(m_children, other.m_children);
	swap(m_CDSect, other.m_CDSect);
	return *this;
}

const std::string& DOMXmlElement::tagname() const
{
	return m_tagname;
}

void DOMXmlElement::setTagName(const std::string& tagname)
{
	m_tagname = tagname;
}

void DOMXmlElement::setAttribs(const std::map<std::string, std::string>& attribs)
{
	m_attribs = attribs;
}

std::string DOMXmlElement::attribute(const std::string& name, bool* ok) const
{
	if (ok) *ok = true;
	auto it = m_attribs.find(name);
	if (it != m_attribs.end())
		return it->second;
	if (ok) *ok = false;
	return "";
}

bool DOMXmlElement::attributeValue(const std::string& name, double* val1) const
{
	bool ok;
	string a = attribute(name, &ok);
	if (!ok) return false;
	*val1 = stringtonumber(a, &ok);
	return ok;
}

bool DOMXmlElement::attributeValue(const std::string& name, double* val1, double* val2) const
{
	bool ok;
	vector<string> v = split(attribute(name, &ok), ';');
	if (!ok) return false;
	if (v.size() != 2) return false;
	*val1 = stringtonumber(v[0], &ok);
	if (!ok) return false;
	*val2 = stringtonumber(v[1], &ok);
	return ok;
}

bool DOMXmlElement::attributeValue(const std::string& name, double* val1, double* val2, double* val3) const
{
	bool ok;
	vector<string> v = split(attribute(name, &ok), ';');
	if (!ok) return false;
	if (v.size() != 3) return false;
	*val1 = stringtonumber(v[0], &ok);
	if (!ok) return false;
	*val2 = stringtonumber(v[1], &ok);
	if (!ok) return false;
	*val3 = stringtonumber(v[2], &ok);
	return ok;
}

bool DOMXmlElement::attributeValue(const std::string& name, int* val1) const
{
	bool ok;
	string a = attribute(name, &ok);
	if (!ok) return false;
	*val1 = stringtointeger(a, &ok);
	return ok;
}

bool DOMXmlElement::attributeValue(const std::string& name, int* val1, int* val2) const
{
	bool ok;
	vector<string> v = split(attribute(name, &ok), ';');
	if (!ok) return false;
	if (v.size() != 2) return false;
	*val1 = stringtointeger(v[0], &ok);
	if (!ok) return false;
	*val2 = stringtointeger(v[1], &ok);
	return ok;
}

bool DOMXmlElement::attributeValue(const std::string& name, int* val1, int* val2, int* val3) const
{
	bool ok;
	vector<string> v = split(attribute(name, &ok), ';');
	if (!ok) return false;
	if (v.size() != 3) return false;
	*val1 = stringtointeger(v[0], &ok);
	if (!ok) return false;
	*val2 = stringtointeger(v[1], &ok);
	if (!ok) return false;
	*val3 = stringtointeger(v[2], &ok);
	return ok;
}

const DOMXmlElement& DOMXmlElement::child(size_t i) const
{
	return *m_children[i];
}

size_t DOMXmlElement::childrenCount() const
{
	return m_children.size();
}

const std::string& DOMXmlElement::cdsect(size_t i) const
{
	return m_CDSect[i];
}

size_t DOMXmlElement::cdsectCount() const
{
	return m_CDSect.size();
}

const std::string& DOMXmlElement::content(size_t i) const
{
	return m_content[i];
}

size_t DOMXmlElement::contentCount() const
{
	return m_content.size();
}

void DOMXmlElement::addChild(const DOMXmlElement& elem)
{
	m_children.push_back(new DOMXmlElement(elem));
}

void DOMXmlElement::addChild(DOMXmlElement&& elem)
{
	m_children.push_back(new DOMXmlElement(std::move(elem)));
}

void DOMXmlElement::addCDSect(const std::string& data)
{
	m_CDSect.push_back(data);
}

void DOMXmlElement::addContent(const std::string& content)
{
	m_content.push_back(content);
}


bool DOMXmlReader::parse(std::istream* input)
{
	while (!m_current.empty()) m_current.pop();
	m_current.push(DOMXmlElement()); // element document
	
	if (!SAXXmlReader::parse(input))
		return false;
		
	// normalement m_current contient qu'un element qui lui meme contient que l'element racine
	return true;
}

const DOMXmlElement& DOMXmlReader::rootElement() const
{
	return m_current.top();
}

bool DOMXmlReader::startTagParsed(const std::string& tagname, const std::map<std::string, std::string>& attribs) 
{
	DOMXmlElement elem;
	elem.setTagName(tagname);
	elem.setAttribs(attribs);
	m_current.push(elem);
	return true;
}

bool DOMXmlReader::emptyTagParsed(const std::string& tagname, const std::map<std::string, std::string>& attribs) 
{
	DOMXmlElement elem;
	elem.setTagName(tagname);
	elem.setAttribs(attribs);
	m_current.top().addChild(elem);
	return true;
}

bool DOMXmlReader::endTagParsed(const std::string& ) 
{
	DOMXmlElement elem = m_current.top();
	m_current.pop();
	m_current.top().addChild(elem);
	return true;
}

bool DOMXmlReader::contentParsed(const std::string& content) 
{
	m_current.top().addContent(content);
	return true;
}

bool DOMXmlReader::commentParsed(const std::string& /*comment*/) 
{
	//!TODO
	return true;
}

bool DOMXmlReader::processingInstructionParsed(const std::string& /*target*/, const std::string& /*data*/) 
{
	//!TODO
	return true;
}

bool DOMXmlReader::cdataSectionParsed(const std::string& data) 
{
	m_current.top().addCDSect(data);
	return true;
}
