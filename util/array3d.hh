#ifndef __ARRAY3D_H__
#define __ARRAY3D_H__

#include <iostream>
#include <new>
#include <stdexcept>

/* La classe Array3D est un conteneur avec 3 indices (x, y, z)
 * 
 * Exemple
 *  Array3D<double> m; // cree un array vide
 *  m.resize(10, 10, 10); // redimensionne
 *  m(3, 4, 5) = 10.0; // modifie la valeur en x=3, y=4, z=5
 * 
 * Les donnees sont accesible sous forme :
 *  + un tableau tridimensionnel -> m(x, y, z)
 *  + un tableau unidimensionnel -> m[i] oubien m.at(i, &x, &y, &z)
 */ 
template<typename T>
class Array3D
{
public:
	// initialise un tableau vide
	Array3D();
	// initialise un tableau avec le constructeur pas defaut de T 
	Array3D(size_t sizex, size_t sizey, size_t sizez, T def = T());

	virtual ~Array3D();
	Array3D(const Array3D<T>& other);
	Array3D(Array3D<T>&& other);
	Array3D<T> &operator =(Array3D<T> other);
	
	// redimentionne le tableau, les anciennes valeurs sont conservees
	void resize(size_t sizex, size_t sizey, size_t sizez);
	
	// retourne l'element (x, y, z). les indices doivent etre positif et ne pas depasser les tailles
	const T &at(size_t x, size_t y, size_t z) const;
	T &at(size_t x, size_t y, size_t z);

	// version avec operateur
	const T &operator()(size_t x, size_t y, size_t z) const;
	T &operator()(size_t x, size_t y, size_t z);
	
	// retourne l'element i du tableau unidimensionnel, pratique pour appliquer une operation qui ne depend pas de la position
	const T &at(size_t i) const;
	T &at(size_t i);

	// version avec operateur
	const T &operator[](size_t i) const;
	T &operator[](size_t i);	
	
	// retourne l'element i du tableau et modifie les referances x, y, z avec les indices de l'element retourne
	const T &at(size_t i, size_t &x, size_t &y, size_t &z) const;
	T &at(size_t i, size_t &x, size_t &y, size_t &z);

	// la taille du tableau unidimensionnel
	size_t size() const;
	
	// les trois tailles du tableau tridimensionnel
	size_t sizex() const;
	size_t sizey() const;
	size_t sizez() const;
	
	// les donnes brut du tableau
	const T* data() const;
	T* data();

private:
	inline constexpr size_t xyz2i(size_t x, size_t y, size_t z, size_t sx, size_t sy) const;
	inline void i2xyz(size_t i, size_t sx, size_t sy, size_t sz, size_t &x, size_t &y, size_t &z) const;
	
	void checkI(size_t i, size_t si) const;
	void checkXYZ(size_t x, size_t y, size_t z, size_t sx, size_t sy, size_t sz) const;

	T *_d;
	size_t _sx, _sy, _sz;
	size_t _si; // = _sx * _sy * _sz
};

template<typename T>
std::ostream &operator <<(std::ostream &out, const Array3D<T> &m);

template<typename T>
Array3D<T>::Array3D() :
	_d(nullptr), 
	_sx(0),
	_sy(0),
	_sz(0),
	_si(0)
{
}

template<typename T>
Array3D<T>::Array3D(size_t sizex, size_t sizey, size_t sizez, T def) :
	_sx(sizex),
	_sy(sizey),
	_sz(sizez),
	_si(sizex * sizey * sizez)
{
	_d = new T[_si];
	for (size_t i = 0; i < _si; ++i)
		_d[i] = def;
}

template<typename T>
Array3D<T>::~Array3D()
{
	delete[] _d;
}

template<typename T>
Array3D<T>::Array3D(const Array3D<T>& other) :
	_sx(other._sx),
	_sy(other._sy),
	_sz(other._sz),
	_si(other._sx * other._sy * other._sz)
{
	_d = new T[_si];
	for (size_t i = 0; i < _si; ++i)
		_d[i] = other._d[i];
}

template<typename T>
Array3D<T>::Array3D(Array3D<T>&& other) : 
	_sx(other._sx),
	_sy(other._sy),
	_sz(other._sz),
	_si(other._sx * other._sy * other._sz)
{
	_d = other._d;
	other._d = nullptr;
}

template<typename T>
Array3D<T> &Array3D<T>::operator =(Array3D<T> other)
{
	using std::swap;
	_sx = other._sx;
	_sy = other._sy;
	_sz = other._sz;
	_si = other._si;
	swap(_d, other._d);
	return *this;
}

template<typename T>
void Array3D<T>::resize(size_t sx, size_t sy, size_t sz)
{
	size_t si = sx * sy * sz;
	T *d = new T[si];
	
	// copie les anciennes valeurs
	for (size_t x = 0; x < sx && x < _sx; ++x) {
		for (size_t y = 0; y < sy && y < _sy; ++y) {
			for (size_t z = 0; z < sz && z < _sz; ++z) {
				d[xyz2i(x, y, z, sx, sy)] = _d[xyz2i(x, y, z, _sx, _sy)];
			}
		}
	}
	
	// initialise les autres valeurs avec les constructeur par defaut de T()
	for (size_t x = _sx; x < sx; ++x) {
		for (size_t y = _sy; y < sy; ++y) {
			for (size_t z = _sz; z < sz; ++z) {
				d[xyz2i(x, y, z, sx, sy)] = T();
			}
		}
	}

	delete[] _d; // Note, however, that deleting a null pointer has no effect
	_d = d;
	_sx = sx;
	_sy = sy;
	_sz = sz;
	_si = si;
}

template<typename T>
const T &Array3D<T>::at(size_t x, size_t y, size_t z) const
{
	checkXYZ(x, y, z, _sx, _sy, _sz);
	return _d[xyz2i(x, y, z, _sx, _sy)];
}

template<typename T>
T &Array3D<T>::at(size_t x, size_t y, size_t z)
{
	checkXYZ(x, y, z, _sx, _sy, _sz);
	return _d[xyz2i(x, y, z, _sx, _sy)];
}

template<typename T>
const T &Array3D<T>::operator()(size_t x, size_t y, size_t z) const
{
	return at(x, y, z);
}

template<typename T>
T &Array3D<T>::operator()(size_t x, size_t y, size_t z)
{
	return at(x, y, z);
}

template<typename T>
const T &Array3D<T>::at(size_t i) const
{
	checkI(i, _si);
	return _d[i];
}

template<typename T>
T &Array3D<T>::at(size_t i)
{
	checkI(i, _si);
	return _d[i];
}

template<typename T>
const T &Array3D<T>::operator[](size_t i) const
{
	return at(i);
}

template<typename T>
T &Array3D<T>::operator[](size_t i)
{
	return at(i);
}

template<typename T>
const T &Array3D<T>::at(size_t i, size_t &x, size_t &y, size_t &z) const
{
	checkI(i, _si);
	i2xyz(i, _sx, _sy, _sz, x, y, z);
	return _d[i];
}

template<typename T>
T &Array3D<T>::at(size_t i, size_t &x, size_t &y, size_t &z)
{
	checkI(i, _si);
	i2xyz(i, _sx, _sy, _sz, x, y, z);
	return _d[i];
}

template<typename T>
const T* Array3D<T>::data() const
{
	return _d;
}

template<typename T>
T* Array3D<T>::data()
{
	return _d;
}

template<typename T>
size_t Array3D<T>::size() const
{
	return _si;
}

template<typename T>
size_t Array3D<T>::sizex() const
{
	return _sx;
}

template<typename T>
size_t Array3D<T>::sizey() const
{
	return _sy;
}

template<typename T>
size_t Array3D<T>::sizez() const
{
	return _sz;
}

template<typename T>
inline constexpr size_t Array3D<T>::xyz2i(size_t x, size_t y, size_t z, size_t sx, size_t sy) const
{
	return x + y * sx + z * sx * sy;
}

template<typename T>
inline void Array3D<T>::i2xyz(size_t i, size_t sx, size_t sy, size_t sz, size_t &x, size_t &y, size_t &z) const
{
	x = i % sx;
	i /= sx;
	y = i % sy;
	i /= sy;
	z = i % sz;
}

template<typename T>
void Array3D<T>::checkI(size_t i, size_t si) const
{
	if (i >= si)
		throw std::out_of_range("in file " __FILE__ ": i index out of range");
}

template<typename T>
void Array3D<T>::checkXYZ(size_t x, size_t y, size_t z, size_t sx, size_t sy, size_t sz) const
{
	if (x >= sx)
		throw std::out_of_range("in file " __FILE__ ": x index out of range");
	if (y >= sy)
		throw std::out_of_range("in file " __FILE__ ": y index out of range");
	if (z >= sz)
		throw std::out_of_range("in file " __FILE__ ": z index out of range");
}

template<typename T>
std::ostream &operator <<(std::ostream &out, const Array3D<T> &m)
{
	for (size_t z = 0; z < m.sizez(); ++z) {
		out << "layer z=" << z << std::endl;
		for (size_t y = 0; y < m.sizey(); ++y) {
			out << "( ";
			for (size_t x = 0; x < m.sizex(); ++x) {
				out << m(x, y, z);
				if (x != m.sizex() - 1)
					out << ", ";
			}
			out << " )" << std::endl;
		}
	}
	return out;
}

#endif
