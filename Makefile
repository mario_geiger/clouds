CXX = g++-4.7
CC = g++-4.7
#CXX = clang
#CC  = clang

CXXFLAGS = -O3 -Werror -Wall -W -g `wx-config --cxxflags` -std=c++11 -I math -I opengl -I rsc -I util -I physics -I view -I control
LDLIBS   = -O3 -Werror -Wall -W -g `wx-config --libs gl,core,base` -std=c++11 -lGL -lGLU -lGLEW -lstdc++ -lm

VPATH=opengl math rsc util physics view control gnuplot

EXEC   = test_vecteur2D test_vec2 test_array3d test_mount test_potential test_wind test_sky test_xml test_system test_mountrange test_parser test_progressbar test_color test_ground test_forest test_random test_system2 clouds clouds_view
GRAPHS = graph_potential.pdf graph_sky.pdf graph_mountrange.pdf graph_wind.pdf

all: $(EXEC)
graphs: $(GRAPHS)

clean:
	rm -f *.o *.dat $(EXEC) $(GRAPHS) resource/Makefile resource/resource

test_vecteur2D: test_vecteur2D.o vecteur2D.o

test_vec2: test_vec2.o

test_vec3: test_vec3.o

test_array3d: test_array3d.o

test_mount: test_mount.o mountsimple.o

test_potential: test_potential.o potential.o mountsimple.o progressbar.o

test_wind: test_wind.o potential.o mountsimple.o progressbar.o 

test_sky: test_sky.o sky.o potential.o mountsimple.o progressbar.o

clouds: clouds.o system.o potential.o sky.o mountsimple.o progressbar.o ground.o forest.o parser.o mountrange.o mountraw.o xml.o

clouds_view: clouds_view.o glview.o glvolumic.o glshader.o matrix4.o glskybox.o gltexture.o texground.o sky30.o mountsimple.o mountrange.o mountraw.o glbuffer.o glmount.o system.o parser.o xml.o sky.o potential.o progressbar.o oasisday.o glbillboard.o ground.o vegtex.o glplant.o forest.o

test_xml: test_xml.o xml.o

test_system: test_system.o system.o potential.o sky.o mountsimple.o progressbar.o ground.o forest.o
test_system2: test_system2.o system.o potential.o sky.o mountsimple.o progressbar.o ground.o forest.o parser.o mountrange.o mountraw.o xml.o

test_mountrange: test_mountrange.o mountsimple.o mountrange.o mountraw.o

test_progressbar: test_progressbar.o progressbar.o

test_parser: test_parser.o parser.o xml.o mountrange.o mountsimple.o mountraw.o system.o sky.o potential.o progressbar.o ground.o forest.o

test_color: test_color.o

test_ground: test_ground.o ground.o system.o potential.o sky.o mountsimple.o progressbar.o forest.o

test_forest: ground.o forest.o sky.o potential.o progressbar.o

test_random: test_random.o

potential.dat: test_potential
	./test_potential -mount > potential.dat

graph_potential.pdf: graph_potential.gnu potential.dat
	gnuplot gnuplot/graph_potential.gnu

sky.dat: test_sky
	./test_sky > sky.dat

graph_sky.pdf: graph_sky.gnu sky.dat
	gnuplot gnuplot/graph_sky.gnu

mountrange.dat: test_mountrange
	./test_mountrange > mountrange.dat
	
graph_mountrange.pdf: graph_mountrange.gnu mountrange.dat
	gnuplot gnuplot/graph_mountrange.gnu

wind.dat: test_wind
	./test_wind -mount > wind.dat
	
graph_wind.pdf: graph_wind.gnu wind.dat
	gnuplot gnuplot/graph_wind.gnu
