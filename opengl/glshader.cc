#include "glshader.hh"
#include <iostream>
#include <cstring>
using namespace std;

GLShader::GLShader()
	: _vertex(0)
	, _fragment(0)
	, _program(0)
	, _init(false)
{
}

GLShader::~GLShader()
{
	free();
}

void GLShader::init()
{
	if (_init)
		cerr << "shader already initialized" << endl;
	_vertex = glCreateShader(GL_VERTEX_SHADER);
	_fragment = glCreateShader(GL_FRAGMENT_SHADER);
	_program = glCreateProgram();
	glAttachShader(_program, _vertex);
	glAttachShader(_program, _fragment);
	_init = true;
}

void GLShader::free()
{
	if (_init) {
		glDetachShader(_program, _vertex);
		glDetachShader(_program, _fragment);
		glDeleteObjectARB(_vertex);
		glDeleteObjectARB(_fragment);
		glDeleteShader(_program);
	}
}

bool GLShader::addVertexSourceCode(const GLchar *src)
{
	if (!_init) {
		cerr << "shader not initialized" << endl;
		return false;
	}
	
	GLint len = strlen(src);
	glShaderSourceARB(_vertex, 1, &src, &len);
	glCompileShaderARB(_vertex);
	
	GLint compiled = 0;
	glGetShaderiv(_vertex, GL_COMPILE_STATUS, &compiled);

	if (compiled == GL_TRUE)
		return true;

	GLint blen = 0;
	GLsizei slen = 0;

	glGetShaderiv(_vertex, GL_INFO_LOG_LENGTH , &blen);       
	if (blen > 1)
	{
		GLchar* compiler_log = new GLchar[blen];
		glGetInfoLogARB(_vertex, blen, &slen, compiler_log);
		cout << "vertex_compiler_log:\n" << compiler_log << endl;
		delete[] compiler_log;
	}
	return false;
}

bool GLShader::addFragmentSourceCode(const GLchar *src)
{
	if (!_init) {
		cerr << "shader not initialized" << endl;
		return false;
	}

	GLint len = strlen(src);
	glShaderSourceARB(_fragment, 1, &src, &len);
	glCompileShaderARB(_fragment);
	
	GLint compiled = 0;
	glGetShaderiv(_fragment, GL_COMPILE_STATUS, &compiled);

	if (compiled == GL_TRUE)
		return true;

	GLint blen = 0;
	GLsizei slen = 0;

	glGetShaderiv(_fragment, GL_INFO_LOG_LENGTH , &blen);       
	if (blen > 1)
	{
		GLchar* compiler_log = new GLchar[blen];
		glGetInfoLogARB(_fragment, blen, &slen, compiler_log);
		cerr << "fragment_compiler_log:\n" << compiler_log << endl;
		delete[] compiler_log;
	}
	return false;
}

bool GLShader::link()
{
	if (!_init) {
		cerr << "shader not initialized" << endl;
		return false;
	}
	
	glLinkProgram(_program);

	GLint linked;
	glGetProgramiv(_program, GL_LINK_STATUS, &linked);
	
	if (linked == GL_FALSE)
		cerr << "program_link_error" << endl;

	return linked == GL_TRUE;
}

void GLShader::bind()
{
	glUseProgram(_program);
}

void GLShader::release()
{
	glUseProgram(0);
}

GLint GLShader::uniformLocation(const GLchar *name) const
{
	return glGetUniformLocation(_program, name);
}

void GLShader::setUniformValue(const GLchar *name, GLfloat value)
{
	setUniformValue(uniformLocation(name), value);
}

void GLShader::setUniformValue(GLint location, GLfloat value)
{
	glUniform1fv(location, 1, &value);
}

void GLShader::setUniformValue(const GLchar *name, GLint value)
{
	setUniformValue(uniformLocation(name), value);
}

void GLShader::setUniformValue(GLint location, GLint value)
{
	glUniform1i(location, value);
}

void GLShader::setUniformValue(const GLchar *name, GLfloat value0, GLfloat value1, GLfloat value2)
{
	setUniformValue(uniformLocation(name), value0, value1, value2);
}

void GLShader::setUniformValue(GLint location, GLfloat value0, GLfloat value1, GLfloat value2)
{
	glUniform3f(location, value0, value1, value2);
}
