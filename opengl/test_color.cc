#include <iostream>
#include "color.hh"
using namespace std;

int main() {
	Color rouge(1.,1.,0.,0.);
	cout << rouge.aF() << " " << rouge.rF() << " " << rouge.gF() << " " << rouge.bF() << endl;
	rouge = rouge.darker();
	cout << rouge.aF() << " " << rouge.rF() << " " << rouge.gF() << " " << rouge.bF() << endl;
	rouge = rouge.lighter();
	cout << rouge.aF() << " " << rouge.rF() << " " << rouge.gF() << " " << rouge.bF() << endl;
	
	return 0;
}
