#include "glplant.hh"

GLPlant::GLPlant() :
	m_vbo(GL_ARRAY_BUFFER)
{
}

GLPlant::~GLPlant()
{
}

void GLPlant::init()
{
	// construction du VBO
	static const float vertices[] = {
		// 0-3
		0.0, 0.0,   -1.0, 0.0, 0.0,
		1.0, 0.0,    1.0, 0.0, 0.0,
		1.0, 1.0,    1.0, 0.0, 2.0,
		0.0, 1.0,   -1.0, 0.0, 2.0,
		// 4-7
		0.0, 0.0,    1.0, 0.0, 0.0,
		1.0, 0.0,   -1.0, 0.0, 0.0,
		1.0, 1.0,   -1.0, 0.0, 2.0,
		0.0, 1.0,    1.0, 0.0, 2.0,
		// 8-11
		0.0, 0.0,    0.0, -1.0, 0.0,
		1.0, 0.0,    0.0,  1.0, 0.0,
		1.0, 1.0,    0.0,  1.0, 2.0,
		0.0, 1.0,    0.0, -1.0, 2.0,
		// 12-15
		0.0, 0.0,    0.0,  1.0, 0.0,
		1.0, 0.0,    0.0, -1.0, 0.0,
		1.0, 1.0,    0.0, -1.0, 2.0,
		0.0, 1.0,    0.0,  1.0, 2.0,
		// 16-19
		0.0, 0.0,   -1.0, -1.0, 1.0,
		1.0, 0.0,    1.0, -1.0, 1.0,
		1.0, 1.0,    1.0,  1.0, 1.0,
		0.0, 1.0,   -1.0,  1.0, 1.0,
	};
	
	m_vbo.create();
	m_vbo.bind();
	m_vbo.allocate(vertices, sizeof vertices);
	m_vbo.release();
}

void GLPlant::draw(GLuint sideview, GLuint topview)
{
	glEnable(GL_CULL_FACE);						// dessine pas les face arrières
	glEnable(GL_TEXTURE_2D);					// active le module texture dans le pipeline par defaut
	glEnable(GL_DEPTH_TEST);					// test de profondeur
	glEnable(GL_BLEND);							// méange lors de l'écriture du fragment sur le buffer
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // fonction de mélange
	glEnable(GL_ALPHA_TEST);					// test de transparence (ne dessine meme pas un pixel trop trensparent)
	glAlphaFunc(GL_GREATER, 0.9f);				// fonction de test de transparence

	glEnableClientState(GL_VERTEX_ARRAY);		// pour le VBO
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	m_vbo.bind();
	glClientActiveTexture(GL_TEXTURE0);
	glTexCoordPointer(2, GL_FLOAT, sizeof (float) * (3+2), 0);
	glVertexPointer(  3, GL_FLOAT, sizeof (float) * (3+2), BUFFER_OFFSET(sizeof (float) * 2));
	m_vbo.release();

	glActiveTexture(GL_TEXTURE0);				// lie la texure et dessine
	glBindTexture(GL_TEXTURE_2D, sideview);
	glDrawArrays(GL_QUADS, 0, 16);
	glBindTexture(GL_TEXTURE_2D, topview);
	glDrawArrays(GL_QUADS, 16, 4);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}
