#ifndef __GLVIEW__
#define __GLVIEW__

#include <GL/glew.h>
#include <GL/glu.h>
#include <GL/gl.h>
#include <wx/wx.h>
#include <wx/glcanvas.h>

#include "glshader.hh"
#include "glvolumic.hh"
#include "glbillboard.hh"
#include "glskybox.hh"
#include "glmount.hh"
#include "glplant.hh"
#include "matrix4.hh"
#include "system.hh"

class GLView : public wxGLCanvas
{
public:
	GLView(System* system, wxFrame* parent, int* args);
	~GLView();

	void initGL();
	void resized(wxSizeEvent& evt);
	void render(wxPaintEvent& evt);

	void timer1Elapsed(wxTimerEvent& event);
	void timer2Elapsed(wxTimerEvent& event);
	void mousePress(wxMouseEvent& event);
	void mouseMoved(wxMouseEvent& event);
	void keyPressed(wxKeyEvent& event);
	void keyReleased(wxKeyEvent& event);

	void setLayerCount(long layerCount);
	void setVerbose(bool on);
private:
	void loadSky();

	System* m_system;

	wxGLContext *m_context;
	
	GLVolumic m_volumic;
	GLBillBoards m_bill;
	GLSkybox m_skybox;
	GLMount m_mount;
	GLPlant m_plant;
	GLuint m_vegtex[6]; // beech, beech_top, ...
	
	wxTimer* m_timer1;
	wxTimer* m_timer2;
	static int TIMER1_ID;
	static int TIMER2_ID;
	
	// view control
	Matrix4 m_viewMatrix;
	enum class Key : uint { W= (1u << 0)
	                      , S= (1u << 1)
	                      , A= (1u << 2)
	                      , D= (1u << 3)
	                      , Q= (1u << 4)
	                      , E= (1u << 5)
	                      , R= (1u << 6)
	                      , F= (1u << 7) };
	uint m_pressed;
	long m_mouseLastX, m_mouseLastY;
	
	bool m_verbose;
	
DECLARE_EVENT_TABLE()
};

#endif
