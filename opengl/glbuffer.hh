#ifndef __GLBUFFER_HH__
#define __GLBUFFER_HH__
#include <GL/glew.h>

// petit wrap des buffers de opengl
class GLBuffer
{
public:
	explicit GLBuffer(GLenum target);
	GLBuffer(const GLBuffer&) = delete;
	GLBuffer& operator =(GLBuffer) = delete;
	
	bool create();
	void destroy();
	void bind();
	void release();
	void allocate(const void* data, int count);
private:
	GLuint m_id;
	GLenum m_target;
};

#define BUFFER_OFFSET(i) ((char*)NULL + (i))

#endif
