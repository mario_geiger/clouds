#include "glbuffer.hh"

GLBuffer::GLBuffer(GLenum target) :
	m_id(0),
	m_target(target)
{
}

bool GLBuffer::create()
{
	m_id = 0;
	glGenBuffers(1, &m_id);
	return m_id != 0;
}

void GLBuffer::destroy()
{
	glDeleteBuffers(1, &m_id);
}

void GLBuffer::bind()
{
	glBindBuffer(m_target, m_id);
}

void GLBuffer::release()
{
	glBindBuffer(m_target, 0);
}

void GLBuffer::allocate(const void* data, int count)
{
	glBufferData(m_target, count, data, GL_STATIC_DRAW_ARB);
}
