#include "glview.hh"
#include "system.hh"
#include "parser.hh"
#include <wx/wx.h>
#include <wx/cmdline.h>
#include <iostream>

using namespace std;

class MyApp: public wxApp
{
private:
	bool OnInit() override;
	void OnInitCmdLine(wxCmdLineParser& parser) override;
	bool OnCmdLineParsed(wxCmdLineParser& parser) override;

	System s;

	wxFrame *_frame;
	GLView *_glPane;
	
	// Parametres et option du programmes, initialises dans OnCmdLineParsed
	wxString _xmlFile;
	long _layerCount;
	bool _verbose;
	bool _fullScreen;
};

IMPLEMENT_APP(MyApp)

bool MyApp::OnInit()
{
	if (!wxApp::OnInit())
		return false;
		
	Parser p(&s);
	if (!p.parse(std::string(_xmlFile.mb_str())))
		return false;
	
	s.init(true);
	
	cout << "wxWidget Initialisation ................" << flush;

	wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
	_frame = new wxFrame((wxFrame *)NULL, -1,  wxT("clouds"), wxPoint(50,50), wxSize(500,500));

	int args[] = {WX_GL_RGBA, WX_GL_DOUBLEBUFFER, WX_GL_DEPTH_SIZE, 16, 0};

	_glPane = new GLView(&s, _frame, args);
	sizer->Add(_glPane, 1, wxEXPAND);

	_frame->SetSizer(sizer);
	_frame->SetAutoLayout(true);

	if (_fullScreen)
		_frame->ShowFullScreen(true, wxFULLSCREEN_ALL);
	else
		_frame->Show();

	cout << "ok" << endl;

	_glPane->setVerbose(_verbose);
	_glPane->initGL();
	_glPane->setLayerCount(_layerCount);
	
	return true;
}

void MyApp::OnInitCmdLine(wxCmdLineParser& parser)
{
	wxApp::OnInitCmdLine(parser);

	parser.AddParam(wxT("xml file"), wxCMD_LINE_VAL_STRING);
	parser.AddSwitch(wxT("h"), wxT("help"), wxT("displays help on the command line parameters"), wxCMD_LINE_OPTION_HELP);
	parser.AddOption(wxT("l"), wxT("layers"), wxT("number of layers for the clouds"), wxCMD_LINE_VAL_NUMBER, wxCMD_LINE_PARAM_OPTIONAL);
	parser.AddSwitch(wxT("b"), wxT("billboards"), wxT("use billboards to render clouds"), wxCMD_LINE_PARAM_OPTIONAL);
	parser.AddSwitch(wxT("f"), wxT("fullscreen"), wxT("switch window fullscreen"), wxCMD_LINE_PARAM_OPTIONAL);
}

bool MyApp::OnCmdLineParsed(wxCmdLineParser& parser)
{
	if (!wxApp::OnCmdLineParsed(parser))
		return false;
	
	_xmlFile = parser.GetParam();

	if (parser.Found(wxT("layers"), &_layerCount)) {
		if (_layerCount < 0) {
			cerr << "option 'layers' must be a nonnegative number" << endl;
			return false;
		}
	} else {
		_layerCount = 0;
	}

	if (parser.Found(wxT("billboards")))
		_layerCount = 0;
		
	_verbose = parser.Found(wxT("verbose"));
	
	_fullScreen = parser.Found(wxT("fullscreen"));

	return true;
}
