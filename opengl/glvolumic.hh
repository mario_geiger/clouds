#ifndef __GLVOLUMIC_HH__
#define __GLVOLUMIC_HH__

#include "glshader.hh"
#include "array3d.hh"
#include "matrix4.hh"
#include "glbuffer.hh"
#include <vector>

class GLVolumic
{
public:
	GLVolumic();
	GLVolumic(const GLVolumic &) = delete;
	~GLVolumic();
	
	int layerCount() const;
	
	void setLayerCount(int layerCount);
	void setVision(float near, float fare);
	void setSize(int sizex, int sizey, int sizez);
	void setVoxel(int x, int y, int z, GLuint voxel);
	
	void init();
	void draw();
private:
	void uploadTexture();
	void updateVBO();

	GLuint _textureId;
	GLShader *_program;
	
	int _layerCount;
	float _near, _fare;
		
	Array3D<GLuint> _data;
	bool _updatedTex;
	bool _updatedVBO;

	GLBuffer m_vbo;
};

#endif
