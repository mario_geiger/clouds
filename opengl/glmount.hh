#ifndef __GLMOUNT_HH__
#define __GLMOUNT_HH__

#include <vector>
#include "vec3.hh"
#include "glbuffer.hh"
#include "glshader.hh"
#include "mount.hh"
#include "ground.hh"

// cette classe dessine une montagne avec opengl
class GLMount
{
public:
	GLMount();
	~GLMount();
	void init(const Mount& mount, const Ground& ground, float x, float y, int nx, int ny);
	void draw();
	void free();
private:
	void addVertex(std::vector<float>& vs, const Mount& m, const Ground& ground, float x, float y);
	GLShader m_prog;
	GLuint m_tex[3];

	GLBuffer m_vbo;
	GLBuffer m_ibo;
	int m_count;
};

#endif
