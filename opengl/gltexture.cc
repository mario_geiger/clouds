#include "gltexture.hh"
#include <wx/image.h>
#include <string>
#include <iostream>

using namespace std;

bool GLTexture::handlersInitialized = false;

void GLTexture::loadTexture(const wxString &path, GLenum target, int option)
{
	if (!handlersInitialized) {
		wxInitAllImageHandlers();
		handlersInitialized = true;
	}
	
	if(!wxFileExists(path))
		throw string("loadTexture : file not found");
	
	wxImage image(path, wxBITMAP_TYPE_ANY);
	loadTexture(image, target, option);
}

void GLTexture::loadTexture(wxInputStream &inputStream, GLenum target, int option)
{
	if (!handlersInitialized) {
		wxInitAllImageHandlers();
		handlersInitialized = true;
	}

	wxImage image(inputStream, wxBITMAP_TYPE_ANY);
	loadTexture(image, target, option);
}

void GLTexture::loadTexture(const wxImage &image, GLenum target, int option)
{
	int width = image.GetWidth();
	int height = image.GetHeight();
//	cout << width << " " << height << ", ";
	
	// glPixelStorei sets pixel storage modes that affect glTexImage2D
	// 1 (byte-alignment)
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	/* -----------------------------------------------------------------
	 * Paramètres pour la texture
	 * -------------------------------------------------------------- */
	GLenum texParameterTarget = target >= GL_TEXTURE_CUBE_MAP_POSITIVE_X && target <= GL_TEXTURE_CUBE_MAP_NEGATIVE_Z ? GL_TEXTURE_CUBE_MAP : target;

	if (option & Mipmap) {
	    glHint(GL_GENERATE_MIPMAP_HINT_SGIS, GL_NICEST);
		glTexParameteri(texParameterTarget, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);
		glTexParameteri(texParameterTarget, GL_TEXTURE_MIN_FILTER, (option & LinearFiltering) ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_LINEAR);
	} else {
		glTexParameteri(texParameterTarget, GL_TEXTURE_MIN_FILTER, (option & LinearFiltering) ? GL_LINEAR : GL_NEAREST);
	}
	glTexParameteri(texParameterTarget, GL_TEXTURE_MAG_FILTER, (option & LinearFiltering) ? GL_LINEAR : GL_NEAREST);

	/* -----------------------------------------------------------------
	 * Sans la transparence
	 * -------------------------------------------------------------- */
	if (!image.HasAlpha()) {
		if (option & InvertedY) {
//			cout << "no alpha, inv" << endl;
			wxImage mirrored = image.Mirror();
			glTexImage2D(target, 0, 3, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, mirrored.GetData());
		} else {
//			cout << "no alpha, not inv" << endl;
			glTexImage2D(target, 0, 3, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image.GetData());
		}
	/* -----------------------------------------------------------------
	 * Avec la transparence
	 * -------------------------------------------------------------- */
	} else {
		GLubyte data[height][width][4];
		if (option & InvertedY) {
//			cout << "alpha, inv" << endl;
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					data[height - y - 1][x][0] = image.GetRed(x, y);
					data[height - y - 1][x][1] = image.GetGreen(x, y);
					data[height - y - 1][x][2] = image.GetBlue(x, y);
					data[height - y - 1][x][3] = image.GetAlpha(x, y);
				}
			}
		} else {
//			cout << "alpha, not inv" << endl;
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					data[y][x][0] = image.GetRed(x, y);
					data[y][x][1] = image.GetGreen(x, y);
					data[y][x][2] = image.GetBlue(x, y);
					data[y][x][3] = image.GetAlpha(x, y);
				}
			}
		}
		glTexImage2D(target, 0, 4, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	}
}
