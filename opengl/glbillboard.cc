#include "glbillboard.hh"
#include "gltexture.hh"
#include "texground.hh"
#include <wx/mstream.h>
#include <vector>
#include <iostream>

// vertex shader
static const GLchar *vsrc = GLSL(120,
uniform vec3 center;
void main(void) 
{
	// effecture la rotation inverse avant de translater puis d'opèrer les transormation standard
	gl_Position = gl_ModelViewProjectionMatrix * vec4(mat3(gl_ModelViewMatrixInverse) * vec3(gl_Vertex) + center, 1.0);

	gl_TexCoord[0] = gl_MultiTexCoord0;
}
);

// fragment shader
static const GLchar *fsrc = GLSL(120,
uniform sampler2D tex;
uniform float opacity;
void main(void)
{
	vec4 tx = texture2D(tex, gl_TexCoord[0].st);
	if (tx.r < 0.1) discard;
	
	// texture noir et blanc / nuage blanc  :-/
	gl_FragColor = vec4(1.0, 1.0, 1.0, tx.r * opacity);
}
);

GLBillBoard::GLBillBoard(float x, float y, float z, float opacity) :
	m_center(x, y, z), m_opacity(opacity)
{
}

GLBillBoard::GLBillBoard(const GLBillBoard& other) :
	m_center(other.m_center), m_opacity(other.m_opacity)
{
}

GLBillBoards::GLBillBoards() :
	m_vbo(GL_ARRAY_BUFFER)
{
}

GLBillBoards::~GLBillBoards()
{
	m_vbo.destroy();
	glDeleteTextures(1, &m_tex0);
}

void GLBillBoards::init(float w, float h)
{
	m_program.init();
	m_program.addVertexSourceCode(vsrc);
	m_program.addFragmentSourceCode(fsrc);
	m_program.link();
	m_program.bind();
	m_program.setUniformValue("tex", 0);
	m_centerLocation = m_program.uniformLocation("center");
	m_opacityLocation = m_program.uniformLocation("opacity");
	m_program.release();

	glGenTextures(1, &m_tex0); {
		glBindTexture(GL_TEXTURE_2D, m_tex0);
		wxMemoryInputStream input(smoketex_jpg, smoketex_jpg_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	
	static const float vertices[16] = 
	{
		-w,   -h, 
		0.0f, 0.0f,
		
		+w,   -h,
		1.0f, 0.0f,

		+w,   +h,
		1.0f, 1.0f,
		
		-w,   +h,
		0.0f, 1.0f
	};
	
	m_vbo.create();				// crée le VBO le plus petit du monde
	m_vbo.bind();
	m_vbo.allocate(vertices, sizeof vertices);
	m_vbo.release();	
}

void GLBillBoards::draw()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);
	glDisable(GL_ALPHA_TEST);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_tex0);

	glEnableClientState(GL_VERTEX_ARRAY);		// active la gestion des attibuts passé par VBO
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	m_vbo.bind();
	glVertexPointer(  2, GL_FLOAT, sizeof (float) * (2+2), 0);
	glClientActiveTexture(GL_TEXTURE0);
	glTexCoordPointer(2, GL_FLOAT, sizeof (float) * (2+2), BUFFER_OFFSET(sizeof (float) * 2));
	m_vbo.release();

	m_program.bind();
	glDepthMask(GL_FALSE);
	for (size_t i = 0; i < m_billboards.size(); ++i) {
		m_program.setUniformValue(m_centerLocation, m_billboards[i].cx(), m_billboards[i].cy(), m_billboards[i].cz());
		m_program.setUniformValue(m_opacityLocation, m_billboards[i].m_opacity);

		glDrawArrays(GL_QUADS, 0, 4);
	}
	glDepthMask(GL_TRUE);
	m_program.release();
	
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
}

void GLBillBoards::clear()
{
	m_billboards.clear();
}
	
void GLBillBoards::addBillBoard(const GLBillBoard& bb)
{
	m_billboards.push_back(bb);
}
