#ifndef __GLBILLBOARD_HH__
#define __GLBILLBOARD_HH__

#include "vec3.hh"
#include "matrix4.hh"
#include "glshader.hh"
#include "glbuffer.hh"
#include <vector>

class GLBillBoards;

// représante un billboard (texure oriantée selon un axe donné)
// ici se sera simplement l'axe z
class GLBillBoard
{
public:
	GLBillBoard(float x, float y, float z, float opacity);
	GLBillBoard(const GLBillBoard& other);

	inline float cx() const { return m_center.x(); }
	inline float cy() const { return m_center.y(); }
	inline float cz() const { return m_center.z(); }
private:
	Vec3f m_center;
	float m_opacity;
	friend GLBillBoards;
};

class GLBillBoards
{
public:
	GLBillBoards();
	~GLBillBoards();

	void init(float width, float height);
	void draw();
	
	void clear();
	void addBillBoard(const GLBillBoard& bb);
	
private:
	GLBuffer m_vbo;
	std::vector<GLBillBoard> m_billboards;
	GLShader m_program;
	GLint m_centerLocation;
	GLint m_opacityLocation;
	GLuint m_tex0;
};

#endif
