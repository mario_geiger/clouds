#ifndef __TEXTURE_HH__
#define __TEXTURE_HH__

#include <GL/gl.h>
#include <wx/wx.h>

class GLTexture
{
public:
	enum Options {
		Nothing         = 0x00,
		InvertedY       = 0x01,
		Mipmap          = 0x02,
		LinearFiltering = 0x04,
		Default         = InvertedY | Mipmap | LinearFiltering,
	};

	static void loadTexture(const wxString &path, GLenum target = GL_TEXTURE_2D, int option = Default);

	static void loadTexture(wxInputStream &inputStream, GLenum target = GL_TEXTURE_2D, int option = Default);

	static void loadTexture(const wxImage &image, GLenum target = GL_TEXTURE_2D, int option = Default);
private:
	static bool handlersInitialized;
};

//constexpr inline Texture::Options operator|(Texture::Options f, Texture::Options g) { return static_cast<Texture::Options>(static_cast<int>(f) | static_cast<int>(g)); }
//constexpr inline Texture::Options operator&(Texture::Options f, Texture::Options g) { return static_cast<Texture::Options>(static_cast<int>(f) & static_cast<int>(g)); }

#endif
