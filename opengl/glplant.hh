#ifndef __GLPLANT_HH__
#define __GLPLANT_HH__

/* Dessine trois plans perpandiculaires texturés
 *              +
 *             /|
 *     +------+-------+
 *     |     /|       |
 *     |    + |       |-+
 *     |    | |       |/
 *     +----| +-------+
 *    /     |/       /|
 *   +------+-------+ |
 *     |    | |       |
 *     +----| +-------+
 *          |/ ^   
 *          +   \
 *               +------ origine (0,0,0)
 */

#include "glbuffer.hh"

class GLPlant
{
public:
	GLPlant();
	~GLPlant();
	
	void init();
	void draw(GLuint sideview, GLuint topview);
private:
	GLBuffer m_vbo;
};

#endif
