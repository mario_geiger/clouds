#ifndef __COLOR__
#define __COLOR__

#include <cmath>

enum ColorName {white, red, green, blue, black, darkRed, darkGreen, darkBlue, cyan, magenta, yellow, gray, darkCyan, darkMagenta, darkYellow, darkGray, lightGray};

class Color {
	public:
		Color(float a, float r, float g, float b);
		Color(ColorName name);
		Color() : _a(0.0), _r(0.0), _g(0.0), _b(0.0) {}
		
		float aF() const { return _a; }
		float rF() const { return _r; }
		float gF() const { return _g; }
		float bF() const { return _b; }
		int a() const { return floor(_a*255.0); }
		int r() const { return floor(_r*255.0); }
		int g() const { return floor(_g*255.0); }
		int b() const { return floor(_b*255.0); }
		
		unsigned int rgb() const;
		
		void aF(float __a);
		void rF(float __r);
		void gF(float __g);
		void bF(float __b);
		void a(int __a);
		void r(int __r);
		void g(int __g);
		void b(int __b);
		
		bool isValid() const;
		Color validate() const;
		
		Color darker(int t = 150) const;
		Color lighter(int t = 150) const;
		
	private:
		//Attributs a,r,g,b ; trois nombres à virgule dans [0,1].
		float _a;
		float _r;
		float _g;
		float _b;
};

inline Color::Color(float a, float r, float g, float b) {
	if(a<0) _a = 0.;
	else if(a>1) _a = 1.;
	else _a = a;
	
	if(r<0) _r = 0.;
	else if(r>1) _r = 1.;
	else _r = r;
	
	if(g<0) _g = 0.;
	else if(g>1) _g = 1.;
	else _g = g;
	
	if(b<0) _b = 0.;
	else if(b>1) _b = 1.;
	else _b = b;
}
inline Color::Color(ColorName name) {
	switch(name) {
		case white :
			_a = 1.; _r = 1.; _g = 1.; _b = 1.;
		break;
		case red :
			_a = 1.; _r = 1.; _g = 0.; _b = 0.;
		break;
		case green :
			_a = 1.; _r = 0.; _g = 1.; _b = 0.;
		break;
		case blue :
			_a = 1.; _r = 0.; _g = 0.; _b = 1.;
		break;
		case black :
			_a = 1.; _r = 0.; _g = 0.; _b = 0.;
		break;
		case darkRed :
			_a = 1.; _r = 128./255.; _g = 0.; _b = 0.;
		break;
		case darkGreen :
			_a = 1.; _r = 0.; _g = 128./255.; _b = 0.;
		break;
		case darkBlue :
			_a = 1.; _r = 0.; _g = 0.; _b = 128./255.;
		break;
		case cyan :
			_a = 1.; _r = 0.; _g = 1.; _b = 1.;
		break;
		case magenta :
			_a = 1.; _r = 1.; _g = 0.; _b = 1.;
		break;
		case yellow :
			_a = 1.; _r = 1.; _g = 1.; _b = 0.;
		break;
		case darkCyan :
			_a = 1.; _r = 0.; _g = 128./255.; _b = 128./255.;
		break;
		case darkMagenta :
			_a = 1.; _r = 128./255.; _g = 0.; _b = 128./255.;
		break;
		case darkYellow :
			_a = 1.; _r = 128./255.; _g = 128./255.; _b = 0.;
		break;
		case gray :
			_a = 1.; _r = 160./255.; _g = 160./255.; _b = 164./255.;
		break;
		case darkGray :
			_a = 1.; _r = 128./255.; _g = 128./255.; _b = 128./255.;
		break;
		case lightGray :
			_a = 1.; _r = 192./255.; _g = 192./255.; _b = 192./255.;
		break;
	}
}

inline void Color::aF(float __a) {
	if(__a<0) _a = 0.;
	else if(__a>1) _a = 1.;
	else _a = __a;
}
inline void Color::rF(float __r) {	
	if(__r<0) _r = 0.;
	else if(__r>1) _r = 1.;
	else _r = __r;
}
inline void Color::gF(float __g) {
	if(__g<0) _g = 0.;
	else if(__g>1) _g = 1.;
	else _g = __g;
}
inline void Color::bF(float __b) {
	if(__b<0) _b = 0.;
	else if(__b>1) _b = 1.;
	else _b = __b;
}
inline void Color::a(int __a) {
	if(__a<0) _a = 0.;
	else if(__a>255) _a = 1.;
	else _a = __a / 255.0;
}
inline void Color::r(int __r) {
	if(__r<0) _r = 0.;
	else if(__r>255) _r = 1.;
	else _r = __r / 255.0;
}
inline void Color::g(int __g) {
	if(__g<0) _g = 0.;
	else if(__g>255) _g = 1.;
	else _g = __g / 255.0;
}
inline void Color::b(int __b) {
	if(__b<0) _b = 0.;
	else if(__b>255) _b = 1.;
	else _b = __b / 255.0;
}

inline unsigned int Color::rgb() const {
	return floor(255*_a)*pow(2,24) + floor(255*_r)*pow(2,16) + floor(255*_g)*pow(2,8) + floor(255*_b);
}

inline bool Color::isValid() const {
	return _a>=0 && _a<=1 && _r>=0 && _r<=1 && _g>=0 && _g<=1 && _b>=0 && _b<=1;
}

	//Remarque : dans les trois méthodes suivantes, c'est le constructeur qui s'asure de la validité des valeurs données pour a,r,g,b !
	
inline Color Color::validate() const {
	return Color(_a,_r,_g,_b);
}
inline Color Color::lighter(int t) const {
	return Color(_a+(1.-_a)*(t-100)/100., _r+(1.-_r)*(t-100)/100., _g+(1.-_g)*(t-100)/100., _b+(1.-_b)*(t-100)/100.);
}
inline Color Color::darker(int t) const {
	return Color(_a-_a*(t-100)/100., _r-_r*(t-100)/100., _g-_g*(t-100)/100., _b-_b*(t-100)/100.);
}

#endif
