#include "glvolumic.hh"
#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

static const GLchar *vsrc = GLSL(120,
void main(void) 
{
	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_ModelViewMatrixInverse * gl_Vertex;
	gl_Position = gl_ProjectionMatrix * gl_Vertex;
}
);

static const GLchar *fsrc = GLSL(120,
uniform sampler3D tex;
void main(void)
{
	vec4 tx = texture3D(tex, gl_TexCoord[0].stp);
	gl_FragColor = smoothstep(0.0, 0.7, tx); // smoothstep for more sharp clouds
}
);

GLVolumic::GLVolumic() :
	_program(new GLShader()),
	_layerCount(10),
	_near(1.0),
	_fare(100.0),
	_data(),
	_updatedTex(false),
	_updatedVBO(false),
	m_vbo(GL_ARRAY_BUFFER)
{
}

GLVolumic::~GLVolumic()
{
	glDeleteTextures(1, &_textureId);
	delete _program;
	m_vbo.destroy();
}

int GLVolumic::layerCount() const
{
	return _layerCount;
}

void GLVolumic::setLayerCount(int layerCount)
{
	_layerCount = layerCount;
	_updatedVBO = false;
}

void GLVolumic::setVision(float near, float fare)
{
	_near = near;
	_fare = fare;
}

void GLVolumic::setSize(int sizex, int sizey, int sizez)
{
	_data.resize(sizex, sizey, sizez);
	_updatedTex = false;
}

void GLVolumic::setVoxel(int x, int y, int z, GLuint voxel)
{
	_data(x, y, z) = voxel;
	_updatedTex = false;
}

void GLVolumic::init()
{
	_program->init();
	_program->addVertexSourceCode(vsrc);
	_program->addFragmentSourceCode(fsrc);
	_program->link();
	_program->bind();
	_program->setUniformValue("tex", 0);
	_program->release();
	glGenTextures(1, &_textureId);

	m_vbo.create();
}

void GLVolumic::draw()
{	
	// bind _textureId sur la texture 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_3D, _textureId);

	if (!_updatedTex) {
		uploadTexture();
		_updatedTex = true;
	}
	if (!_updatedVBO) {
		updateVBO();
		_updatedVBO = true;
	}

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_CULL_FACE);
	glDisable(GL_ALPHA_TEST);
	
	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();
	glScalef(1.0 / _data.sizex(), 1.0 / _data.sizey(), 1.0 / _data.sizez());

	_program->bind();
	m_vbo.bind();

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, 0);
	
	glDrawArrays(GL_QUADS, 0, 4 * _layerCount);
	
	glDisableClientState(GL_VERTEX_ARRAY);

	m_vbo.release();
	_program->release();

	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

void GLVolumic::uploadTexture()
{
	glTexImage3D(GL_TEXTURE_3D, 0, 4, _data.sizex(), _data.sizey(), _data.sizez(), 
	             0, GL_RGBA, GL_UNSIGNED_BYTE, _data.data());

	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
}

void GLVolumic::updateVBO()
{
	std::vector<Vec3f> vertices;
	GLfloat z = - _fare;
	GLfloat coeff = (_fare - _near) / (0.5 * _layerCount * (_layerCount + 1));
	for (int i = 0; i < _layerCount; ++i) {
		// suppose l'angle de perspective de 75 degree et aspect ratio inférieur à 2
		const float big = std::tan(75.0/2.0*M_PI/180.0) * z;
		vertices.push_back(Vec3f(-big * 2.0, -big, z));
		vertices.push_back(Vec3f(+big * 2.0, -big, z));
		vertices.push_back(Vec3f(+big * 2.0, +big, z));
		vertices.push_back(Vec3f(-big * 2.0, +big, z));
		z += (_layerCount - i) * coeff;
	}
	m_vbo.bind();
	m_vbo.allocate(vertices.data(), sizeof (Vec3f) * vertices.size());
	m_vbo.release();
}
