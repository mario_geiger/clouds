#include "glview.hh"
#include "sky30.hh"
#include "oasisday.hh"
#include "vegtex.hh"
#include "gltexture.hh"
#include <iostream>
#include <chrono>
#include <wx/mstream.h>
using namespace std;

int GLView::TIMER1_ID(12);
int GLView::TIMER2_ID(13);

BEGIN_EVENT_TABLE(GLView, wxGLCanvas)
EVT_TIMER(TIMER1_ID, GLView::timer1Elapsed)
EVT_TIMER(TIMER2_ID, GLView::timer2Elapsed)
EVT_MOTION(GLView::mouseMoved)
EVT_LEFT_DOWN(GLView::mousePress)
EVT_SIZE(GLView::resized)
EVT_KEY_DOWN(GLView::keyPressed)
EVT_KEY_UP(GLView::keyReleased)
EVT_PAINT(GLView::render)
END_EVENT_TABLE()

GLView::GLView(System* system, wxFrame* parent, int* args)
	: wxGLCanvas(parent, wxID_ANY, args, wxDefaultPosition, wxDefaultSize, wxFULL_REPAINT_ON_RESIZE)
	, m_system(system)
	, m_context(nullptr)
	, m_timer1(new wxTimer(this, TIMER1_ID))
	, m_timer2(new wxTimer(this, TIMER2_ID))
	, m_pressed(0x0)
{
	m_viewMatrix = Matrix4::rotate(-90.0, 1.0, 0.0, 0.0) * m_viewMatrix;
	m_viewMatrix = Matrix4::translate(0.0, -10.0, 0.0) * m_viewMatrix;
}

GLView::~GLView()
{
	glDeleteTextures(5, m_vegtex);
	if (m_context)
		delete m_context;
}

void GLView::initGL()
{
	cout << "OpenGL Initialisation .................." << flush;
	m_context = new wxGLContext(this);
	SetCurrent(*m_context);
	glewInit();
	
	//cout << "opengl : " << glGetString(GLm_VERSION) << endl;
	//cout << "glsl : " << glGetString(GLm_SHADINGm_LANGUAGEm_VERSION) << endl;
	//cout << "exts : " << glGetString(GLm_EXTENSIONS) << endl;

	glClearColor(0.0, 0.0, 0.0, 1.0);
	
	int radius = std::max(std::max(m_system->nx(), m_system->ny()), m_system->nz());
	
	m_volumic.setSize(m_system->nx(), m_system->ny(), m_system->nz());
	m_volumic.init();
	m_volumic.setVision(4.0, 2.0 * radius);
	
	m_bill.init(1.0, 1.0);
	
	loadSky();

	m_skybox.init();
	/*
	m_skybox.loadTexture(0, posx_jpg_size, posx_jpg);
	m_skybox.loadTexture(1, negx_jpg_size, negx_jpg);
	m_skybox.loadTexture(2, posy_jpg_size, posy_jpg);
	m_skybox.loadTexture(3, negy_jpg_size, negy_jpg);
	m_skybox.loadTexture(4, posz_jpg_size, posz_jpg);
	m_skybox.loadTexture(5, negz_jpg_size, negz_jpg);
	*/
	m_skybox.loadTexture(0, oasisday_right_jpg_size, oasisday_right_jpg);
	m_skybox.loadTexture(1, oasisday_left_jpg_size, oasisday_left_jpg);
	m_skybox.loadTexture(2, oasisday_top_jpg_size, oasisday_top_jpg);
	m_skybox.loadTexture(3, negy_jpg_size, negy_jpg);
	m_skybox.loadTexture(4, oasisday_front_jpg_size, oasisday_front_jpg);
	m_skybox.loadTexture(5, oasisday_back_jpg_size, oasisday_back_jpg);

	m_mount.init(m_system->mount(), m_system->ground(), -m_system->nx() / 2.0, -m_system->ny() / 2.0, 2.0 * m_system->nx(), 2.0 * m_system->ny());

	m_plant.init();
	
	glGenTextures(5, m_vegtex); 
	// Texture beech
	{
		glBindTexture(GL_TEXTURE_2D, m_vegtex[0]);
		wxMemoryInputStream input(beech_png, beech_png_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap | GLTexture::InvertedY);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	// Texture beech_top
	{
		glBindTexture(GL_TEXTURE_2D, m_vegtex[1]);
		wxMemoryInputStream input(beech_top_png, beech_top_png_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	// Texture palm
	{
		glBindTexture(GL_TEXTURE_2D, m_vegtex[2]);
		wxMemoryInputStream input(palm_png, palm_png_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap | GLTexture::InvertedY);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	// Texture palm_top
	{
		glBindTexture(GL_TEXTURE_2D, m_vegtex[3]);
		wxMemoryInputStream input(palm_top_png, palm_top_png_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	// Texture bush
	{
		glBindTexture(GL_TEXTURE_2D, m_vegtex[4]);
		wxMemoryInputStream input(bush2_png, bush2_png_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	m_vegtex[5] = m_vegtex[4];

	wxSizeEvent evt;
	resized(evt);
	m_timer1->Start(40);
	m_timer2->Start(150);
	cout << "ok" << endl;
}
	
void GLView::resized(wxSizeEvent& event)
{
	wxGLCanvas::OnSize(event);
	if (m_context) {
		int w, h;
		GetClientSize(&w, &h);
		SetCurrent(*m_context);
		glViewport(0, 0, (GLint)w, (GLint)h);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(75.0, float(w) / float(h?h:1), 0.01, 1000);
	}
	event.Skip();
}

void GLView::render(wxPaintEvent& )
{
	if (!m_context) return;

	SetCurrent(*m_context);

	glPolygonMode(GL_BACK, GL_LINE);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(m_viewMatrix.data());

	std::chrono::steady_clock::time_point t1, t2;

	t1 = std::chrono::steady_clock::now();
	glPushMatrix();
	glRotatef(90.0, 1.0, 0.0, 0.0);
	m_skybox.draw();
	glPopMatrix();
	t2 = std::chrono::steady_clock::now();
	if (m_verbose) std::cout << "skybox: " << 1000.0 * std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count() << "ms" << std::endl;

	t1 = std::chrono::steady_clock::now();
	//m_mount.free();
	//m_mount.init(m_system->mount(), m_system->ground(), -m_system->nx() / 2.0, -m_system->ny() / 2.0, 2.0 * m_system->nx(), 2.0 * m_system->ny());
	m_mount.draw();
	t2 = std::chrono::steady_clock::now();
	if (m_verbose) std::cout << "mount: " << 1000.0 * std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count() << "ms" << std::endl;

	// vegetation
	
	t1 = std::chrono::steady_clock::now();
	for (auto* p : m_system->forest().plants()) {
		glPushMatrix();
		glTranslatef(p->x(), p->y(), m_system->mount().alt(p->x(), p->y()));
		glScalef(p->width() * p->size(), p->width() * p->size(), p->height() * p->size());
		m_plant.draw(m_vegtex[2 * p->id()], m_vegtex[2 * p->id() + 1]);
		glPopMatrix();
	}
	t2 = std::chrono::steady_clock::now();
	if (m_verbose) std::cout << "vege: " << 1000.0 * std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count() << "ms" << std::endl;
	

	t1 = std::chrono::steady_clock::now();
	if (m_volumic.layerCount() == 0)
		m_bill.draw();
	else
		m_volumic.draw();
	t2 = std::chrono::steady_clock::now();
	if (m_verbose) std::cout << "clouds: " << 1000.0 * std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count() << "ms" << std::endl;

	glFlush();
	SwapBuffers();
}

// EVENTS
void GLView::timer1Elapsed(wxTimerEvent& event)
{
	if (!m_pressed) return;
	
	float speed = 1.0 / 40.0 * event.GetInterval();

	if (m_pressed & (uint)Key::A)
		m_viewMatrix = Matrix4::translate(speed, 0.0, 0.0) * m_viewMatrix;
	if (m_pressed & (uint)Key::D)
		m_viewMatrix = Matrix4::translate(-speed, 0.0, 0.0) * m_viewMatrix;
	if (m_pressed & (uint)Key::W)
		m_viewMatrix = Matrix4::translate(0.0, 0.0, speed) * m_viewMatrix;
	if (m_pressed & (uint)Key::S)
		m_viewMatrix = Matrix4::translate(0.0, 0.0, -speed) * m_viewMatrix;
	if (m_pressed & (uint)Key::R)
		m_viewMatrix = Matrix4::translate(0.0, -speed, 0.0) * m_viewMatrix;
	if (m_pressed & (uint)Key::F)
		m_viewMatrix = Matrix4::translate(0.0, speed, 0.0) * m_viewMatrix;
	if (m_pressed & (uint)Key::Q)
		m_viewMatrix = Matrix4::rotate(-speed * 5.0, 0.0, 0.0, 1.0) * m_viewMatrix;
	if (m_pressed & (uint)Key::E)
		m_viewMatrix = Matrix4::rotate(speed * 5.0, 0.0, 0.0, 1.0) * m_viewMatrix;

	Refresh(false);
}

void GLView::timer2Elapsed(wxTimerEvent& event)
{
	double dt = (double)event.GetInterval() / 1000.0;
	std::chrono::steady_clock::time_point t1, t2;
	t1 = std::chrono::steady_clock::now();
	m_system->evolve(dt);
	t2 = std::chrono::steady_clock::now();
	if (m_verbose) std::cout << "simulation: " << 1000.0 * std::chrono::duration_cast<std::chrono::duration<double>>(t2 - t1).count() << "ms" << std::endl;
	loadSky();
	Refresh(false);
}

void GLView::mousePress(wxMouseEvent& event)
{
	long x, y;
	event.GetPosition(&x, &y);
	m_mouseLastX = x;
	m_mouseLastY = y;
	event.Skip();
}

void GLView::mouseMoved(wxMouseEvent& event)
{
	int dx, dy;
	
	long x, y;
	event.GetPosition(&x, &y);
	
	dx = x - m_mouseLastX; 
	m_mouseLastX = x;
	dy = y - m_mouseLastY; 
	m_mouseLastY = y;

	if (event.LeftIsDown()) {
		m_viewMatrix = Matrix4::rotate(std::sqrt(dx*dx + dy*dy) * 0.2, dy, dx, 0.0) * m_viewMatrix;
		Refresh(false);
	}
	if (event.RightIsDown()) {
		m_viewMatrix = Matrix4::rotate(dx * 0.2, 0.0, 0.0, 1.0) * m_viewMatrix;
		Refresh(false);
	}
	event.Skip();
}

void GLView::keyPressed(wxKeyEvent& event)
{
	switch(event.GetKeyCode()) {
	case 'A':
		m_pressed |= (uint)Key::A;
		break;
	case 'D':
		m_pressed |= (uint)Key::D;
		break;
	case 'W':
		m_pressed |= (uint)Key::W;
		break;
	case 'S':
		m_pressed |= (uint)Key::S;
		break;
	case 'Q':
		m_pressed |= (uint)Key::Q;
		break;
	case 'E':
		m_pressed |= (uint)Key::E;
		break;
	case 'R':
		m_pressed |= (uint)Key::R;
		break;
	case 'F':
		m_pressed |= (uint)Key::F;
		break;
	case 'L':
		m_volumic.setLayerCount(m_volumic.layerCount() + 1);
		cout << "the layer count was increased : " << m_volumic.layerCount() << endl;
		Refresh(false);
		break;
	case 'K':
		if (m_volumic.layerCount() > 0) {
			m_volumic.setLayerCount(m_volumic.layerCount() - 1);
			cout << "the layer count was reduced : " << m_volumic.layerCount() << endl;
			Refresh(false);
		}
		break;
	}
	event.Skip();
}

void GLView::keyReleased(wxKeyEvent& event)
{
	switch(event.GetKeyCode()) {
	case 'A':
		m_pressed &= ~(uint)Key::A;
		break;
	case 'D':
		m_pressed &= ~(uint)Key::D;
		break;
	case 'W':
		m_pressed &= ~(uint)Key::W;
		break;
	case 'S':
		m_pressed &= ~(uint)Key::S;
		break;
	case 'Q':
		m_pressed &= ~(uint)Key::Q;
		break;
	case 'E':
		m_pressed &= ~(uint)Key::E;
		break;
	case 'R':
		m_pressed &= ~(uint)Key::R;
		break;
	case 'F':
		m_pressed &= ~(uint)Key::F;
		break;
	}
	event.Skip();
}

void GLView::setLayerCount(long layerCount)
{
	m_volumic.setLayerCount(static_cast<int>(layerCount));
}

void GLView::setVerbose(bool on)
{
	m_verbose = on;
}

namespace {
float clamp(float x, float min, float max)
{
	return std::max(std::min(x, max), min);
}

float step(float x, float min, float max)
{
	return clamp((x - min) / (max - min), 0.0, 1.0);
}
}

void GLView::loadSky()
{
	m_bill.clear();
	for (int i = 0; i < m_system->nx(); ++i) {
	for (int j = 0; j < m_system->ny(); ++j) {
	for (int k = 0; k < m_system->nz(); ++k) {
		if (m_volumic.layerCount() > 0) {
			GLuint voxel = 0x00FFFFFF; // ABGR
			float alpha = step(m_system->sky().liquidState(i,j,k), 1.0, 1.02);
			voxel += int(0xFF000000 * alpha) & 0xFF000000;
			
			float wet = step(m_system->sky().liquidState(i,j,k), 0.6, 2.0);
			voxel -= int(0x000000FF * wet) & 0x000000FF;
			voxel -= int(0x0000FF00 * wet) & 0x0000FF00;
			voxel -= int(0x00FF0000 * wet) & 0x00FF0000;
			m_volumic.setVoxel(i, j, k, voxel);
		} else {
			if (m_system->sky().liquidState(i,j,k) > 1.0) {
				m_bill.addBillBoard(GLBillBoard(i, j, k, step(m_system->sky().liquidState(i,j,k), 0.9, 1.1)));
			}
		}
	}}}
}
