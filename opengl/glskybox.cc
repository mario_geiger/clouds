#include <wx/mstream.h>
#include "glskybox.hh"
#include "gltexture.hh"

// ------------------------- GLSL shaders ------------------------------
// vertex shader
static const GLchar *vsrc = GLSL(120,
void main(void)
{
	// ignore la partie translation de la matrice MODELVIEW
	gl_Position = gl_ProjectionMatrix * mat4(mat3(gl_ModelViewMatrix)) * gl_Vertex;
	
	// reporte les coordonnées textures pour le fragment shader
	gl_TexCoord[0] = gl_MultiTexCoord0;
}
);

// fragment shader
static const GLchar *fsrc = GLSL(120,
uniform samplerCube tex0;
void main (void)
{
	// utilise l'extention textureCube pour pas s'embeter
	gl_FragColor = textureCube(tex0, gl_TexCoord[0].xyz);
}
); 

GLSkybox::GLSkybox()
{
}

GLSkybox::~GLSkybox() 
{
	glDeleteTextures(6, textures);
}

void GLSkybox::loadTexture(int faceId, unsigned int size, const unsigned char *data) 
{
	wxMemoryInputStream input(data, size);
	glBindTexture(GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceId, textures[faceId]);
	GLTexture::loadTexture(input, GL_TEXTURE_CUBE_MAP_POSITIVE_X + faceId, GLTexture::LinearFiltering);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_REPEAT);
}

void GLSkybox::init()
{
	// génère les texures et crée le program shader
	glGenTextures(6, textures);
	_program.init();
	_program.addVertexSourceCode(vsrc);
	_program.addFragmentSourceCode(fsrc);
	_program.link();
	_program.bind();
	_program.setUniformValue("tex0", 0);
	_program.release();
}

void GLSkybox::draw()
{
	glActiveTexture(GL_TEXTURE0);
	
	static const GLfloat vertices[72] = {
        // -Z
        -1.0, +1.0, -1.0,
        -1.0, -1.0, -1.0,
        +1.0, -1.0, -1.0,
        +1.0, +1.0, -1.0,

        // -X
        -1.0, +1.0, +1.0,
        -1.0, -1.0, +1.0,
        -1.0, -1.0, -1.0,
        -1.0, +1.0, -1.0,

        // +Z
        +1.0, +1.0, +1.0,
        +1.0, -1.0, +1.0,
        -1.0, -1.0, +1.0,
        -1.0, +1.0, +1.0,

        // +X
        +1.0, +1.0, -1.0,
        +1.0, -1.0, -1.0,
        +1.0, -1.0, +1.0,
        +1.0, +1.0, +1.0,

        // -Y
        -1.0, -1.0, -1.0,
        -1.0, -1.0, +1.0,
        +1.0, -1.0, +1.0,
        +1.0, -1.0, -1.0,

        // +Y
        +1.0, +1.0, -1.0,
        +1.0, +1.0, +1.0,
        -1.0, +1.0, +1.0,
        -1.0, +1.0, -1.0
    };

	_program.bind();
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, vertices);
	glClientActiveTexture(GL_TEXTURE0);
	glTexCoordPointer(3, GL_FLOAT, 0, vertices);

	glDepthMask(GL_FALSE);							// ne pas écrire sur le DEPTH BUFFER
	glDrawArrays(GL_QUADS, 0, 24);
	glDepthMask(GL_TRUE);
    
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	_program.release();
}
