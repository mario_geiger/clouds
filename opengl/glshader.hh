#ifndef __GLSHADER_HH__
#define __GLSHADER_HH__
#include <GL/glew.h>

#define GLSL(version, shader)  "#version " #version "\n" #shader

// wrap minimaliste des shaders d'opengl
class GLShader
{
public:
	GLShader();
	GLShader(const GLShader &) = delete;
	~GLShader();
	
	void init();
	void free();
	
	bool addVertexSourceCode(const GLchar *src);
	bool addFragmentSourceCode(const GLchar *src);
	bool link();

	void bind();
	void release();
	
	GLint uniformLocation(const GLchar *name) const;
	
	void setUniformValue(const GLchar *name, GLfloat value);
	void setUniformValue(GLint location, GLfloat value);
	void setUniformValue(const GLchar *name, GLint value);
	void setUniformValue(GLint location, GLint value);
	void setUniformValue(const GLchar *name, GLfloat value0, GLfloat value1, GLfloat value2);
	void setUniformValue(GLint location, GLfloat value0, GLfloat value1, GLfloat value2);
	
private:
	GLuint _vertex;
	GLuint _fragment;
	GLuint _program;
	bool _init;
};

#endif
