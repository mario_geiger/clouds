#include "glmount.hh"
#include "gltexture.hh"
#include "texground.hh"
#include <wx/mstream.h>
#include <cmath>

// ------------------------- GLSL shaders ------------------------------
// vertex shader
static const GLchar *vsrc = GLSL(120,
varying vec3 p;
void main(void)
{
	// envoie la position du sommet au fragment shader par interpolation linéaire
	p = gl_Vertex.xyz;
	
	// applique les matrices de transormation standard
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	
	// envoie la couleur au fragment shader
	gl_FrontColor = gl_Color;
}
);

// fragment shader
static const GLchar *fsrc = GLSL(120,
varying vec3 p;
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;
void main (void)
{
	// récupère les textures (une texture tous les carrés 5x5)
	vec2 tx = p.xy / 5.0;
	vec3 t0 = texture2D(tex0, tx).rgb; // rocky
	vec3 t1 = texture2D(tex1, tx).rgb; // arid
	vec3 t2 = texture2D(tex2, tx).rgb; // fertile

	// combine les texures à l'aide du vecteur couleurs
	gl_FragColor = vec4(gl_Color.r * t0 + gl_Color.g * t1 + gl_Color.b * t2, 1.0);
}
); 

GLMount::GLMount() :
	m_vbo(GL_ARRAY_BUFFER),
	m_ibo(GL_ELEMENT_ARRAY_BUFFER)
{
}

GLMount::~GLMount()
{
	free();
}

void GLMount::free()
{
	glDeleteTextures(3, m_tex);
	m_vbo.destroy();
	m_ibo.destroy();
	m_prog.free();
}

void GLMount::init(const Mount& mount, const Ground& ground, float x, float y, int nx, int ny)
{
	// consruit le program shader
	m_prog.init();
	m_prog.addVertexSourceCode(vsrc);
	m_prog.addFragmentSourceCode(fsrc);
	m_prog.link();

	// charge les 3 textures
	glGenTextures(3, m_tex); 

	// Texture 0
	{
		glBindTexture(GL_TEXTURE_2D, m_tex[0]);
		wxMemoryInputStream input(rock_jpg, rock_jpg_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	// Texture 1
	{
		glBindTexture(GL_TEXTURE_2D, m_tex[1]);
		wxMemoryInputStream input(arid_jpg, arid_jpg_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	// Texture 2
	{
		glBindTexture(GL_TEXTURE_2D, m_tex[2]);
		wxMemoryInputStream input(grass_jpg, grass_jpg_size);
		GLTexture::loadTexture(input, GL_TEXTURE_2D, GLTexture::LinearFiltering | GLTexture::Mipmap);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	
	}
	m_prog.bind();
	m_prog.setUniformValue("tex0", 0);
	m_prog.setUniformValue("tex1", 1);
	m_prog.setUniformValue("tex2", 2);
	m_prog.release();

	float morepoints = 8.0;
	nx *= morepoints;
	ny *= morepoints;

	// précalcule les sommets de la montagne une fois pour toute
	std::vector<float> vertices;
	for (float i = 0; i <= nx; ++i) {
	for (float j = 0; j <= ny; ++j) {
		addVertex(vertices, mount, ground, x + (i - 0.5) / morepoints, y + (j - 0.5) / morepoints);
	}}
	for (float i = 0; i < nx; ++i) {
	for (float j = 0; j < ny; ++j) {
		addVertex(vertices, mount, ground, x + i / morepoints, y + j / morepoints);
	}}
	
	GLuint mid = (nx + 1) * (ny + 1);
	std::vector<GLuint> indices;
	for (int i = 0; i < nx; ++i) {
	for (int j = 0; j < ny; ++j) {
		GLuint id = ny * i + j;
		GLuint id2 = (ny + 1) * i + j;
		// 1
		indices.push_back(mid + id);
		indices.push_back(id2);
		indices.push_back(id2 + ny + 1);
		// 2
		indices.push_back(mid + id);
		indices.push_back(id2 + ny + 1);
		indices.push_back(id2 + ny + 2);
		// 3
		indices.push_back(mid + id);
		indices.push_back(id2 + ny + 2);
		indices.push_back(id2 + 1);
		// 4
		indices.push_back(mid + id);
		indices.push_back(id2 + 1);
		indices.push_back(id2);
	}}
	
	// upload les données sur le GPU une fois pour toute
	m_vbo.create();
	m_vbo.bind();
	m_vbo.allocate(vertices.data(), sizeof (float) * vertices.size());
	m_vbo.release();
		
	m_ibo.create();
	m_ibo.bind();
	m_ibo.allocate(indices.data(), sizeof (GLuint) * indices.size());
	m_ibo.release();
	
	m_count = indices.size();
}

void GLMount::addVertex(std::vector<float>& vs, const Mount& m, const Ground& ground, float x, float y)
{
	float alt = m.alt(x, y);
	// position
	vs.push_back(x);
	vs.push_back(y);
	vs.push_back(alt);
	// color (recyclage de l'attribut couleur)
	Vec3d s = ground.soil(x, y);
	vs.push_back(s.x()); // rocky
	vs.push_back(s.y()); // arid
	vs.push_back(s.z()); // fertile
}

void GLMount::draw()
{
	glEnable(GL_DEPTH_TEST);					// test de profondeur
	glEnable(GL_CULL_FACE);						// dessine pas back face
	glDisable(GL_BLEND);						// desactive mélange (écriture sur le color buffer bit)

	glActiveTexture(GL_TEXTURE0);				// bind les texures
	glBindTexture(GL_TEXTURE_2D, m_tex[0]);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_tex[1]);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_tex[2]);
	
	m_prog.bind();

	glEnableClientState(GL_VERTEX_ARRAY);		// active la gestion des attibuts passé par VBO
	glEnableClientState(GL_COLOR_ARRAY);

	m_vbo.bind();								// indique les adresses memoire des attributs dans le VBO
	glVertexPointer(3, GL_FLOAT, sizeof (float) * (3+3), 0);
	glColorPointer( 3, GL_FLOAT, sizeof (float) * (3+3), BUFFER_OFFSET(sizeof (float) * 3));
	m_vbo.release();
		
	m_ibo.bind();								// active IBO et dessine avec
	glDrawElements(GL_TRIANGLES, m_count, GL_UNSIGNED_INT, 0);
	m_ibo.release();
	
	glDisableClientState(GL_COLOR_ARRAY);
	glDisableClientState(GL_VERTEX_ARRAY);

	m_prog.release();
}
