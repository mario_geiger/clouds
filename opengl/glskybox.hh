#ifndef _SKYBOX_
#define _SKYBOX_

#include <GL/glew.h>
#include <GL/gl.h>
#include "glshader.hh"

// cette classe dessine un skybox avec opengl
// à l'aide d'un cube 2x2x2 centré à l'origine sans écrire de le tampon de profondeur (comme si cube de taille infini)
class GLSkybox {
public :
	GLSkybox();
	~GLSkybox();
	
	void loadTexture(int faceId, unsigned int size, const unsigned char *data);
	void init();
	void draw();

private :
	GLuint textures[6];
	GLShader _program;
};

#endif
