#ifndef _VECTEUR2D_
#define _VECTEUR2D_

		/* Remarque : cette classe a été la toute première classe des vecteurs en deux
		 * dimension (exercice [P1]. Elle a été abandonnée et complêtement réécrite 
		 * pour l'exercice [P3] : voir classe Vec2.
		 */

class Vecteur2D {
	private:
		//attributs : les 2 composantes x et y
		double _x;
		double _y;
		
	public:
		//Méthodes
		void affiche() const;
		bool compare(const Vecteur2D& autre) const;
		
		//Méthodes "get"
		double x() const;
		double y() const;
		
		//Méthodes "set"
		void x(const double& nb);
		void y(const double& nb);
		
		//Méthodes mathématiques
		Vecteur2D addition(const Vecteur2D& autre) const;
		Vecteur2D soustraction(const Vecteur2D& autre) const;
		Vecteur2D oppose() const;
		Vecteur2D mult(const double&) const;
		double prod_scal(const Vecteur2D& autre) const;
		double norme() const;
		double normecarre() const;
};

#endif
