#ifndef __Vec3__
#define __Vec3__

#include <iostream>
#include <cmath>

template<typename T>
class Vec3;

typedef Vec3<float> Vec3f;
typedef Vec3<double> Vec3d;

template<typename T>
class Vec3
{
private:
	T _x, _y, _z;
	
public:
	// constructeurs
	Vec3();
	Vec3(T __x, T __y, T __z);

	// getters et setters
	inline T x() const { return _x; }
	inline T y() const { return _y; }
	inline T z() const { return _z; }
	inline T x(T __x) { return _x = __x; }
	inline T y(T __y) { return _y = __y; }
	inline T z(T __z) { return _z = __z; }

	// comparaison
	bool operator ==(const Vec3<T> &v) const;
	bool operator !=(const Vec3<T> &v) const;

	// addition et soustraction
	Vec3 &operator +=(const Vec3<T> &v);
	Vec3 &operator -=(const Vec3<T> &v);
	Vec3 operator -() const;

	// amplification
	template<typename K>
	Vec3 &operator *=(K k);
	template<typename K>
	Vec3 &operator /=(K k);

	// norme
	T lengthSquared() const;
	T length() const;
	
	// normalization
	Vec3 normalized() const;

	// produit scalaire
	template<typename U>
	friend U dot(const Vec3<U> &u, const Vec3<U> &v);

	// produit vectoriel
	template<typename U>
	friend Vec3<U> cross(const Vec3<U> &u, const Vec3<U> &v);

	// addition et soustraction
	template<typename U>
	friend Vec3<U> operator + (Vec3<U> u, const Vec3<U> &v);
	template<typename U>
	friend Vec3<U> operator - (Vec3<U> u, const Vec3<U> &v);

	// amplification par un scalaire
	template<typename U, typename K>
	friend Vec3<U> operator * (Vec3<U> u, K k);
	template<typename U, typename K>
	friend Vec3<U> operator * (K k, Vec3<U> v);
	template<typename U, typename K>
	friend Vec3<U> operator / (Vec3<U> u, K k);

	// surcharge pour cout << 
	template<typename U>
	friend std::ostream &operator << (std::ostream &out, const Vec3<U> &v);
};

// implementation inline
template<typename T>
inline Vec3<T>::Vec3()
    : _x(), _y(), _z()
{
}

template<>
inline Vec3f::Vec3()
    : _x(0.0), _y(0.0), _z(0.0)
{
}

template<>
inline Vec3d::Vec3()
    : _x(0.0), _y(0.0), _z(0.0)
{
}

template<typename T>
inline Vec3<T>::Vec3(T __x, T __y, T __z)
    : _x(__x), _y(__y), _z(__z)
{
}

template<typename T>
inline bool Vec3<T>::operator ==(const Vec3<T> &v) const
{
    return _x == v._x && _y == v._y && _z == v._z;
}

template<typename T>
inline bool Vec3<T>::operator !=(const Vec3<T> &v) const
{
    return _x != v._x || _y != v._y || _z != v._z;
}

template<typename T>
inline Vec3<T> &Vec3<T>::operator +=(const Vec3<T> &v)
{
    _x += v._x; _y += v._y; _z += v._z;
    return *this;
}

template<typename T>
inline Vec3<T> &Vec3<T>::operator -=(const Vec3<T> &v)
{
    _x -= v._x; _y -= v._y; _z -= v._z;
    return *this;
}

template<typename T>
inline Vec3<T> Vec3<T>::operator -() const
{
    return Vec3<T>(-_x, -_y, -_z);
}

template<typename T>
template<typename K>
inline Vec3<T> &Vec3<T>::operator *=(K k)
{
	_x *= k; _y *= k; _z *= k;
	return *this;
}

template<typename T>
template<typename K>
inline Vec3<T> &Vec3<T>::operator /=(K k)
{
	_x /= k; _y /= k; _z /= k;
	return *this;
}

template<typename T>
inline T Vec3<T>::lengthSquared() const
{
    return _x * _x + _y * _y + _z * _z;
}

template<typename T>
inline T Vec3<T>::length() const
{
    return std::sqrt(lengthSquared());
}

template<typename T>
Vec3<T> Vec3<T>::normalized() const
{
	T l = length();
	if (l == 0.0)
		return Vec3d(0.0, 0.0, 0.0);
	else
		return *this / l;
}

template<typename U>
inline U dot(const Vec3<U> &u, const Vec3<U> &v)
{
	return u._x * v._x + u._y * v._y + u._z * v._z;
}

template<typename U>
inline Vec3<U> cross(const Vec3<U> &u, const Vec3<U> &v)
{
	return Vec3<U>(u._y * v._z - v._y * u._z
	             , v._x * u._z - u._x * v._z
	             , u._x * v._y - v._x * u._y);
}

template<typename U>
inline Vec3<U> operator +(Vec3<U> u, const Vec3<U> &v)
{
	return u += v;
}

template<typename U>
inline Vec3<U> operator -(Vec3<U> u, const Vec3<U> &v)
{
	return u -= v;
}

template<typename U, typename K>
inline Vec3<U> operator *(Vec3<U> u, K k)
{
	return u *= k;
}

template<typename U, typename K>
inline Vec3<U> operator *(K k, Vec3<U> v)
{
	return v *= k;
}

template<typename U, typename K>
inline Vec3<U> operator /(Vec3<U> u, K k)
{
	return u /= k;
}

template<typename U>
inline std::ostream &operator <<(std::ostream &out, const Vec3<U> &v)
{
    return out << '(' << v._x << ", " << v._y << ", " << v._z << ')';
}

#endif
