#ifndef __VEC2__
#define __VEC2__

#include <iostream>
#include <cmath>

		/* Nouvelle classe pour les vecteurs en deux dimensions, avec surcharge
		 * d'oppérateurs.
		 */

class Vec2 {
	public:
			//Constructeurs (dont par défaut, et de copie)
		Vec2(double x, double y);
		Vec2();
		Vec2(const Vec2& other);
		
			//Conversion coordonnées polaires -> cartésiennes
		inline static Vec2 polar(double rho, double theta = 0.0);
		
			//Méthodes "get"
		inline double x() const { return _x; }
		inline double y() const { return _y; }
		
			//Méthodes "set"
		inline double x(double __x) { _x = __x; return __x; }
		inline double y(double __y) { _y = __y; return __y; }
		
			//Opérateurs de comparaison et d'affectation
		bool operator==(const Vec2& other) const;
		bool operator!=(const Vec2& other) const;
		
			//Opérateurs mathématiques
		Vec2& operator+=(const Vec2& other);
		friend Vec2 operator+(Vec2 vect1, const Vec2& vect2);
		Vec2 operator-() const;
		Vec2& operator-=(const Vec2& other);
		friend Vec2 operator-(Vec2 vect1, const Vec2& vect2);
		Vec2& operator*=(double nb);
		friend Vec2 operator*(Vec2 vect, double nb);
		friend Vec2 operator*(double nb, Vec2 vect);
		double operator*(const Vec2& other) const;
		
			//Méthodes pour la norme et la norme carrée
		double norm() const;
		double square_norm() const;
		
			//Opérateur << surchargé en externe
		friend std::ostream& operator<<(std::ostream& out, const Vec2& vect);
												
	private:
			//Attributs : composantes cartésiennes
		double _x;
		double _y;	
};

inline Vec2::Vec2(double x, double y) : _x(x), _y(y) {}
inline Vec2::Vec2() : Vec2(0.0,0.0) {}
inline Vec2::Vec2(const Vec2& other) : Vec2(other._x, other._y) {}

inline Vec2 Vec2::polar(double rho, double theta)
{
	return rho * Vec2(std::cos(theta), std::sin(theta));
}

inline bool Vec2::operator==(const Vec2& other) const { return _x == other._x && _y == other._y; }
inline bool Vec2::operator!=(const Vec2& other) const { return !(*this == other); }

inline Vec2& Vec2::operator+=(const Vec2& other) {
	_x += other._x;
	_y += other._y;
	return *this;
}
inline Vec2 operator+(Vec2 vect1, const Vec2& vect2) { return vect1 += vect2; }
inline Vec2 Vec2::operator-() const { return Vec2(-_x,-_y); }
inline Vec2& Vec2::operator-=(const Vec2& other) {
	_x -= other._x;
	_y -= other._y;
	return *this;
}
inline Vec2 operator-(Vec2 vect1, const Vec2& vect2) { return vect1 -= vect2; }
inline Vec2& Vec2::operator*=(double nb) { 
	_x *= nb;
	_y *= nb;
	return *this;
}
inline Vec2 operator*(Vec2 vect, double nb) {
	return vect*= nb;
}
inline Vec2 operator*(double nb, Vec2 vect) {
	return vect * nb;
}
inline double Vec2::operator*(const Vec2& other) const {
	return _x*other._x + _y*other._y;
}
inline double Vec2::square_norm() const {
	return *this * *this;
}
inline double Vec2::norm() const {
	return sqrt(_x*_x + _y*_y);
}

inline std::ostream& operator<<(std::ostream& out, const Vec2& vect) {
	out << "(" << vect._x << ", " << vect._y << ")";
	return out;
}

#endif
