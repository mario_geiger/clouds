#include <iostream>
#include <cmath>
using namespace std;

#include "vecteur2D.hh"

	//Définition des méthodes de la class Vecteur2D

void Vecteur2D::affiche() const {
	cout << _x << "  " << _y;
}
bool Vecteur2D::compare(const Vecteur2D& b) const {
	return (_x == b._x) && (_y == b._y);
}

double Vecteur2D::x() const {
	return _x;
}
double Vecteur2D::y() const {
	return _y;
}

void Vecteur2D::x(const double& nb) {
	_x = nb;
}
void Vecteur2D::y(const double& nb) {
	_y = nb;
}

Vecteur2D Vecteur2D::addition(const Vecteur2D& autre) const {
	Vecteur2D reponse;
	reponse._x = _x + autre._x;
	reponse._y = _y + autre._y;
	return reponse;
}
Vecteur2D Vecteur2D::soustraction(const Vecteur2D& autre) const {
	Vecteur2D reponse;
	reponse._x = _x - autre._x;
	reponse._y = _y - autre._y;
	return reponse;
}
Vecteur2D Vecteur2D::oppose() const {
	Vecteur2D reponse;
	reponse._x = -_x;
	reponse._y = -_y;
	return reponse;
}
Vecteur2D Vecteur2D::mult(const double& nb) const {
	Vecteur2D reponse;
	reponse._x = nb*_x;
	reponse._y = nb*_y;
	return reponse;
}
double Vecteur2D::prod_scal(const Vecteur2D& autre) const {
	return (_x*autre._x) + (_y*autre._y);
}
double Vecteur2D::norme() const {
	return sqrt(prod_scal(*this));
}
double Vecteur2D::normecarre() const {
	return prod_scal(*this);
}
