#ifndef __MATRIX4__
#define __MATRIX4__

#include "vec3.hh"

class Matrix4 {
public:
	Matrix4();
	//Matrix4 &operator =(const Matrix4& other);
	static Matrix4 scale(float x, float y, float z);
	static Matrix4 translate(float x, float y, float z);
	static Matrix4 rotate(float angle, float x, float y, float z);
	float &operator ()(int i, int j);
	float operator ()(int i, int j) const;
	const Vec3f operator *(const Vec3f &v) const;
	Matrix4 &operator *=(const Matrix4 &b);
	const float *data() const;

private:
	float m[4][4];
	// m[0] : 1st column
	// m[1] : 2nd column
	// ...
};

inline const Matrix4 operator *(Matrix4 a, const Matrix4 &b) {
	return a *= b;
}

inline Matrix4::Matrix4() 
{
	m[0][0] = 1.0; m[1][0] = 0.0; m[2][0] = 0.0; m[3][0] = 0.0;
	m[0][1] = 0.0; m[1][1] = 1.0; m[2][1] = 0.0; m[3][1] = 0.0;
	m[0][2] = 0.0; m[1][2] = 0.0; m[2][2] = 1.0; m[3][2] = 0.0;
	m[0][3] = 0.0; m[1][3] = 0.0; m[2][3] = 0.0; m[3][3] = 1.0;
}

inline float &Matrix4::operator ()(int i, int j)
{
	return m[j][i];
}

inline float Matrix4::operator ()(int i, int j) const
{
	return m[j][i];
}

inline const Vec3f Matrix4::operator *(const Vec3f &v) const
{
	return Vec3f(m[0][0] * v.x() + m[1][0] * v.y() + m[2][0] * v.z() + m[3][0]
	           , m[0][1] * v.x() + m[1][1] * v.y() + m[2][1] * v.z() + m[3][1]
	           , m[0][2] * v.x() + m[1][2] * v.y() + m[2][2] * v.z() + m[3][2]);
}

inline const float *Matrix4::data() const
{
	return *m;
}

#endif
