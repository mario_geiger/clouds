#include <iostream>
using namespace std;

#include "vecteur2D.hh"

int main() {		/*Fichier de test de la classe Vecteur2D; exercice [P1]*/
	Vecteur2D vect1;
	Vecteur2D vect2;
	Vecteur2D vect3;
	
	vect1.x(1.0); vect1.y(2.0);
	vect2.x(2.6); vect2.y(3.5);
	
	vect3 = vect1;
	
	cout << "Vecteur 1 : ";
	vect1.affiche();
	cout << endl;

	cout << "Vecteur 2 : ";
	vect2.affiche();
	cout << endl;

	cout << "Le vecteur 1 est ";
	if (vect1.compare(vect2)) {
		cout << "�gal au";
	} else {
		cout << "diff�rent du";
	}
	cout << " vecteur 2," << endl << "et est ";
	if (not vect1.compare(vect3)) {
		cout << "diff�rent du";
	} else {
		cout << "�gal au";
	}
	cout << " vecteur 3." << endl;
	
	cout << "("; vect1.affiche(); cout << ") + ("; vect2.affiche(); cout << ") = ("; vect1.addition(vect2).affiche(); cout << ")" << endl;
	cout << "("; vect2.affiche(); cout << ") + ("; vect1.affiche(); cout << ") = ("; vect2.addition(vect1).affiche(); cout << ")" << endl;
	vect3.x(0.0); vect3.y(0.0);
	cout << "("; vect1.affiche(); cout << ") + ("; vect3.affiche(); cout << ") = ("; vect1.addition(vect3).affiche(); cout << ")" << endl;
	cout << "("; vect3.affiche(); cout << ") + ("; vect1.affiche(); cout << ") = ("; vect3.addition(vect1).affiche(); cout << ")" << endl;
	cout << "("; vect1.affiche(); cout << ") - ("; vect2.affiche(); cout << ") = ("; vect1.soustraction(vect2).affiche(); cout << ")" << endl;
	cout << "("; vect2.affiche(); cout << ") - ("; vect2.affiche(); cout << ") = ("; vect2.soustraction(vect2).affiche(); cout << ")" << endl;
	cout << "- ("; vect1.affiche(); cout << ") = ("; vect1.oppose().affiche(); cout << ")" << endl;
	cout << "- ("; vect2.affiche(); cout << ") + ("; vect1.affiche(); cout << ") = ("; vect2.oppose().addition(vect1).affiche(); cout << ")" << endl;
	cout << "3 * ("; vect1.affiche(); cout << ") = ("; vect1.mult(3.0).affiche(); cout << ")" << endl;
	cout << "("; vect1.affiche(); cout << ") * ("; vect2.affiche(); cout << ") = " << vect1.prod_scal(vect2) << endl;
	cout << "("; vect2.affiche(); cout << ") * ("; vect1.affiche(); cout << ") = " << vect2.prod_scal(vect1) << endl;
	cout << "||"; vect1.affiche(); cout << "||� = " << vect1.normecarre() << endl;
	cout << "||"; vect1.affiche(); cout << "|| = " << vect1.norme() << endl;
	cout << "||"; vect2.affiche(); cout << "||� = " << vect2.normecarre() << endl;
	cout << "||"; vect2.affiche(); cout << "|| = " << vect2.norme() << endl;
	
	return 0;
}
