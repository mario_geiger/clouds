#include "matrix4.hh"
#include <cmath>

Matrix4 Matrix4::scale(float x, float y, float z)
{
	Matrix4 sca;
	sca.m[0][0] = x;
	sca.m[1][1] = y;
	sca.m[2][2] = z;
	return sca;
}

Matrix4 Matrix4::translate(float x, float y, float z)
{
	 Matrix4 tra;
	 tra.m[3][0] = x;
	 tra.m[3][1] = y;
	 tra.m[3][2] = z;
	 return tra;
}

Matrix4 Matrix4::rotate(float angle, float x, float y, float z)
{
	// http://fr.wikipedia.org/wiki/Matrice_de_rotation#Matrices_de_rotation_dans_le_cas_g.C3.A9n.C3.A9ral
	float a = angle * M_PI / 180.0;
	float c = cosf(a);
	float s = sinf(a);
	double len = double(x) * double(x) +
	             double(y) * double(y) +
	             double(z) * double(z);
    if (len != 1.0 && len != 0.0) {
		len = sqrt(len);
		x = float(double(x) / len);
		y = float(double(y) / len);
		z = float(double(z) / len);
	}
	float ic = 1.0f - c;
	Matrix4 rot;
	rot.m[0][0] = x * x * ic + c;
	rot.m[1][0] = x * y * ic - z * s;
	rot.m[2][0] = x * z * ic + y * s;

	rot.m[0][1] = y * x * ic + z * s;
	rot.m[1][1] = y * y * ic + c;
	rot.m[2][1] = y * z * ic - x * s;

	rot.m[0][2] = x * z * ic - y * s;
	rot.m[1][2] = y * z * ic + x * s;
	rot.m[2][2] = z * z * ic + c;

	return rot;
}

Matrix4 &Matrix4::operator *=(const Matrix4 &other)
{
	float m0, m1, m2;
	m0 = m[0][0] * other.m[0][0]
	   + m[1][0] * other.m[0][1]
	   + m[2][0] * other.m[0][2]
	   + m[3][0] * other.m[0][3];
	m1 = m[0][0] * other.m[1][0]
	   + m[1][0] * other.m[1][1]
	   + m[2][0] * other.m[1][2]
	   + m[3][0] * other.m[1][3];
	m2 = m[0][0] * other.m[2][0]
	   + m[1][0] * other.m[2][1]
	   + m[2][0] * other.m[2][2]
	   + m[3][0] * other.m[2][3];
	m[3][0] = m[0][0] * other.m[3][0]
	        + m[1][0] * other.m[3][1]
	        + m[2][0] * other.m[3][2]
	        + m[3][0] * other.m[3][3];
	m[0][0] = m0;
	m[1][0] = m1;
	m[2][0] = m2;

	m0 = m[0][1] * other.m[0][0]
	   + m[1][1] * other.m[0][1]
	   + m[2][1] * other.m[0][2]
	   + m[3][1] * other.m[0][3];
	m1 = m[0][1] * other.m[1][0]
	   + m[1][1] * other.m[1][1]
	   + m[2][1] * other.m[1][2]
	   + m[3][1] * other.m[1][3];
	m2 = m[0][1] * other.m[2][0]
	   + m[1][1] * other.m[2][1]
	   + m[2][1] * other.m[2][2]
	   + m[3][1] * other.m[2][3];
	m[3][1] = m[0][1] * other.m[3][0]
	   + m[1][1] * other.m[3][1]
	   + m[2][1] * other.m[3][2]
	   + m[3][1] * other.m[3][3];
	m[0][1] = m0;
	m[1][1] = m1;
	m[2][1] = m2;

	m0 = m[0][2] * other.m[0][0]
	   + m[1][2] * other.m[0][1]
	   + m[2][2] * other.m[0][2]
	   + m[3][2] * other.m[0][3];
	m1 = m[0][2] * other.m[1][0]
	   + m[1][2] * other.m[1][1]
	   + m[2][2] * other.m[1][2]
	   + m[3][2] * other.m[1][3];
	m2 = m[0][2] * other.m[2][0]
	   + m[1][2] * other.m[2][1]
	   + m[2][2] * other.m[2][2]
	   + m[3][2] * other.m[2][3];
	m[3][2] = m[0][2] * other.m[3][0]
	        + m[1][2] * other.m[3][1]
	        + m[2][2] * other.m[3][2]
	        + m[3][2] * other.m[3][3];
	m[0][2] = m0;
	m[1][2] = m1;
	m[2][2] = m2;

	m0 = m[0][3] * other.m[0][0]
	   + m[1][3] * other.m[0][1]
	   + m[2][3] * other.m[0][2]
	   + m[3][3] * other.m[0][3];
	m1 = m[0][3] * other.m[1][0]
	   + m[1][3] * other.m[1][1]
	   + m[2][3] * other.m[1][2]
	   + m[3][3] * other.m[1][3];
	m2 = m[0][3] * other.m[2][0]
	   + m[1][3] * other.m[2][1]
	   + m[2][3] * other.m[2][2]
	   + m[3][3] * other.m[2][3];
	m[3][3] = m[0][3] * other.m[3][0]
	        + m[1][3] * other.m[3][1]
	        + m[2][3] * other.m[3][2]
	        + m[3][3] * other.m[3][3];
	m[0][3] = m0;
	m[1][3] = m1;
	m[2][3] = m2;
	return *this;
}
