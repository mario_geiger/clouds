#include "vec2.hh"
#include <iostream>
using namespace std;

int main() {		/*Fichier de test de la classe vec2, exercice [P3]*/
	Vec2 vect1(1.0, 2.0);
	Vec2 vect2(2.6, 3.5);
	Vec2 vect3(vect1);
	Vec2 vect4;
	
	cout << "Vecteur 1 : " << vect1 << endl;
	cout << "Vecteur 2 : " << vect2 << endl;
	cout << "Vecteur 3 : " << vect3 << endl;
	cout << "Vecteur 4 : " << vect4 << endl;
	
	cout << "Le vecteur 1 est ";
	if (vect1 == vect2) {
	    cout << "égal au";
	} else {
	    cout << "différent du";
	}
	cout << " vecteur 2," << endl << "et est ";
	if (vect1 != vect3) {
	    cout << "différent du";
	} else {
	    cout << "égal au";
	}
	cout << " vecteur 3." << endl;
	
	vect4 = vect1; cout << vect4 << endl;
	
	cout << vect1 << " + " << vect2 << " = " << vect1 + vect2 << endl;
	cout << vect2 << " + " << vect1 << " = " << vect2 + vect1 << endl;
	cout << "-" << vect1 << " = " << -vect1 << endl;
	cout << vect1 << " - " << vect2 << " = " << vect1 - vect2 << endl;
	cout << vect2 << " - " << vect1 << " = " << vect2 - vect1 << endl;
	cout << vect1 << " x 3 = " << vect1 * 3 << endl;
	cout << "3 x " << vect1 << " = " << 3 * vect1 << endl;
	cout << vect1 << " * " << vect2 << " = " << vect1 * vect2 << endl;
	cout << vect2 << " * " << vect1 << " = " << vect2 * vect1 << endl;
	cout << "||" << vect1 << "||² = " << vect1.square_norm() << endl;
	cout << "||" << vect1 << "|| = " << vect1.norm() << endl;
}
