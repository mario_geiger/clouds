set terminal pdf
set output 'graph_wind.pdf'

a = 5e-2
unset key
set size square
splot 'wind.dat' using 1:2:3:($9 > 700 ? a*$6 : 1/0):(a*$7):(a*$8) with vect
