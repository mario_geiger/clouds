  a    = .05                  ; # Cette variable contrôle la taille des flèches sur la figure
  file = 'potential.dat'      ; # nom du fichier à dessiner
  z    = 10                    ; # altitude du plan de dessin
  set size square ; unset key ;
  set terminal pdf
  set output 'graph_potential.pdf'
  p [-1:30][-1:30] file u ($3 == z ? $1 :1/0):2:(a*$4):(a*$5) w vectors head filled
