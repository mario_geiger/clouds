set terminal pdf
set output "graph_mountrange.pdf"

unset key
set pm3d map
set size square
splot [0:29][0:29] "mountrange.dat"
