set terminal pdf
set output 'graph_sky.pdf'

file = 'sky.dat'

splot file u 1:2:3:($11 > 0 ? $11 : 1/0) notitle w p
