#include "parser.hh"
#include "mountsimple.hh"
#include "mountraw.hh"
#include "mountrange.hh"
#include "stringutil.hh"
#include <iostream>
#include <fstream>
using namespace std;

Parser::Parser(System* sys) :
	m_sys(sys)
{
}

bool Parser::parse(std::string fileName)
{	
	fstream file(fileName, fstream::in);
	if (!file.is_open()) {
		cerr << "error : cannot open the file : " << fileName << endl;
		return false;
	}
	if (!DOMXmlReader::parse(&file)) {
		cerr << "error : " << line() << ":" << column() << " " << errorString() << endl;
		return false;
	}
	file.close();
	DOMXmlElement e = rootElement();
	if (e.childrenCount() != 1) {
		cerr << "error : no root element in document" << endl;
		return false;
	}
	e = e.child(0);
	if (e.tagname() != "system") {
		cerr << "error : the root element need to be \"system\"" << endl;
		return false;
	}
	double d1;
	if (e.attributeValue("lambda", &d1)) {
		m_sys->set_lambda(d1);
	} else {
		cerr << "warning : no lambda attribute given" << endl;
	}
	int i1, i2, i3;
	if (e.attributeValue("size", &i1, &i2, &i3)) {
		m_sys->set_size(i1, i2, i3);
	} else {
		cerr << "warning : no size attribute given" << endl;
	}
	if (e.attributeValue("plainWindSpeed", &d1)) {
		m_sys->set_plain_wind_speed(d1);
	} else {
		cerr << "warning : no plainWindSpeed attribute given" << endl;
	}
	if (e.attributeValue("plainTemperature", &d1)) {
		m_sys->set_plain_temperature(d1);
	} else {
		cerr << "warning : no plainTemperature attribute given" << endl;
	}
	if (e.attributeValue("plainPressure", &d1)) {
		m_sys->set_plain_pressure(d1);
	} else {
		cerr << "warning : no plainPressure attribute given" << endl;
	}
	if (e.attributeValue("humidity", &d1)) {
		m_sys->set_humidity(d1);
	} else {
		cerr << "warning : no humidity attribute given" << endl;
	}
	if (e.attributeValue("rockLimit", &d1)) {
		m_sys->set_rock_limit(d1);
	} else {
		cerr << "warning : no rockLimit attribute given" << endl;
	}
	if (e.attributeValue("epsilon", &d1)) {
		m_sys->set_epsilon(d1);
	} else {
		cerr << "warning : no epsilon attribute given" << endl;
	}
	if (e.attributeValue("precision", &d1)) {
		m_sys->set_precision(d1);
	} else {
		cerr << "warning : no precision attribute given" << endl;
	}
	if (e.attributeValue("maxiter", &i1)) {
		m_sys->set_max_iter(i1);
	} else {
		cerr << "warning : no maxiter attribute given" << endl;
	}
	
	
	
	if (e.childrenCount() == 0) {
		cerr << "error : nothing in the system element" << endl;
		return false;
	}
	DOMXmlElement m = e.child(0);
	if (m.tagname() == "mountrange" || m.tagname() == "mountsimple" || m.tagname() == "mountraw")
		if (!parseMount(m, nullptr))
			return false;

	return true;
}

bool Parser::parseMount(const DOMXmlElement& m, MountRange *range)
{
	if (m.tagname() == "mountsimple") {
		double positionx, positiony, alt, spreadx, spready;
		if (m.attributeValue("position", &positionx, &positiony) &&
		    m.attributeValue("alt", &alt) &&
		    m.attributeValue("spread", &spreadx, &spready)) {
			MountSimple simple(positionx, positiony, alt, spreadx, spready);
			if (range == nullptr)
				m_sys->set_mount(simple);
			else
				range->addMount(simple);
			return true;
		} else {
			cerr << "error : cannot create a simple mountain without position, alt and spread" << endl;
			return false;
		}
	}
	
	if (m.tagname() == "mountraw" && m.cdsectCount() == 1) {
		int nx, ny;
		if (m.attributeValue("size", &nx, &ny)) {
			MountRaw raw(nx, ny);
			vector<string> lines = split(m.cdsect(0), '\n');
			for (auto l : lines) {
				vector<string> t = splitws(l);
				if (t.size() == 0) continue;
				if (t.size() != 3) {
					cerr << "error : wrong format, <x> <y> <alt> needed \"" << l << "\"" << endl;
					return false;
				}
				bool ok1, ok2, ok3;
				int x = stringtointeger(t[0], &ok1);
				int y = stringtointeger(t[1], &ok2);
				double alt = stringtonumber(t[2], &ok3);
				if (!ok1 || !ok2 || !ok3) {
					cerr << "error : this are not numbers \"" << l << "\"" << endl;
					return false;
				}
				if (x < 0 || x >= nx || y < 0 || y >= ny) {
					cerr << "error : data out of range" << endl;
					return false;
				}
				raw.setAlt(x, y, alt);
			}
			if (range == nullptr)
				m_sys->set_mount(raw);
			else
				range->addMount(raw);
			return true;
		} else {
			cerr << "error : cannot create a mountraw without size" << endl;
			return false;
		}
	}
	
	if (m.tagname() == "mountrange") {
		MountRange range2;
		for (size_t i = 0; i < m.childrenCount(); ++i) {
			if (!parseMount(m.child(i), &range2))
				return false;
		}
		if (range == nullptr)
			m_sys->set_mount(range2);
		else
			range->addMount(range2);
		return true;
	}
	
	cerr << "error : wrong tagname mountain expected" << endl;
	return false;
}
