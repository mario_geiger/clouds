#ifndef __PARSER_HH__
#define __PARSER_HH__

#include "xml.hh"
#include "system.hh"
#include "mountrange.hh"
#include <vector>

class Parser : public DOMXmlReader
{
public:
	Parser(System* sys);
	bool parse(std::string fileName);
	
private:	
	bool parseMount(const DOMXmlElement& m, MountRange *range);
	
	System* m_sys;
};

#endif
