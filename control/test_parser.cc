#include "parser.hh"
#include "system.hh"
#include <iostream>
using namespace std;

int main()
{
	System s;
	
	Parser p(&s);
	if (!p.parse("simple.xml"))
		return 1;
	
	s.init(false);
	
	cout << s.sky();
	//View v(&s);
	
	return 0;
}
