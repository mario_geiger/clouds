#include "gltopography.h"

GLTopography::GLTopography(QObject *parent)
    : QObject(parent),
      _vbo(QGLBuffer::VertexBuffer),
      _ibo(QGLBuffer::IndexBuffer)
{
}

#define GLSL(version, shader)  "#version " #version "\n" #shader
#define PROGRAM_VERTEX_ATTRIBUTE 0
#define PROGRAM_TEXCOO_ATTRIBUTE 1
#define BUFFER_OFFSET(a) ((char*)NULL + (a))

static const char *vertsrc = GLSL(130,
   in vec3 vertex;
   in vec2 texCoord;

   uniform mat4 matrixp; // projection
   uniform mat4 matrixv; // view (camera)
   uniform mat4 matrixm; // model (position of object in scene)

   out vec2 t;

   void main(void)
   {
       t = texCoord;
       gl_Position = matrixp * matrixv * matrixm * vec4(vertex, 1.0);
   }
);

static const char *fragsrc = GLSL(130,
    in vec2 t;

    uniform sampler2D texname;

    out vec4 color;

    void main(void)
    {
        color = texture(texname, t);
    }
);

void GLTopography::initializeGL(const QGLContext *context)
{
    _program = new QGLShaderProgram(context, this);
    _program->addShaderFromSourceCode(QGLShader::Vertex, vertsrc);
    _program->addShaderFromSourceCode(QGLShader::Fragment, fragsrc);
    _program->bindAttributeLocation("vertex", PROGRAM_VERTEX_ATTRIBUTE);
    _program->bindAttributeLocation("texCoord", PROGRAM_TEXCOO_ATTRIBUTE);
    _program->link();

    _modelLocation = _program->uniformLocation("matrixm");
    _viewLocation = _program->uniformLocation("matrixv");
    _projectionLocation = _program->uniformLocation("matrixp");

    setProjection(QMatrix4x4());
    setView(QMatrix4x4());
    setModel(QMatrix4x4());

    _program->bind();
    _program->setUniformValue("texname", 0);
    _program->release();
}

void GLTopography::initializeBuffer(const Topography &topo, double lambda, int nx, int ny)
{
    QVector<GLfloat> vs;

    // initialisation des "vertices", les attributs du "vertex shader"
    double ly = lambda * (ny - 1);
    for (int xi = 0; xi < nx; ++xi) {
        double x = xi * lambda;
        for (int yi = 0; yi < ny; ++yi) {
            double y = yi * lambda - ly / 2.0;
            vs << x << y << topo.altitude(x, y);
            vs << GLfloat(xi) / GLfloat(nx - 1) << GLfloat(yi) / GLfloat(ny - 1);
        }
    }

    _vbo.create(); // creation du vertex buffer object
    _vbo.bind(); // activation du vbo pour travailler dessus (OpenGL est une machine a etats)
    _vbo.allocate(vs.constData(), vs.size() * sizeof (GLfloat)); // allocation de la memoire et copie des donnees
    _vbo.release(); // desactivation


    QVector<GLuint> is;

    // calcul des indices pour glDrawElements avec le mode GL_TRIANGLE_STRIP
    for (int xi = 1; xi < nx; ++xi) {
        for (int yi = 0; yi < ny; ++yi) {
            is << (xi - 1) * ny + yi;
            is << xi * ny + yi;
        }
    }

    _ibo.create();
    _ibo.bind();
    _ibo.allocate(is.constData(), is.size() * sizeof (GLuint));
    _ibo.release();

    _nx = nx;
    _ny = ny;
}

void GLTopography::drawGL(GLuint textureId)
{
    glEnable(GL_TEXTURE_2D); // texture
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
//    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    glDisable(GL_BLEND);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    _program->bind();

    _vbo.bind();
    _program->setAttributeBuffer(PROGRAM_VERTEX_ATTRIBUTE, GL_FLOAT, 0,                    3, 5 * sizeof (GLfloat));
    _program->setAttributeBuffer(PROGRAM_TEXCOO_ATTRIBUTE, GL_FLOAT, 3 * sizeof (GLfloat), 2, 5 * sizeof (GLfloat));
    _vbo.release();

    _program->enableAttributeArray(PROGRAM_VERTEX_ATTRIBUTE);
    _program->enableAttributeArray(PROGRAM_TEXCOO_ATTRIBUTE);

    _ibo.bind();
    for (int xi = 0; xi < _nx - 1; ++xi) {
        glDrawElements(GL_QUAD_STRIP, 2 * _ny, GL_UNSIGNED_INT, BUFFER_OFFSET(xi * 2 * _ny * sizeof (GLuint)));
    }
    _ibo.release();

    _program->disableAttributeArray(PROGRAM_VERTEX_ATTRIBUTE);
    _program->disableAttributeArray(PROGRAM_TEXCOO_ATTRIBUTE);

    _program->release();
}

void GLTopography::setModel(const QMatrix4x4 &m)
{
    _program->bind();
    _program->setUniformValue(_modelLocation, m);
    _program->release();
}

void GLTopography::setView(const QMatrix4x4 &v)
{
    _program->bind();
    _program->setUniformValue(_viewLocation, v);
    _program->release();
}

void GLTopography::setProjection(const QMatrix4x4 &p)
{
    _program->bind();
    _program->setUniformValue(_projectionLocation, p);
    _program->release();
}
