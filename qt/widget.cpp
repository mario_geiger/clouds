#include "widget.h"
#include <QKeyEvent>
#include <QMouseEvent>

Widget::Widget(QWidget *parent) :
    QGLWidget(parent)
{
    _m.setCenter(150.0, 0.0);
    _m.setSigma(30.0,40.0);
    _m.setHeight(50.0);

    setMouseTracking(true);
    setCursor(QCursor(Qt::BlankCursor));
    QCursor::setPos(geometry().center());

    camera = QVector3D(-100.0, 0.0, 100.0);
    up = QVector3D(0.0, 0.0, 1.0);
    right = QVector3D(0.0, -1.0, 0.0);
    forward = QVector3D(1.0, 0.0, 0.0);
    keys = 0;
    time.start();
    startTimer(0);
}

void Widget::initializeGL()
{
    _t.initializeGL();
    _t.initializeBuffer(_m, 10.0, 31, 31);

    _v.initializeGL();

    QImage img(3, 3, QImage::Format_ARGB32);
    img.setPixel(0, 0, 0xFFFF0000);
    img.setPixel(1, 0, 0xFF00FF00);
    img.setPixel(2, 0, 0xFF0000FF);
    img.setPixel(0, 1, 0xFFFF0000);
    img.setPixel(1, 1, 0xFF00FF00);
    img.setPixel(2, 1, 0xFF0000FF);
    img.setPixel(0, 2, 0xFFFF0000);
    img.setPixel(1, 2, 0xFF00FF00);
    img.setPixel(2, 2, 0xFFFFFFFF);
    _textureId2D = bindTexture(img);


    GLuint pixels[32][32][32];
    for (int x = 0; x < 32; ++x) {
        for (int y = 0; y < 32; ++y) {
            for (int z = 0; z < 32; ++z) {
                pixels[x][y][z] = 0x00FFFFFF;
                if ((x-10)*(x-10) + (y-10)*(y-10) + (z-10)*(z-10) < 20 && x < 12 && x > 8)
                    pixels[x][y][z] += 0x01000000 * (rand() % 80);
            }
        }
    }
    _v.loadTexture(pixels);
}

void Widget::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    QMatrix4x4 p;
    p.perspective(50.0, qreal(w)/qreal(h?h:1), 0.1, 1000.0);
    _t.setProjection(p);
    _v.setProjection(p);
}

void Widget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    qglClearColor(QColor(100, 100, 255));

    QMatrix4x4 v;
    v.lookAt(camera, camera + forward, up);
    _t.setView(v);
    _t.drawGL(_textureId2D);

    _v.setView(v);
    _v.drawGL();
}

void Widget::keyPressEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_W:
        keys |= 0x1;
        break;
    case Qt::Key_S:
        keys |= 0x2;
        break;

    case Qt::Key_A:
        keys |= 0x4;
        break;
    case Qt::Key_D:
        keys |= 0x8;
        break;

    case Qt::Key_R:
        keys |= 0x10;
        break;
    case Qt::Key_F:
        keys |= 0x20;
        break;

    case Qt::Key_Q:
        keys |= 0x40;
        break;
    case Qt::Key_E:
        keys |= 0x80;
        break;
    }
}

void Widget::keyReleaseEvent(QKeyEvent *e)
{
    switch (e->key()) {
    case Qt::Key_W:
        keys &= ~0x1;
        break;
    case Qt::Key_S:
        keys &= ~0x2;
        break;

    case Qt::Key_A:
        keys &= ~0x4;
        break;
    case Qt::Key_D:
        keys &= ~0x8;
        break;

    case Qt::Key_R:
        keys &= ~0x10;
        break;
    case Qt::Key_F:
        keys &= ~0x20;
        break;

    case Qt::Key_Q:
        keys &= ~0x40;
        break;
    case Qt::Key_E:
        keys &= ~0x80;
        break;
    }
}

void Widget::mouseMoveEvent(QMouseEvent *e)
{
    QPoint center = geometry().center();
    QVector2D move = QVector2D(e->globalPos() - center);
    QCursor::setPos(center);

    QMatrix4x4 rotation;
    rotation.rotate(-0.02 * move.length(), move.x() * up + move.y() * right);
    up = rotation * up;
    right = rotation * right;
    forward = rotation * forward;
}

void Widget::timerEvent(QTimerEvent *)
{
    double dt = time.restart();
    if (keys & 0x1)
        camera += 0.1 * dt * forward;
    if (keys & 0x2)
        camera -= 0.1 * dt * forward;

    if (keys & 0x4)
        camera -= 0.1 * dt * right;
    if (keys & 0x8)
        camera += 0.1 * dt * right;

    if (keys & 0x10)
        camera += 0.1 * dt * up;
    if (keys & 0x20)
        camera -= 0.1 * dt * up;

    if (keys & 0x40 || keys & 0x80) {
        QMatrix4x4 r;
        r.rotate((keys & 0x40 ? -0.05 : 0.05) * dt, forward);
        up = r * up;
        right = r * right;
    }

    updateGL();
}
