#include "mountain.h"
#include <cmath>

Mountain::Mountain()
{
}

double Mountain::altitude(double x, double y) const
{
    double a = _height * exp(-(x - _x0)*(x - _x0)/(2.0 * _xs*_xs)
                             -(y - _y0)*(y - _y0)/(2.0 * _ys*_ys));
    return a < 0.5 ? 0.0 : a;
}

void Mountain::setCenter(double x, double y)
{
    _x0 = x;
    _y0 = y;
}

void Mountain::setSigma(double x, double y)
{
    _xs = x;
    _ys = y;
}

void Mountain::setHeight(double height)
{
    _height = height;
}
