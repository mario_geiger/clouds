#ifndef MOUNTAIN_H
#define MOUNTAIN_H
#include "topography.h"

class Mountain : public Topography
{
public:
    Mountain();

    double altitude(double x, double y) const;

    void setCenter(double x, double y);
    void setSigma(double x, double y);
    void setHeight(double height);

private:
    double _x0, _y0;
    double _xs, _ys;
    double _height;
};

#endif // MOUNTAIN_H
