#ifndef WIDGET_H
#define WIDGET_H

#include <QGLWidget>
#include <QTime>
#include "gltopography.h"
#include "mountain.h"
#include "glvolumic.h"

class Widget : public QGLWidget
{
    Q_OBJECT
public:
    explicit Widget(QWidget *parent = 0);

private:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void keyPressEvent(QKeyEvent *e);
    void keyReleaseEvent(QKeyEvent *e);
    void mouseMoveEvent(QMouseEvent *e);

    void timerEvent(QTimerEvent *);

    Mountain _m;
    GLTopography _t;
    GLVolumic _v;
    GLuint _textureId2D;
    GLuint _textureId3D;

    QVector3D up, right, forward;
    QVector3D camera;
    QTime time;
    int keys;
};

#endif // WIDGET_H
