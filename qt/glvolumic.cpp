#include "glvolumic.h"

GLVolumic::GLVolumic(QObject *parent) :
    QObject(parent)
{
}

#define GLSL(version, shader)  "#version " #version "\n" #shader
#define PROGRAM_VERTEX_ATTRIBUTE 0
#define PROGRAM_TEXCOO_ATTRIBUTE 1
#define BUFFER_OFFSET(a) ((char*)NULL + (a))

static const char *vertsrc = GLSL(130,
in vec2 vertex;
uniform float z;

uniform mat4 matrixp; // projection
uniform mat4 matrixv; // view (camera)

out vec3 t;

void main(void)
{
    vec4 p = vec4(200.0 * vertex, z, 1.0);
    gl_Position = matrixp * p;
    t = vec3(matrixv * p);
    t /= 300.0;
    t.y += 0.5;
}
);

static const char *fragsrc = GLSL(130,
in vec3 t;

uniform sampler3D texname;

out vec4 color;

void main(void)
{
    color = texture(texname, t);
//    color = vec4(t.x, t.y, t.z, 0.2);
}
);

void GLVolumic::initializeGL(const QGLContext *context)
{
    _program = new QGLShaderProgram(context, this);
    _program->addShaderFromSourceCode(QGLShader::Vertex, vertsrc);
    _program->addShaderFromSourceCode(QGLShader::Fragment, fragsrc);
    _program->bindAttributeLocation("vertex", PROGRAM_VERTEX_ATTRIBUTE);
    _program->bindAttributeLocation("texCoord", PROGRAM_TEXCOO_ATTRIBUTE);
    _program->link();

    _zLocation = _program->uniformLocation("z");
    _viewLocation = _program->uniformLocation("matrixv");
    _projectionLocation = _program->uniformLocation("matrixp");

    setProjection(QMatrix4x4());
    setView(QMatrix4x4());

    _program->bind();
    _program->setUniformValue("texture", 0);
    _program->release();
}

void GLVolumic::loadTexture(GLvoid *data)
{
    glGenTextures(1, &textureId);
    glBindTexture(GL_TEXTURE_3D, textureId);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA, 32, 32, 32, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
}

void GLVolumic::drawGL()
{
    GLfloat vertices[] = {
        -1.0, -1.0,
        1.0, -1.0,
        1.0, 1.0,
        -1.0, 1.0,
    };

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_TEXTURE_3D);
    glEnable(GL_CULL_FACE);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, textureId);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    _program->bind();

    _program->setAttributeArray(PROGRAM_VERTEX_ATTRIBUTE, GL_FLOAT, vertices, 2);

    _program->enableAttributeArray(PROGRAM_VERTEX_ATTRIBUTE);
    GLfloat z = -300.0;
    for (int i = 0; i < 75; ++i) {
        _program->setUniformValue(_zLocation, z);
        glDrawArrays(GL_QUADS, 0, 4);
        z += 4.0;
    }
    _program->disableAttributeArray(PROGRAM_VERTEX_ATTRIBUTE);

    _program->release();
}

void GLVolumic::setView(const QMatrix4x4 &v)
{
    _program->bind();
    _program->setUniformValue(_viewLocation, v.inverted());
    _program->release();
}

void GLVolumic::setProjection(const QMatrix4x4 &p)
{
    _program->bind();
    _program->setUniformValue(_projectionLocation, p);
    _program->release();
}
