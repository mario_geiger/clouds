#ifndef TOPOGRAPHY_H
#define TOPOGRAPHY_H

class Topography
{
public:
    Topography();

    virtual double altitude(double x, double y) const = 0;
};

#endif // TOPOGRAPHY_H
