#ifndef GLTOPOGRAPHY_H
#define GLTOPOGRAPHY_H

#include <QtOpenGL/QGLShaderProgram>
#include <QtOpenGL/QGLBuffer>
#include <QObject>
#include "topography.h"

class GLTopography : public QObject
{
    Q_OBJECT

public:
    explicit GLTopography(QObject *parent = 0);

    void initializeGL(const QGLContext *context = 0);
    void initializeBuffer(const Topography &topo, double lambda, int nx, int ny);
    void drawGL(GLuint textureId);

    void setModel(const QMatrix4x4 &m);
    void setView(const QMatrix4x4 &v);
    void setProjection(const QMatrix4x4 &p);

private:
    int _nx;
    int _ny;

    QGLShaderProgram *_program;
    QGLBuffer _vbo;
    QGLBuffer _ibo;

    int _projectionLocation;
    int _viewLocation;
    int _modelLocation;
};

#endif // GLTOPOGRAPHY_H
