#-------------------------------------------------
#
# Project created by QtCreator 2013-02-24T22:08:04
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = test3D
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
        gltopography.cpp \
        mountain.cpp \
        topography.cpp \
        glvolumic.cpp

HEADERS  += widget.h \
         gltopography.h \
         mountain.h \
         topography.h \
         glvolumic.h

