#ifndef GLVOLUMIC_H
#define GLVOLUMIC_H

#include <QtOpenGL/QGLShaderProgram>
#include <QtOpenGL/QGLBuffer>
#include <QObject>

class GLVolumic : public QObject
{
    Q_OBJECT
public:
    explicit GLVolumic(QObject *parent = 0);
    
    void initializeGL(const QGLContext *context = 0);
    void loadTexture(GLvoid *data);
    void drawGL();

    void setView(const QMatrix4x4 &v);
    void setProjection(const QMatrix4x4 &p);

private:
    QGLShaderProgram *_program;

    GLuint textureId;
    int _projectionLocation;
    int _viewLocation;
    int _zLocation;
};

#endif // GLVOLUMIC_H
