#ifndef __SYSTEM__
#define __SYSTEM__

#include "potential.hh"
#include "sky.hh"
#include "mount.hh"
#include "ground.hh"
#include "forest.hh"
#include "textdrawable.hh"
#include <iostream>

		/* Classe Système.
		 * 
		 * N.B. : tous les paramètres de la simulation sont placés en attributs
		 * (privés). De plus, System ne dispose que d'un constructeur par défaut,
		 * qui initialise ces attributs à une valeur par défaut. Les conteneurs
		 * de System sont initialisés à vide et le pointeur sur la montagne à un
		 * nul pointeur. Des méthodes setter sont fournies pour paramétrer les
		 * attributs du système. Cette conception a pour but de permettre que
		 * le parser XML commence par initialiser le système par défaut, puis
		 * lui donne les paramètres choisit par l'utilisateur à mesure qu'il les
		 * lit. Quand ceci est fait, la méthode init() du système est appelée,
		 * et elle se charge d'initaliser tous ses éléments par leurs méthodes
		 * respectives.
		 */

class System : public TextDrawable { //Classe TextDrawable pour l'affichage en mode texte...
	public:
		System();//Attributs aux valeurs par défaut,
				 //puis resizés par 'setDimensionDuMonde' ; ciel construit sur champ de potentiel etc.
		~System(); //Doit détruire la montagne !
		// Supression des constructeurs de copie et d'affectation
		System(const System&) = delete;
		System& operator=(const System&) = delete;
		
		//Getters utiles pour la vue (nuages, terrain, etc.)
		const PotentialField& potential_field() const;
		const Sky& sky() const;
		const Mount& mount() const;
		const Ground& ground() const;
		const Forest& forest() const;
		int nx() const;
		int ny() const;
		int nz() const;
		
		//Setters
		void set_mount(const Mount& mountain); //copie ! utilise méthide copy de Mount
		void set_mount(Mount&& mountain); //move semantics ! utilise métode move de Mount
		void set_lambda(const double lambda);
		void set_size(const size_t nx, const size_t ny, const size_t nz);
		void set_plain_wind_speed(const double speed);
		void set_plain_temperature(const double temperature);
		void set_plain_pressure(const double pressure);
		void set_humidity(const double humidity);
		void set_epsilon(const double epsilon);
		void set_precision(const double precision);
		void set_max_iter(const unsigned int maxIter);
		void set_rock_limit(const double limit);
		
		// initialisation et evolution sur un pas de temps dt du système
		void init(bool verbal = false);
		void evolve(double dt);
		
		// méthode d'affichage en mode texte.
		void draw(std::ostream& out) const override;
		
	private:
		PotentialField _potentialField;
		
		Sky _sky;
		
		Mount* _mount;
		
		Ground _ground;
		
		Forest _forest;
		
		// Paramètres de la simulation
		double _lambda;
		size_t _nx;
		size_t _ny;
		size_t _nz;
		double _plainWindSpeed;
		double _plainTemperature;
		double _plainPressure;
		double _humidity;
		double _rockLimit;
		
		double _epsilon;
		double _precision;
		unsigned int _maxIter;
};

#endif
