#ifndef __GROUND__
#define __GROUND__

#include <vector>
#include <string>
#include "worldcube.hh"
#include "mount.hh"
#include "sky.hh"
#include "textdrawable.hh"
#include "random.hh"
#include "vec3.hh"

		/* Classe Ground, grille 2D de GroundCell, une classe
		 * polymorhpique pour les trois types de terrains.
		 * N.B. : le comportement (évolution de la végétation
		 * selon la pluie) ne suit pas exactement la donnée de
		 * l'exercice. C'était le cas au début, mais des modifications
		 * ont été apportées pour obtenir des résultats plus
		 * significatifs, et qui seraient ainsi mieux représentés
		 * par les plantes de la vue.
		 */

class Ground;

class GroundCell : public TextDrawable {		// classe abstraite, terrain générique
	public:
		// Constructeur
		GroundCell(Ground* soil);
		virtual ~GroundCell();
		
		// méthodes accesseux pour la végétation
		float get_vegetation() const { return _vegetation; }
		
		// méthodes pour l'évolution du terrain : polymorphisme
		virtual void init_vegetation() = 0;
		virtual void evolve_vegetation(double nb_rain, size_t i, size_t j, double dt) = 0;
		virtual std::string type_name() const = 0;
		virtual Vec3d soil() const = 0;
		virtual void draw(std::ostream& out) const override;
	protected:
		void set_vegetation(float vege);
		float _vegetation;		// dans [0,1]
		Ground* _soil;			// pour accéder aux cases voisines
};

	// les trois types différents de terrains (polymorphisme)
class RockyGround : public GroundCell {
	public:
		RockyGround(Ground* soil);
		
		void init_vegetation() override;
		void evolve_vegetation(double nb_rain, size_t i, size_t j, double dt) override;
		std::string type_name() const override { return "rocky"; }
		inline Vec3d soil() const override { return Vec3d(1.0, 0.0, 0.0); }
};
class AridGround : public GroundCell {
	public:
		AridGround(Ground* soil);
		
		void init_vegetation() override;
		void evolve_vegetation(double nb_rain, size_t i, size_t j, double dt) override;
		std::string type_name() const override { return "arid"; }
		inline Vec3d soil() const override { return Vec3d(0.0, 1.0, 0.0); }
};
class FertileGround : public GroundCell {
	public:
		FertileGround(Ground* soil);
		
		void init_vegetation() override;
		void evolve_vegetation(double nb_rain, size_t i, size_t j, double dt) override;
		std::string type_name() const override { return "fertile"; }
		inline Vec3d soil() const override { return Vec3d(0.0, 0.0, 1.0); }
};

class Ground : public WorldCube<GroundCell*>, public TextDrawable { //Used as 2D array only !
	public:
		// Constructeur
		Ground(size_t Nx, size_t Ny, double lambda);
		~Ground();
		
		// méthodes d'initialisation et d'évolution
		void init(const Mount* mountain, double limit_alt);
		void evolve(Sky& sky, double dt);
		
		// retourne le type de sol (3 composantes
		// pour les trois types), interpolé entre
		// les cases voisines de la position (i,j).
		Vec3d soil(double i, double j) const;
		
		// retourne le taux de végétation interpolé
		// dans une position (i,j).
		double vegetation(double i, double j) const;
		
		// méthode de dessin mode texte.
		void draw(std::ostream& out) const override;
		
	private:
		// accesseur pour la grille seulement 2D.
		const GroundCell& at(size_t i, size_t j) const { return *WorldCube::at(i,j,0); }
		Vec2 position(size_t i, size_t j) const { return Vec2(iToX(i),jToY(j)); }
};

#endif
