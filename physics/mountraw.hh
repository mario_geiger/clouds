#ifndef __MOUNTRAW_HH__
#define __MOUNTRAW_HH__

#include "mount.hh"
#include "array3d.hh"

class MountRaw : public Mount
{
public:
	MountRaw(int nx, int ny);

	void setAlt(int i, int j, double a);
	double alt(double x, double y) const override;
	MountRaw* copy() const override;
	MountRaw* move() override;
	void draw(std::ostream& out) const override;

private:
	Array3D<double> _d; // used as 2D array only :)
};

#endif
