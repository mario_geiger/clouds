#ifndef __MOUNT__
#define __MOUNT__

#include "textdrawable.hh"

class Mount : public TextDrawable
{
public :
	inline virtual ~Mount() {}
	virtual double alt(double x, double y) const = 0;
	virtual Mount* copy() const = 0;
	virtual Mount* move() = 0;
};

#endif
