#include "system.hh"
#include <cmath>
#include <iostream>
using namespace std;

// Constructeur par défaut. Donne des valeurs par défaut aux attributs du système et crée des conteneurs vides.
System::System() : 
	_potentialField(0.0, 0.0, 0.0, 0.0),
	_sky(0.0, 0.0, 0.0, 0.0), 
	_mount(nullptr), 
	_ground(0,0,0.0), 
	_forest(&_ground),
	_lambda(20./29.), 
	_nx(30), _ny(30), _nz(30),
	_plainWindSpeed(20.0),
	_plainTemperature(284.5),
	_plainPressure(101325.0),
	_humidity(8e-3),
	_rockLimit(12.0),
	_epsilon(0.1),
	_precision(pow(20./29, 4)*1e-4),
	_maxIter(5000)
{
}

System::~System()
{
	delete _mount;
}

// getters
const PotentialField& System::potential_field() const
{
	return _potentialField;
}

const Sky& System::sky() const
{
	return _sky;
}

const Mount& System::mount() const
{
	return *_mount;
}

const Ground& System::ground() const
{
	return _ground;
}

const Forest& System::forest() const
{
	return _forest;
}

int System::nx() const
{
	return _nx;
}

int System::ny() const
{
	return _ny;
}

int System::nz() const
{
	return _nz;
}

void System::set_mount(const Mount& mountain)
{
	_mount = mountain.copy();
}
void System::set_mount(Mount&& mountain)
{
	_mount = mountain.move();
}

// setters pour les attributs du système
void System::set_lambda(const double lambda)								{ _lambda = lambda; }
void System::set_size(const size_t nx, const size_t ny, const size_t nz)	{ _nx = nx; _ny = ny; _nz = nz; }
void System::set_plain_wind_speed(const double speed)						{ _plainWindSpeed = speed; }
void System::set_plain_temperature(const double temperature)				{ _plainTemperature = temperature; }
void System::set_plain_pressure(const double pressure)						{ _plainPressure = pressure; }
void System::set_humidity(const double humidity)							{ _humidity = humidity; }
void System::set_epsilon(const double epsilon)								{ _epsilon = epsilon; }
void System::set_precision(const double precision)							{ _precision = precision; }
void System::set_max_iter(const unsigned int maxIter)						{ _maxIter = maxIter; }
void System::set_rock_limit(const double limit)								{ _rockLimit = limit; }

// initialisation du système : redimensionne les conteneurs et fait appel à leurs processus d'initalisation respectifs
void System::init(bool verbal) {
	_potentialField.setSize(_nx, _ny, _nz);
	_potentialField.setLambda(_lambda);
	_potentialField.init(_plainWindSpeed, _mount);
	_potentialField.solve(_epsilon, _precision, _maxIter, _mount, verbal);
	_sky.setSize(_nx, _ny, _nz);
	_sky.setLambda(_lambda);
	_sky.setWindSpeeds(_potentialField);
	_sky.initialize(_plainTemperature, _plainPressure, _plainWindSpeed, _humidity);
	_ground.setSize(_nx, _ny, 1);
	_ground.setLambda(_lambda);
	_ground.init(_mount, _rockLimit);
	_forest.init();
}

// evolution du système sur un pas de temps dt : évolution de ses composants
void System::evolve(double dt) {
	_sky.evolve(dt);
	_ground.evolve(_sky, dt);
	_forest.grow(dt);
}

// méthode pour l'affichage en mode texte
void System::draw(ostream& out) const {
	out << endl << "\tParameters of the simulation :" << endl << endl
	<< "size of the world (x,y,z) : " << (_nx-1)*_lambda << " " << (_ny-1)*_lambda << " " << (_nz-1)*_lambda << " " << endl
	<< "world discretized in " << _nx << "x" << _ny << "x" << _nz << " cubes with a side of " << _lambda << endl
	<< "plain wind speed : " << _plainWindSpeed << endl
	<< "plain temperature : " << _plainTemperature << endl
	<< "plain pressure : " << _plainPressure << endl
	<< "humidity : " << _humidity << endl
	<< "max altitude for non-rocky ground : " << _rockLimit << endl << endl << endl
	
	<< "\tThe system is composed of :" << endl << endl
		
	<< *_mount << endl
		
	<< _potentialField << endl
		
	<< _sky << endl
	
	<< _ground << endl
	
	<< _forest << endl;
}
