#include "ground.hh"
#include <cmath>
#include <iostream>
using namespace std;

GroundCell::GroundCell(Ground* soil) : _vegetation(0.), _soil(soil) {}
GroundCell::~GroundCell() 
{
}

// modification du taux de végétation.
void GroundCell::set_vegetation(float vege) {
	// vérification que le paramètre est dans le bon intervalle [0,1]
	if(vege <= 0.) _vegetation = 0;
	else if(vege >= 1.) _vegetation = 1;
	else _vegetation = vege;
}

RockyGround::RockyGround(Ground* soil) : GroundCell(soil) {}
AridGround::AridGround(Ground* soil) : GroundCell(soil) {}
FertileGround::FertileGround(Ground* soil) : GroundCell(soil) {}

// initialisation du niveau de végétation
void RockyGround::init_vegetation() {
	set_vegetation(rdm::normal(0.1,0.01));
}
void AridGround::init_vegetation() {
	set_vegetation(rdm::normal(0.2,0.03));
}
void FertileGround::init_vegetation() {
	set_vegetation(rdm::normal(0.3,0.05));
}

// évolution du niveau de végétation ; augmentation proportionnelle à la quantité de pluie
void RockyGround::evolve_vegetation(double nb_rain, size_t , size_t , double dt) {
	if(nb_rain > 0) {
		_vegetation += nb_rain * 0.2;
		if (_vegetation > 1.0) _vegetation = 1.0;
	} else {
		_vegetation *= std::pow(0.85, dt);
	}
}
void AridGround::evolve_vegetation(double nb_rain, size_t i, size_t j, double dt) {
	if(nb_rain > 0) {
		double sides(0.);
		double sum(0.);
		
		// prise en compte que l'on peut se trouver en bordure du monde.
		if(i > 0) { sides++; sum += _soil->vegetation(i-1,j); }
		if(i+1 < _soil->sizei()) { sides++; sum += _soil->vegetation(i+1,j); }
		if(j > 0) { sides++; sum += _soil->vegetation(i,j-1); }
		if(j+1 < _soil->sizej()) { sides++; sum += _soil->vegetation(i,j+1); }
		
		_vegetation += nb_rain * 0.5 * (sum + _vegetation) / (sides + 1.0);
		if (_vegetation > 1.0) _vegetation = 1.0;
	} else {
		_vegetation *= std::pow(0.9, dt);
	}
}
void FertileGround::evolve_vegetation(double nb_rain, size_t , size_t , double dt) {
	if(nb_rain > 0) {
		_vegetation *= std::pow(1.05, nb_rain);
		if (_vegetation > 1.0) _vegetation = 1.0;
	} else {
		_vegetation *= std::pow(0.95, dt);
	}
}

Ground::Ground(size_t Nx, size_t Ny, double lambda) : WorldCube(Nx, Ny, 1, lambda) {}

Ground::~Ground()
{
	for (size_t i = 0; i < _d.size(); ++i) {
		delete _d[i];
	}
}

// initialisation du sol (détermination des types de terrains et niveau de végétation initial)
void Ground::init(const Mount* mountain, double limit_alt) {
	double u(0.);
	double curr_alt(0.);
	
	for(size_t i(0); i<sizei(); i++) {
		for(size_t j(0); j<sizej(); j++) {
			curr_alt = mountain->alt(i,j);
			if(curr_alt >= limit_alt)
				_d(i,j,0) = new RockyGround(this);
			else {
				u = rdm::canonical();
				if(u < 1-sqrt(2*curr_alt/limit_alt - pow(curr_alt/limit_alt,2)))
					_d(i,j,0) = new FertileGround(this);
				else if(u < 2-sqrt(2*curr_alt/limit_alt - pow(curr_alt/limit_alt,2))-sqrt(1-pow(curr_alt/limit_alt,2)))
					_d(i,j,0) = new RockyGround(this);
				else
					_d(i,j,0) = new AridGround(this);
			}
			_d(i,j,0)->init_vegetation();
		}
	}
}

// évolution du niveau de végétation dans chaque cellule de terrain (selon la pluie);
void Ground::evolve(Sky& sky, double dt) {
	for(size_t i(0); i<sizei(); i++) {
		for(size_t j(0); j<sizej(); j++) {
			_d(i,j,0)->evolve_vegetation(sky.rain(i, j, dt), i, j, dt);
		}
	}
}

// retourne un vecteur interpolé (rocky, arid, fertile)
Vec3d Ground::soil(double i, double j) const
{
	// fi <= i <= ci
	int fi = std::floor(i);
	int ci = std::ceil(i);
	// fj <= j <= cj
	int fj = std::floor(j);
	int cj = std::ceil(j);
	
	// si on sort du monde on retourne fertile (dans la plaine)
	if (fi < -1 || fj < -1 || ci > (int)sizei() || cj > (int)sizej()) {
		return Vec3d(0.0, 0.0, 1.0);
	}
	
	// si on est sur le bord on interpole pas
	if (fi < 0)
		fi = ci = 0;
	if (fj < 0)
		fj = cj = 0;
	
	if (ci >= (int)sizei())
		ci = fi = sizei() - 1;
	if (cj >= (int)sizej())
		cj = fj = sizej() - 1;
	
	// pas besoin d'interpolation
	if (fi == ci && fj == cj) {
		return at(ci, cj).soil();
	}
	
	i -= fi; // in [0,1[
	j -= fj; // in [0,1[

	Vec3d a = at(fi, fj).soil();
	Vec3d b = at(ci, fj).soil();
	Vec3d c = at(ci, cj).soil();
	Vec3d d = at(fi, cj).soil();

	return (1.0-i)*(1.0-j) * a
	     +    i   *(1.0-j) * b
	     +    i   *   j    * c
	     + (1.0-i)*   j    * d;
}

double Ground::vegetation(double i, double j) const
{
	// fi <= i <= ci
	int fi = std::floor(i);
	int ci = std::ceil(i);
	// fj <= j <= cj
	int fj = std::floor(j);
	int cj = std::ceil(j);
	
	// si on sort du monde on retourne 0.0
	if (fi < -1 || fj < -1 || ci > (int)sizei() || cj > (int)sizej()) {
		return 0.0;
	}
	
	// si on est sur le bord on interpole pas
	if (fi < 0)
		fi = ci = 0;
	if (fj < 0)
		fj = cj = 0;
	
	if (ci >= (int)sizei())
		ci = fi = sizei() - 1;
	if (cj >= (int)sizej())
		cj = fj = sizej() - 1;
	
	// pas besoin d'interpolation
	if (fi == ci && fj == cj) {
		return at(ci, cj).get_vegetation();
	}
	
	i -= fi; // in [0,1[
	j -= fj; // in [0,1[

	double a = at(fi, fj).get_vegetation();
	double b = at(ci, fj).get_vegetation();
	double c = at(ci, cj).get_vegetation();
	double d = at(fi, cj).get_vegetation();

	return (1.0-i)*(1.0-j) * a
	     +    i   *(1.0-j) * b
	     +    i   *   j    * c
	     + (1.0-i)*   j    * d;
}

// méthodes d'affichage en mode texte
void GroundCell::draw(ostream& out) const {
	out << this->type_name() << "   " << _vegetation;
}
void Ground::draw(ostream& out) const {
	out << "A ground; for each point, position (x,y), soil type and vegetation level :" << endl;
	for(size_t i(0); i<sizei(); i++) {
		for(size_t j(0); j<sizej(); j++) {
			out << position(i,j) << "   " << at(i,j) << endl;
		}
		out << endl;
	}
	out << endl;
}
