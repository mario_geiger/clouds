#ifndef __FOREST_HH__
#define __FOREST_HH__

#include "vec3.hh"
#include "vec2.hh"
#include "ground.hh"
#include "textdrawable.hh"
#include "random.hh"
#include "textdrawable.hh"
#include <vector>
#include <list>
#include <random>
#include <string>
#include <map>

		/* Une classe supplémentaire pour avoir des arbres qui se développent suivant la végétation */

class Forest;

class Plant	: TextDrawable	// classe abstraite
{
public:
	Plant(Forest* forest, double x, double y);
	virtual ~Plant();
	bool grow(double dt, std::back_insert_iterator<std::list<Plant*>> back_it);
	
	virtual double probability_of_surviving() = 0; // 0 = mort, 1 = vivant (toutes les secondes)
	virtual double rate_of_growth() = 0; // par seconde
	virtual int number_of_seeds() = 0; // nombres de graines lachées
	virtual Vec2 random_seed_throw() = 0; // lancé aléatoire, la plante et toujours en (0,0)
	virtual double initial_number() const = 0; // nombre initial de plantes par cases de terrain
	virtual double width() const = 0;
	virtual double height() const = 0;
	virtual double mass() const = 0;
	virtual Plant* kid(double x, double y) const = 0; // crée un enfant à son image
	virtual int id() const = 0; // pour distinguer les espèces (textures)
	virtual std::string species() const = 0;

	inline double age() const { return m_age; }
	inline double size() const { return std::min(m_age, 1.0); }
	inline double x() const { return m_x; }
	inline double y() const { return m_y; }
	inline Vec3d soil() const { return m_soil; }
	
	virtual void draw(std::ostream& out) const override;
protected:
	double m_x;
	double m_y;
	double m_age;
	Forest *m_forest;
	Vec3d m_soil;
	double m_veg;
};

class Beech : public Plant
{
public:
	Beech(Forest* forest, double x, double y);
	double probability_of_surviving() override;
	double rate_of_growth() override;
	int number_of_seeds() override;
	Vec2 random_seed_throw() override;
	double initial_number() const override;
	double width() const override;
	double height() const override;
	double mass() const override;
	Beech* kid(double x, double y) const override;
	inline int id() const override { return 0; }
	std::string species() const override { return "beech"; }
};

class Palm : public Plant
{
public:
	Palm(Forest* forest, double x, double y);
	double probability_of_surviving() override;
	double rate_of_growth() override;
	int number_of_seeds() override;
	Vec2 random_seed_throw() override;
	double initial_number() const override;
	double width() const override;
	double height() const override;
	double mass() const override;
	Palm* kid(double x, double y) const override;
	inline int id() const override { return 1; }
	std::string species() const override { return "palm tree"; }
};

class Bush : public Plant
{
public:
	Bush(Forest* forest, double x, double y);
	double probability_of_surviving() override;
	double rate_of_growth() override;
	int number_of_seeds() override;
	Vec2 random_seed_throw() override;
	double initial_number() const override;
	double width() const override;
	double height() const override;
	double mass() const override;
	Bush* kid(double x, double y) const override;
	inline int id() const override { return 2; }
	std::string species() const override { return "bush"; }
};

class Forest : public TextDrawable
{
public:
	Forest(const Ground* ground);
	~Forest();
	void init();
	void populate(const Plant& seed);
	void grow(double dt);
	bool check_density(double x, double y, double density);
	
	inline const Ground* ground() const { return m_ground; }
	inline const std::list<Plant*>& plants() const { return m_plants; }
	
	void draw(std::ostream& out) const;
private:
	std::list<Plant*> m_plants;
	const Ground* m_ground;
	std::map<unsigned long, double> m_density;
};

#endif
