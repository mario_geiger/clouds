#include "sky.hh"
#include "random.hh"
#include <cmath>
#include <iostream>
using namespace std;

AirCube::AirCube() :
	_windSpeedSquared(0.0),
	_enthalpy(0.0),
	_temperature(0.0),
	_pressure(0.0),
	_partialPressure(0.0),
	_vaporPressure(0.0),
	_liquidState(0.0),
	_traveltime(0.0)
{
}

void AirCube::initialize(double z
			            , double plainTemperature, double plainPressure
			            , double bernouilliConstant)
{
	_enthalpy = bernouilliConstant - Physique::g * z - 0.5 * _windSpeedSquared;
	_temperature = 2.0 / 7.0 * Physique::airMolecularWeight * _enthalpy / Physique::R;
	_pressure = plainPressure * pow(_temperature / plainTemperature, 7.0 / 2.0);
	update();
}

void AirCube::update()
{
	_partialPressure = _humidity * _pressure / (Physique::waterMolecularWeight / Physique::airMolecularWeight + _humidity);
	_vaporPressure = Physique::pressureRef * exp(13.96 - 5208.0 / _temperature);
	_liquidState = _partialPressure / _vaporPressure;
}

double AirCube::rain(double dt)
{
	// facteur 10 car on aimerait, pour un _liquidState à 1.2 pendant une seconde une quantité de pluie de l'ordre de 1.
	double quantity = (_liquidState - 1.0) * dt / 0.1;
	if (quantity > 0) {
		_humidity *= pow(0.95, quantity);
		update();
		return quantity;
	} else {
		return 0.0;
	}
}

void AirCube::draw(std::ostream& out) const
{
	out << _windSpeedSquared << ' '
		<< _humidity << ' '
	    << _enthalpy << ' ' 
	    << _temperature << ' ' 
	    << _pressure << ' ' 
	    << _partialPressure << ' ' 
	    << _vaporPressure << ' ' 
	    << (_liquidState > 1.0 ? '1':'0');
}

Sky::Sky() : 
	_bernouilliConstant(0.0)
{
}

Sky::Sky(size_t nx, size_t ny, size_t nz, double lambda, double windSpeed) :
	WorldCube<AirCube>(nx, ny, nz, lambda),
	_bernouilliConstant(0.0)
{
	for (size_t i = 0; i < _d.size(); ++i) 
		_d[i].setWindSpeed(Vec3d(1.0, 0.0, 0.0) * windSpeed);
}

Sky::Sky(size_t nx, size_t ny, size_t nz, double lambda) :
	WorldCube<AirCube>(nx, ny, nz, lambda),
	_bernouilliConstant(0.0)
{
}

Sky::Sky(const PotentialField& pot) :
	WorldCube<AirCube>(pot.sizei(), pot.sizej(), pot.sizek(), pot.lambda())
{
	setWindSpeeds(pot);
}

void Sky::initialize(double plainTemperature, double plainPressure, double plainWindSpeed, double humidity)
{
	double plainEnthalpy = 7.0 / 2.0 * Physique::R * plainTemperature / Physique::airMolecularWeight;
	_bernouilliConstant = 0.5 * plainWindSpeed * plainWindSpeed + plainEnthalpy;
	
	for(size_t i = 0; i < sizei(); ++i) {
	for(size_t j = 0; j < sizej(); ++j) {
	for(size_t k = 0; k < sizek(); ++k) {
		_d(i,j,k)._humidity = humidity;
		_d(i,j,k).initialize(kToZ(k), plainTemperature, plainPressure, _bernouilliConstant);
	}}}
}

void Sky::setWindSpeeds(const PotentialField& pot)
{
	for (size_t i = 0; i < sizei(); ++i) 
	for (size_t j = 0; j < sizej(); ++j)
	for (size_t k = 0; k < sizek(); ++k)
		_d(i,j,k).setWindSpeed(pot.wind_speed(i,j,k));
}

double Sky::liquidState(size_t i, size_t j, size_t k) const
{
	return at(i,j,k).liquidState();
}

double Sky::rain(size_t i, size_t j, double dt)
{
	double sum = 0.0;
	for (size_t k = 0; k < sizek(); ++k)
		sum += _d(i, j, k).rain(dt);
	return sum;
}

void Sky::evolve(double dt)
{
	dt /= 50.0; // sinon les nuages vont trop vite
	for (size_t i = 0; i < sizei(); ++i) {
	for (size_t j = 0; j < sizej(); ++j) {
	for (size_t k = 0; k < sizek(); ++k) {
		AirCube& c = _d(i,j,k); // current

		c._traveltime += dt;
		if (c.windSpeed().lengthSquared() * c._traveltime * c._traveltime < 1.0)
			continue;
		c._traveltime = 0.0;

		Vec3d v = c.windSpeed().normalized();
		size_t ii = std::round(double(i) - v.x());
		size_t jj = std::round(double(j) - v.y());
		size_t kk = std::round(double(k) - v.z());

		if (ii == i && jj == j && kk == k) {
			// avance pas
		} else if (/*ii >= 0 &&*/ ii < sizei() && /*jj >= 0 &&*/ jj < sizej() && /*kk >= 0 &&*/ kk < sizek()) {
			AirCube& p = _d(ii,jj,kk); // previous

			// déplacement de l'humidité avec le vent, on ne touche pas aux bords
			if (i != 0 && j != 0 && k != 0 && i != sizei()-1 && j != sizej()-1 && k != sizek()-1)
				c._humidity += p._humidity * 0.001;
			if (ii != 0 && jj != 0 && kk != 0 && ii != sizei()-1 && jj != sizej()-1 && kk != sizek()-1)
				p._humidity *= 0.999;
	
			
			// les nuages se déplacent plus vite que l'humidité (pourquoi pas^^)
			// si C est nuageuse mais P ne l’est pas
			if (c.liquidState() > 1.0 && p.liquidState() <= 1.0)
				// diminuer l’humidité de C de 1.7%
				c._humidity *= 0.99;
			// si C n’est pas nuageuse mais P l’est
			if (c.liquidState() <= 1.0 && p.liquidState() > 1.0)
				// augmenter l’humidité de C de 5%
				c._humidity *= 1.02;
		
		}
	}}}
	for (size_t i = 0; i < sizei(); ++i) { 
	for (size_t j = 0; j < sizej(); ++j) {
	for (size_t k = 0; k < sizek(); ++k) {
		_d(i,j,k).update();
	}}}
}

void Sky::draw(std::ostream& out) const
{
	out << "A sky; for each point, position (x,y,z), norm of wind speed, humidity, enthalpy, temperature, pressure, partial pressure, vapor pressure, liquid state :" << endl;
	for (size_t i = 0; i < sizei(); ++i) { 
	for (size_t j = 0; j < sizej(); ++j) {
	for (size_t k = 0; k < sizek(); ++k) {
		out << position(i,j,k) << "   ";
		out << _d(i, j, k) << endl;
	} out << endl; }}
}
void Sky::data(std::ostream& out) const
{
	for (size_t i = 0; i < sizei(); ++i) { 
	for (size_t j = 0; j < sizej(); ++j) {
	for (size_t k = 0; k < sizek(); ++k) {
		out << iToX(i) << " " << jToY(j) << " " << kToZ(k) << ' ';
		out << _d(i, j, k) << endl;
	} out << endl; }}
}
