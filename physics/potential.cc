#include "potential.hh"
#include "progressbar.hh"
#include <iostream>
#include <iomanip>
using namespace std;

// méthodes setter et getter
Vec2 Potential::potential() const {
	return _potential;
}
Vec2 Potential::laplacian() const {
	return _laplacian;
}
void Potential::potential(const Vec2& other) {
	_potential = other;
}
void Potential::laplacian(const Vec2& other) {
	_laplacian = other;
}

// constructeur
PotentialField::PotentialField(size_t nx, size_t ny, size_t nz, double lambda) : WorldCube<Potential>(nx, ny, nz, lambda), _v(0.0) {};

// méthode d'initialisation
void PotentialField::init(const double v, const Mount* mountain) {
	_v = v;
	for(size_t i(0); i<_d.sizex(); i++) {
		for(size_t j(0); j<_d.sizey(); j++) {
			for(size_t k(0); k<_d.sizez(); k++) {
				if(k < mountain->alt(i, j)) {
					_d(i,j,k).potential(Vec2(0.0, 0.0));
				} else {
					_d(i,j,k).potential(Vec2(-kToZ(k)*_v/2.0, jToY(j)*_v/2.0));
				}
				_d(i,j,k).laplacian(Vec2(0.0, 0.0));
			}
		}
	}
}
// calcul des laplaciens
void PotentialField::calculate_laplacian(const Mount* mountain) {
	for(size_t i(1); i<_d.sizex()-1; i++) {
		for(size_t j(1); j<_d.sizey()-1; j++) {
			for(size_t k(1); k<_d.sizez()-1; k++) {
				if(k < mountain->alt(i, j)) {
					_d(i,j,k).laplacian( Vec2(0.0, 0.0) );
				} else {
					_d(i,j,k).laplacian(finite_diff(i,j,k));
				}
			}
		}
	}
}
// erreur totale = somme des normes des laplaciens (doit tendre vers zéro)
double PotentialField::error() const {
	double sum(0.0);
	for(size_t i(0); i<_d.sizex(); i++) {
		for(size_t j(0); j<_d.sizey(); j++) {
			for(size_t k(0); k<_d.sizez(); k++) {
				sum += _d(i,j,k).laplacian().square_norm();
			}
		}
	}
	return sum;
}
// une itération du processus de convergence
void PotentialField::iteration(const double epsilon, const Mount* mountain) {
	calculate_laplacian(mountain);
	for(size_t i(0); i<_d.sizex(); i++) {
		for(size_t j(0); j<_d.sizey(); j++) {
			for(size_t k(0); k<_d.sizez(); k++) {
				_d(i,j,k).potential(_d(i,j,k).potential() + epsilon*_d(i,j,k).laplacian());
			}
		}
	}
}
// approximation du laplacien à la position (i,j,k) par une méthode de différences finies centrée
Vec2 PotentialField::finite_diff(const size_t i, const size_t j, const size_t k) const {
	return _d(i-1,j,k).potential() + _d(i,j-1,k).potential() + _d(i,j,k-1).potential() -6*_d(i,j,k).potential() + _d(i+1,j,k).potential() + _d(i,j+1,k).potential() + _d(i,j,k+1).potential();
}

// résolution des équations de Laplace pour les vents
void PotentialField::solve(const double epsilon, const double precision, const int max_iter, const Mount* mountain, const bool verbal) {
	int counter(1);
	calculate_laplacian(mountain);
	double err = error();

	ProgressBar pb;
	pb.setMinimum(-std::log(err / 100.0));
	pb.setMaximum(-std::log(precision));
	while(err > precision && counter <= max_iter) {
		if(verbal) {
			pb.display(-std::log(err));			// affichage de la barre de chargement
			//cout << counter << " " << error() << endl;
			/*cout << counter << " : Potentiel" << endl << endl;
			aff_potential();
			cout << endl << counter << " : Laplacien" << endl << endl;
			aff_laplacian();
			cout << endl << endl;*/
		}
		iteration(epsilon, mountain);
		err = error();
		counter++;
	}
	if(verbal)
		pb.clearline();
}

// retourne la vitesse du vent à la position (i,j,k)
Vec3d PotentialField::wind_speed(const size_t i, const size_t j, const size_t k) const {
	Vec3d speed(0.0, 0.0, 0.0);
	if(i==0 || j==0 || k==0 || i==_d.sizex()-1 || j==_d.sizey()-1 || k==_d.sizez()-1) {
		Vec3d pos(iToX(i), jToY(j), kToZ(k));
		speed = pos*(1/pos.length())*_v;	
	} else {
		speed.x(  (_d(i,j+1,k).potential().y()
				  - _d(i,j-1,k).potential().y()
				  - _d(i,j,k+1).potential().x()
				  + _d(i,j,k-1).potential().x())/(2.0*_lambda));
		speed.y(  (-_d(i+1,j,k).potential().y()
				  + _d(i-1,j,k).potential().y())/(2.0*_lambda));
		speed.z(  (_d(i+1,j,k).potential().x()
				  - _d(i-1,j,k).potential().x())/(2.0*_lambda));
	}
	return speed;
}

// méthodes d'affichage
void PotentialField::aff_potential() const {
	for(size_t i(0); i<_d.sizex(); i++) {
		for(size_t j(0); j<_d.sizey(); j++) {
			for(size_t k(0); k<_d.sizez(); k++) {
				cout << i << " " << j << " " << k << " " << _d(i,j,k).potential().x() << " " << _d(i,j,k).potential().y() << endl;
			}
			cout << endl;
		}
	}
}
void PotentialField::aff_laplacian() const {
	for(size_t i(0); i<_d.sizex(); i++) {
		for(size_t j(0); j<_d.sizey(); j++) {
			for(size_t k(0); k<_d.sizez(); k++) {
				cout << i << " " << j << " " << k << " " << _d(i,j,k).laplacian().x() << " " << _d(i,j,k).laplacian().y() << endl;
			}
			cout << endl;
		}
	}
}

void PotentialField::aff_all(ostream& out) const {
	Vec3d speed(0.0, 0.0, 0.0);
	for(size_t i(0); i<_d.sizex(); i++) {
		for(size_t j(0); j<_d.sizey(); j++) {
			for(size_t k(0); k<_d.sizez(); k++) {
				speed = wind_speed(i,j,k);
				out << iToX(i) << " " << jToY(j) << " " << kToZ(k) << " "
					<< _d(i,j,k).potential().x() << " " << _d(i,j,k).potential().y() << " "
					<< speed.x() << " " << speed.y() << " " << speed.z() << " "
					<< speed.lengthSquared() << endl;
			}
			out << endl;
		}
	}
}

// méthodes d'affichage pour la simulation mode texte
void Potential::draw(ostream& out) const {
	out << _potential;
}
void PotentialField::draw(ostream& out) const {
	out << "A potential field; for each point, position(x,y,z), potential (x,y), wind speed (x,y,z) and wind speed norm :" << endl;
	
	Vec3d speed;
	for(size_t i(0); i<sizei(); i++) {
		for(size_t j(0); j<sizej(); j++) {
			for(size_t k(0); k<sizek(); k++) {
				speed = wind_speed(i,j,k);
				out   << position(i,j,k) << "   "
					 << at(i,j,k) << "   "
					 << speed << "   "
					 << speed.length() << endl;
			}
			out << endl;
		}
	}
}
