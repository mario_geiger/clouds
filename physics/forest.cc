#include "forest.hh"
#include <iterator>
#include <iostream>
#include <cmath>

constexpr double linear_fit(double input, double input_min, double input_max, double output_min, double output_max)
{
	return (input - input_min) / (input_max - input_min) * (output_max - output_min) + output_min;
}

unsigned long hash_vec2d(double x, double y)
{
	unsigned long ulx = std::round(x);
	unsigned long uly = std::round(y);
	// 64 bit (32 pour uly, 32 pour ulx)
	return (uly << 32) | (ulx & 0x00000000FFFFFFFF);
}

///============ Plant ============
Plant::Plant(Forest* forest, double x, double y) :
	m_x(x),
	m_y(y),
	m_age(0.0),
	m_forest(forest)
{
	m_soil = m_forest->ground()->soil(m_x, m_y);
}

Plant::~Plant()
{
}

bool Plant::grow(double dt, std::back_insert_iterator<std::list<Plant*>> back_it) // dt donné en secondes
{
	m_veg = m_forest->ground()->vegetation(m_x, m_y);
	
	// calculer les chances de survivre par seconde puis adapter avec dt
	double alive = std::pow(probability_of_surviving(), dt);
	
	// lancer les dès
	if (rdm::canonical() > alive)
		return false;
	
	// faire grandir
	double age1 = std::floor(m_age);
	m_age += rate_of_growth() * dt;
	double age2 = std::floor(m_age);
	
	// lancer des graines (les plantes se reproduisent à chaque cycle de croissance)
	// caractériser par le passage à l'unité suivante de la variable age
	if (age1 != age2) {
		int n = number_of_seeds();
		for (int i = 0; i < n; ++i) {
			Vec2 p = random_seed_throw() + Vec2(m_x, m_y);
			// copie dynamique
			if (m_forest->check_density(p.x(), p.y(), mass()))
				back_it = kid(p.x(), p.y());
		}
	}
	return true;
}

///============ Beech ============
Beech::Beech(Forest* forest, double x, double y) :
	Plant(forest, x, y)
{
}

double Beech::probability_of_surviving()
{
	// fit l'age de l'échelle [0,1] -> [0.5,1]
	double f1 = linear_fit(std::min(m_age, 1.0), 0.0, 1.0, 0.9, 1.0);
	f1 = f1 * f1;
	double f2 = linear_fit(m_veg, 0.0, 1.0, 0.97, 1.0);
	// dans l'ordre : rocky , arid, fertile
	return f1 * f2 * dot(m_soil, Vec3d(0.9, 0.9, 0.99999));
}

double Beech::rate_of_growth()
{
	return linear_fit(m_veg, 0.0, 1.0, 1.0/20.0, 1.0/4.0) * std::exp(-m_age / 5.0);
}

int Beech::number_of_seeds()
{
	// dans l'ordre : rocky , arid, fertile
	return 3; //rdm::uniformi(0, std::round(dot(m_soil, Vec3d(1, 2, 3))));
}

Vec2 Beech::random_seed_throw()
{
	return Vec2::polar(rdm::uniformd(1.0, 7.0), rdm::uniformd(0, 2.0 * M_PI));
}

double Beech::initial_number() const { return 0.2; }
double Beech::width() const          { return 1.0; }
double Beech::height() const         { return 1.5; }
double Beech::mass() const           { return 1.0/2.0; }

Beech* Beech::kid(double x, double y) const 
{
	return new Beech(m_forest, x, y);
}

///============ Palm ============
Palm::Palm(Forest* forest, double x, double y) :
	Plant(forest, x, y)
{
}

double Palm::probability_of_surviving()
{
	// fit l'age de l'échelle [0,1] -> [0.5,1]
	double f1 = linear_fit(std::min(m_age, 1.0), 0.0, 1.0, 0.8, 1.0);
	f1 = f1 * f1;
	double f2 = linear_fit(m_veg, 0.0, 1.0, 0.97, 1.0);
	// dans l'ordre : rocky , arid, fertile
	return dot(m_soil, Vec3d(0.9, 0.9999, 0.9)) * f1 * f2;
}

double Palm::rate_of_growth()
{
	// 10 secondes
	return linear_fit(m_veg, 0.0, 1.0, 1.0/10.0, 1.0/3.0) * std::exp(-m_age / 5.0);
}

int Palm::number_of_seeds()
{
	return rdm::uniformi(1, 4);
}

Vec2 Palm::random_seed_throw()
{
	return Vec2::polar(rdm::uniformd(0.0, 5.0), rdm::uniformd(0, 2.0 * M_PI));
}

double Palm::initial_number() const { return 0.2; }
double Palm::width() const { return 0.5; }
double Palm::height() const { return 0.5; }
double Palm::mass() const { return 1.0/5.0; }

Palm* Palm::kid(double x, double y) const 
{
	return new Palm(m_forest, x, y);
}

///============ Bush ============
Bush::Bush(Forest* forest, double x, double y) :
	Plant(forest, x, y)
{
}

double Bush::probability_of_surviving()
{
	// fit l'age de l'échelle [0,1] -> [0.5,1]
	double f1 = linear_fit(std::min(m_age, 1.0), 0.0, 1.0, 0.8, 1.0);
	f1 = f1 * f1;
	double f2 = linear_fit(m_veg, 0.0, 1.0, 0.99, 1.0);
	// dans l'ordre : rocky , arid, fertile
	return dot(m_soil, Vec3d(0.999, 0.85, 0.8)) * f1 * f2;
}

double Bush::rate_of_growth()
{
	// pousse en une minute
	return linear_fit(m_veg, 0.0, 1.0, 1.0/10.0, 1.0/3.0) * std::exp(-m_age / 5.0);
}

int Bush::number_of_seeds()
{
	// dans l'ordre : rocky , arid, fertile
	return 2;
}

Vec2 Bush::random_seed_throw()
{
	return Vec2::polar(rdm::uniformd(0.0, 3.0), rdm::uniformd(0, 2.0 * M_PI));
}

double Bush::initial_number() const { return 0.5; }
double Bush::width() const { return 0.4; }
double Bush::height() const { return 0.4; }
double Bush::mass() const { return 1.0/5.0; }

Bush* Bush::kid(double x, double y) const 
{
	return new Bush(m_forest, x, y);
}


///============ Forest ============
Forest::Forest(const Ground* ground) :
	m_ground(ground)
{
}

Forest::~Forest()
{
	for (auto p : m_plants) delete p;
}

void Forest::init() 
{
	populate(Beech(this, 0.0, 0.0));
	populate(Palm(this, 0.0, 0.0));
	populate(Bush(this, 0.0, 0.0));
}

void Forest::populate(const Plant& seed)
{
	int n = std::round(seed.initial_number() * m_ground->sizei() * m_ground->sizej());
	for (; n > 0; --n) {
		m_plants.push_back(seed.kid(rdm::uniformd(0.0, m_ground->sizei() - 1.0), 
								    rdm::uniformd(0.0, m_ground->sizej() - 1.0)));
	}
}

void Forest::grow(double dt)
{
	std::back_insert_iterator<std::list<Plant*>> back_it(m_plants);
	for (auto it = m_plants.begin(); it != m_plants.end(); ) {
		if (!(*it)->grow(dt, back_it)) {
			m_density[hash_vec2d((*it)->x(), (*it)->y())] -= (*it)->mass();
			delete *it;
			it = m_plants.erase(it);
		} else {
			++it;
		}
	}
}

bool Forest::check_density(double x, double y, double mass)
{
	double &d = m_density[hash_vec2d(x, y)];
	if (d + mass > 1.0)
		return false;
	d += mass;
	return true;
}

// méthodes pour l'affichage mode texte
void Plant::draw(std::ostream& out) const {
	out << m_x << "   " << m_y << "   " << this->species() << "   " << m_age;
}
void Forest::draw(std::ostream& out) const {
	out << " A forest; for each plant, position (x,y), species, and age :" << std::endl;
	for(auto it = m_plants.begin(); it != m_plants.end(); ++it) {
		(*it)->draw(out);
		out << std::endl;
	}
	out << std::endl;
}
