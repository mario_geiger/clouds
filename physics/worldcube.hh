#ifndef __WORLDCUBE_H__
#define __WORLDCUBE_H__

#include "array3d.hh"
#include "vec3.hh"

		/* Template WorldCube : représente un tableau 3D, utile pour toutes
		 * les classes de ce type (champ de potentiels, ciel, etc.)
		 */

template<typename T>
class WorldCube
{
public:
	// Constructeurs. Délétion du constructeur de copie et de l'oppérateur d'affectation
	WorldCube();
	WorldCube(size_t nx, size_t ny, size_t nz, double lambda);
	WorldCube(const WorldCube<T>&) = delete;
	WorldCube& operator =(const WorldCube<T>&) = delete;
	virtual ~WorldCube() {}

	// changement de coordinnées : indices tableau -> coordonnées cartésiennes réelles
	double iToX(size_t i) const;
	double jToY(size_t j) const;
	double kToZ(size_t k) const;
	Vec3d position(size_t i, size_t j, size_t k) const;

	// la taille du tableau dans les 3 dimensions
	size_t sizei() const;
	size_t sizej() const;
	size_t sizek() const;

	// l'arrête des petits cubes
	double lambda() const;
	
	// setters
	void setLambda(double lambda);
	void setSize(int nx, int ny, int nz);
	
protected:
	// accéder à l'élément d'indice i,j,k
	const T& at(size_t i, size_t j, size_t k) const;

	Array3D<T> _d;
	double _lambda;
};

template<typename T>
WorldCube<T>::WorldCube() :
	_lambda(0.0)
{
}

template<typename T>
WorldCube<T>::WorldCube(size_t nx, size_t ny, size_t nz, double lambda) :
	_d(nx, ny, nz), _lambda(lambda)
{
}
template<typename T>
inline double WorldCube<T>::iToX(size_t i) const {
	return i * _lambda;
}
template<typename T>
inline double WorldCube<T>::jToY(size_t j) const {
	return (j - (_d.sizey() - 1) / 2.0) * _lambda;
}
template<typename T>
inline Vec3d WorldCube<T>::position(size_t i, size_t j, size_t k) const {
	return Vec3d(iToX(i),jToY(j),kToZ(k));
}
template<typename T>
inline double WorldCube<T>::kToZ(size_t k) const {
	return k * _lambda;
}
template<typename T>
const T& WorldCube<T>::at(size_t i, size_t j, size_t k) const {
	return _d(i, j, k);
}
template<typename T>
size_t WorldCube<T>::sizei() const {
	return _d.sizex();
}
template<typename T>
size_t WorldCube<T>::sizej() const {
	return _d.sizey();
}
template<typename T>
size_t WorldCube<T>::sizek() const {
	return _d.sizez();
}
template<typename T>
double WorldCube<T>::lambda() const {
	return _lambda;
}
template<typename T>
void WorldCube<T>::setLambda(double lambda) {
	_lambda = lambda;
}
template<typename T>
void WorldCube<T>::setSize(int nx, int ny, int nz) {
	_d.resize(nx, ny, nz);
}

#endif
