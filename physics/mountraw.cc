#include "mountraw.hh"
#include <cmath>

MountRaw::MountRaw(int nx, int ny) :
	_d(nx, ny, 1, 0.0)
{
}

void MountRaw::setAlt(int i, int j, double a)
{
	_d(i,j,0) = a;
}

double MountRaw::alt(double x, double y) const
{
	int fx = std::floor(x);
	int cx = std::ceil(x);
	int fy = std::floor(y);
	int cy = std::ceil(y);
		
	// (x,y) est hors du cadre (bord compris)
	if (fx < 0 || cx >= (int)_d.sizex() || 
	    fy < 0 || cy >= (int)_d.sizey())
		return 0.0;
	
	// pas besoin d'interpolation
	if (fx == cx && fy == cy)
		return _d(fx,fy,0);
		
	x -= fx; // in [0,1[
	y -= fy; // in [0,1[
	// sauf (x,y)=(0,0)

	// 4 points ne forment pas un plan
	// prenons une fonction f(x,y) tel que
	// f(x,0), f(x,1), f(0,y) et f(1,y) sont des interpolation linéaires
	// pour que quand on colle ensemble les carrés on obtient une fonction continue
	// f(x,y) = (1-x)(1-y) a + x(1-y) b + xy c + (1-x)y d
	// si on fixe y à 0 on a f(x,0)=(1-x) a + x b
	return (1.0-x)*(1.0-y) * _d(fx,fy,0)
	     +    x   *(1.0-y) * _d(cx,fy,0)
	     +    x   *   y    * _d(cx,cy,0)
	     + (1.0-x)*   y    * _d(fx,cy,0);
}

MountRaw* MountRaw::copy() const 
{
	return new MountRaw(*this);
}

MountRaw* MountRaw::move() 
{
	return new MountRaw(std::move(*this));
}

void MountRaw::draw(std::ostream& out) const 
{
	out << "Raw mountain of size (" << _d.sizex() << ", " << _d.sizey() << ") ; altitude at each point (discreete coordonates) :" << std::endl;
	for (size_t i = 0; i < _d.sizex(); ++i) {
		for (size_t j = 0; j < _d.sizey(); ++j) {
			out << i << ' ' << j << ' ' << alt(i,j) << std::endl;
		}
		out << std::endl;
	}
}

