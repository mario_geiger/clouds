#include "potential.hh"
#include "mountsimple.hh"
#include <fstream>
#include <string>
using namespace std;

int main(int argc, char* argv[]) {
	bool mount = argc > 1 && string(argv[1]) == string("-mount");
	MountSimple montagne(15.0, 15.0, mount ? 15.0 : 0.0, 5.0, 5.0);
	PotentialField champ(30, 30, 30, 20.0/29.0);
	champ.init(20.0, &montagne);
	champ.calculate_laplacian(&montagne);
	champ.solve(0.1, 2.2621843e-5, 5000, &montagne, false);
	champ.aff_potential();
}
