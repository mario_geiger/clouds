#ifndef __MOUNTRANGE_HH__
#define __MOUNTRANGE_HH__

#include "mount.hh"
#include <list>

class MountRange : public Mount
{
public:
	MountRange();
	MountRange(MountRange&& other);
	MountRange(const MountRange& other);
	~MountRange();
	MountRange& operator= (MountRange other);

	void addMount(const Mount& m);
	void addMount(Mount&& m);
	double alt(double x, double y) const override;
	MountRange* copy() const override;
	MountRange* move() override;
	void draw(std::ostream& out) const override;

private:
	std::list<Mount*> m_ms;
};

#endif

