#ifndef __MOUNTSIMPlE_HH__
#define __MOUNTSIMPLE_HH__

#include "mount.hh"

class MountSimple : public Mount
{
public:
	MountSimple(double xCenter, double yCenter, double amplitude, double xSpread, double ySpread); 
	double alt(double x, double y) const override;
	MountSimple* copy() const override;
	MountSimple* move() override;
	void draw(std::ostream& out) const override;

private:
	double _xCenter;
	double _yCenter;
	double _amplitude;
	double _xSpread2;
	double _ySpread2;
};

#endif

