#include <iostream>
#include <fstream>
#include "mountsimple.hh"
using namespace std;

int main(int argc, char* argv[]) {
	if (argc < 2) return 1;
	MountSimple mountain(15.0, 15.0, 18.0, 5.0, 10.0);
	ofstream out;
	out.open(argv[1]);
	
	if(!out.fail()) {
		for(int i(0); i<=29; i++) {
			for(int j(0); j<=29; j++) {
				out << i << " " << j << " " << mountain.alt(i, j) << endl;
			}
			out << endl;
		}
	} else {
		cerr << "Erreur, le fichier " << argv[1] << " ne peut être ouvert pour écriture !";
	}
	
	out.close();
	
	return 0;
}

