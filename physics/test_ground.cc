#include <iostream>
#include "system.hh"
#include "mountsimple.hh"
using namespace std;

int main() {
	MountSimple montagne(15,15,15,5,5);
	System monde;
	monde.set_mount(move(montagne));
	monde.init(false);
	
	for(int i(1); i <= 100; i++) {
		monde.evolve(1.0);
	}
	
	cout << monde;
	
	return 0;
}
