#include <iostream>
#include "sky.hh"
#include "mountsimple.hh"
#include "potential.hh"
using namespace std;

int main()
{
	// copié de test_laplacian.cc
	MountSimple m(15.0, 15.0, 15.0, 5.0, 5.0);
	PotentialField pot(30, 30, 30, 20.0/29.0);
	pot.init(20.0, &m);
	pot.solve(0.1, pow(20.0/29.0, 4)*0.0001, 5000, &m, false);

	Sky sky(pot);
	
	//               +-------------------------------------- temperature dans la plaine
	//               |         +---------------------------- pression dans la plaine
	//               |         |      +--------------------- vitesse du vent dans la plaine
	//               v         v      v       v------------- humidite
	sky.initialize(284.5, 101325.0, 20.0, 8.0e-3);

	sky.data(cout);
	return 0;
}
