#include "mountsimple.hh"
#include <cmath>

MountSimple::MountSimple(double xCenter, double yCenter, double amplitude, double xSpread, double ySpread) :
	_xCenter(xCenter), _yCenter(yCenter), _amplitude(amplitude), _xSpread2(2.0*xSpread*xSpread), _ySpread2(2.0*ySpread*ySpread)
{
}

double MountSimple::alt(double x, double y) const
{
	double a = _amplitude * std::exp(-(x-_xCenter)*(x-_xCenter) / _xSpread2 - (y-_yCenter)*(y-_yCenter) / _ySpread2);
	return a < 0.5 ? 0.0 : a;
}

MountSimple* MountSimple::copy() const
{
	return new MountSimple(*this);
}

MountSimple* MountSimple::move() 
{
	return new MountSimple(std::move(*this));
}		

void MountSimple::draw(std::ostream& out) const 
{
	out << "A simple mountain at (" << _xCenter << ", " << _yCenter 
	    << "), spread (" << std::sqrt(_xSpread2/2.0) << ", " << std::sqrt(_ySpread2/2.0) 
	    << "), height " << _amplitude << "." << std::endl;
}
