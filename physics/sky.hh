#ifndef __SKY_H__
#define __SKY_H__

#include "constants.hh"
#include "worldcube.hh"
#include "potential.hh"
#include "textdrawable.hh"

class Sky;
class AirCube : public TextDrawable {
public:
	AirCube();
	void initialize(double z
	              , double plainTemperature, double plainPressure
	              , double bernouilliConstant);
	void update();

	void draw(std::ostream& out) const override;

	inline Vec3d windSpeed() const         { return _windSpeed; }
	inline double windSpeedSquared() const { return _windSpeedSquared; }
	inline double enthalpy() const         { return _enthalpy; }
	inline double temperature() const      { return _temperature; }
	inline double pressure() const         { return _pressure; }
	inline double partialPressure() const  { return _partialPressure; }
	inline double vaporPressure() const    { return _vaporPressure; }
	inline double liquidState() const      { return _liquidState; }
	
	inline void setWindSpeed(Vec3d windSpeed) { _windSpeed = windSpeed; _windSpeedSquared = windSpeed.lengthSquared(); }
	
	double rain(double dt);
private:
	Vec3d _windSpeed;
	double _humidity;
	double _windSpeedSquared;
	double _enthalpy;
	double _temperature;
	double _pressure;
	double _partialPressure;
	double _vaporPressure;
	double _liquidState; // > 1  => liquid
	
	double _traveltime;
	
	friend Sky;
};

class Sky : public WorldCube<AirCube>, public TextDrawable {
public:
	Sky();
	Sky(size_t nx, size_t ny, size_t nz, double lambda, double windSpeed);
	Sky(size_t nx, size_t ny, size_t nz, double lambda);
	explicit Sky(const PotentialField& pot);
	Sky(const Sky&) = delete;
	Sky& operator =(const Sky&) = delete;
	
	void initialize(double plainTemperature, double plainPressure, double plainWindSpeed, double humidity);
	void setWindSpeeds(const PotentialField& pot);
	
	double liquidState(size_t i, size_t j, size_t k) const;
	double rain(size_t i, size_t j, double dt);
	
	void evolve(double dt);
	void draw(std::ostream& out) const override;
	void data(std::ostream& out) const;		// pour les tests
private:
	double _bernouilliConstant;
};

#endif
