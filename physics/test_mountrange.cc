#include "mountrange.hh"
#include "mountsimple.hh"
#include <iostream>

using namespace std;

int main()
{
	MountSimple m1(15.0, 15.0, 18.0, 5.0, 10.0);
	MountRange r1;
	{
		MountSimple m2(2.0, 22.0, 12.0, 12.0, 3.0);
		MountSimple m3(20.0, 2.0, 15.0, 8.0, 4.0);
		r1.addMount(std::move(m2));
		r1.addMount(std::move(m3));
	}
	MountRange r2;
	r2.addMount(std::move(m1));
	r2.addMount(std::move(r1));

	for (int i = 0; i < 30; ++i) {
		for (int j = 0; j < 30; ++j) {
			cout << i << ' ' << j << ' ' << r2.alt(i, j) << endl;
		}
		cout << endl;
	}
}
