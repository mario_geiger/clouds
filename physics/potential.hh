#ifndef __POTENTIAL__
#define __POTENTIAL__

#include "array3d.hh"
#include "vec2.hh"
#include "vec3.hh"
#include "mount.hh"
#include "worldcube.hh"
#include "textdrawable.hh"

		/* Classe pour les champs de potentiels.
		 * N.B. : Certaines méthodes d'affichages sont superflues pour la 
		 * version finales du programme, mais sont laissées ici pour 
		 * permettre aux fichiers de test de fonctionner toujours
		 * (production des graphiques).
		 */

class Potential : public TextDrawable {
	public : 
		// Constructeur par défaut par défaut -> vecteurs nuls à l'initialisation.
	
		// méthodes getters
		Vec2 potential() const;
		Vec2 laplacian() const;
		
		// méthodes setters
		void potential(const Vec2& other);
		void laplacian(const Vec2& other);
		
		// méthode d'affichage en mode texte
		void draw(std::ostream& out) const override;
	private :
		Vec2 _potential;
		Vec2 _laplacian;
};

class PotentialField : public WorldCube<Potential>, public TextDrawable {
	public:
		// Constructeur
		PotentialField(size_t nx, size_t ny, size_t nz, double lambda);
		// Constructeur de copie et d'affectation deleted car superflus
		PotentialField operator=(const PotentialField&) = delete;
		PotentialField(const PotentialField&) = delete;
		
		// initalisation
		void init(const double v, const Mount* mountain);
		
		// calcul des laplaciens
		void calculate_laplacian(const Mount* mountain);
		// résolution des équations de Laplace pour les vents
		void solve(const double epsilon, const double precision, const int max_iter, const Mount* mountain, const bool verbal);
		// retourne la vitesse du vent à la position (i,j,k)
		Vec3d wind_speed(const size_t i, const size_t j, const size_t k) const ;
		
		
		// méthodes d'affichages des potentiels et laplaciens ( pour les tests )
		void aff_potential() const;
		void aff_laplacian() const;
		// affichage de toutes les informations (potentiels, laplaciens, vitesses du vent)
		void aff_all(std::ostream& out) const;
		
		// méthodes d'affichages en mode texte
		void draw(std::ostream& out) const override;
		
	private:
		double _v;	// vitesse du vent dans la plaîne
		
		// approximation du laplacien à la position (i,j,k)
		// par une méthode de différences finies centrée
		Vec2 finite_diff(const size_t i, const size_t j, const size_t k) const;
		// erreur totale
		double error() const;
		// une itération du processus de convergence pour le calcul des vents
		void iteration(const double epsilon, const Mount* mountain);
};
#endif
