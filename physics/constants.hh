#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

		/* Namespace pour les constantes physiques. */

namespace Physique {
constexpr double g = 9.81;
constexpr double R = 8.3144621;
constexpr double pressureRef = 101325.0;
constexpr double airMolecularWeight = 0.02896;
constexpr double waterMolecularWeight = 0.0180153;
}

#endif
