#include "mountrange.hh"

MountRange::MountRange()
{
}

MountRange::MountRange(MountRange&& other)
{
	m_ms = other.m_ms;
	other.m_ms.clear();
}

MountRange::MountRange(const MountRange& other)
{
	for (auto k : other.m_ms)
		m_ms.push_back(k->copy());
}

MountRange::~MountRange()
{
	for (auto k : m_ms)
		delete k;
}

MountRange& MountRange::operator= (MountRange other)
{
	std::swap(m_ms, other.m_ms);
	return *this;
}

void MountRange::addMount(const Mount& m)
{
	m_ms.push_back(m.copy());
}

void MountRange::addMount(Mount&& m)
{
	m_ms.push_back(m.move());
}

double MountRange::alt(double x, double y) const 
{
	double a = 0.0;
	for (auto k : m_ms)
		a = std::max(a, k->alt(x, y));
	return a;
}

MountRange* MountRange::copy() const 
{
	return new MountRange(*this);
}

MountRange* MountRange::move() 
{
	return new MountRange(std::move(*this));
}

void MountRange::draw(std::ostream& out) const 
{
	out << "Range mountain :" << std::endl;
	for (auto k : m_ms)
		k->draw(out);
}
