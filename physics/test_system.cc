#include <iostream>
#include <cmath>
#include "system.hh"
#include "mountsimple.hh"
using namespace std;

int main() {
	System monde;
	monde.set_size(30,30,30);
	monde.set_lambda(20./29.);
	MountSimple pelerin(15,15,15,5,5);
	monde.set_mount(move(pelerin));
	monde.init(false);
	for(int i(0); i<100; i++) monde.evolve(0.1);
	cout << monde << endl;
	return 0;
}
